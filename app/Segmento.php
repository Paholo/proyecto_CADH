<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Segmento extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'documento_id','Titulo','tipo', 'documento_titulo',
    ];
}
