<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class antecedenteCambio extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'id_secCambio','Detalle','Contenido_Html','Nombre','Contra','Tipo',
        'Serie','Numero','Fecha','Corte',
    ];
}
