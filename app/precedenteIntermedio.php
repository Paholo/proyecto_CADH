<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class precedenteIntermedio extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'id_segmento','Titulo','Contenido', 'Fecha', 'Autor'
    ];
}
