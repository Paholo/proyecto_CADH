<?php
namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;
use App\Documento;
use App\Segmento;
use App\cambio;
use App\User;
use DB;


class DisplayAntecedenteAntController extends Controller
{

    public function show($id)
    {
        $cambio =DB::table('antecedentecambios')->where('id', $id)->get();
        // $ar  =  DB::table('seccioncambio')->where('id',)->select('Titulo','id','Autor','id_secPadre')->get();
        // $secTitulo = $ar[0]->Titulo;
        $documento = $cambio[0]->id;
        // $autor = $ar[0]->Autor;
       
        // $padre  =  DB::table('sec_sec')->where('id_secHijo', $ar[0]->id_secPadre)->select('id_secPadre')->get();

        // $sec  =  DB::table('secciones')->where('id', $padre[0]->id_secPadre)->select('Titulo','id')->get();

        // $sec = $sec[0];
        $cambio = $cambio[0];

     
        $p = DB::table('antecedente_antecedente')->where('id_antPadre', $id)->select('id_antHijo')->get();
        $antecedentes = array();
        if(sizeof($p) >0 ){
            foreach($p as $hijo)
            {
                $ant =DB::table('antecedentecambios')->where('id', $hijo->id_antHijo)->get();
                $antecedentes[] = $ant[0];
            }
            
        }
        $cat =array();

        foreach($antecedentes as $ant){
            $categoria = DB::table('categoriaantecedentes')->where('id_antecedente',$ant->id)->select('categoria')->get();
            $categorias = array();
           for($i =0;$i< sizeof($categoria);$i++)
            {
                $categorias[] = $categoria[$i]->categoria;
            }
            // dd($categorias);
            $cat[] = $categorias;
        }
       
        // dd($cat);
     

        return view('displayAntecedenteAnt.show',compact('cambio','antecedentes','sec','documento','cat'));
    }
}
