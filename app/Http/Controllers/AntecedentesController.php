<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;
use Auth;
use Illuminate\Http\Request;
use App\Documento;
use App\Segmento;
use App\antecedenteCambio;
use App\cambio;
use App\User;
use Carbon\Carbon;
use DB;

class AntecedentesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $cambios = cambio::orderBy('id','DESC')->paginate(10);
        $documento = array();
        $segmento = array();
        foreach($cambios as $cambio){
            $id_seg =  DB::table('seccioncambio')->where('id', $cambio->id_seccion)->select('Titulo')->get();
            
            array_push($segmento, $id_seg[0]->Titulo);
            // array_push($documento, $id_seg[0]->id_seccion);
        }
        return view('cambios.index',compact('cambios','segmento'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $idPadre = Input::get('iden'); 
        // dd($idPadre);

        $padre  =  DB::table('cambios')->where('id',(int) $idPadre)->get();
         
 
        $padre = $padre[0]->id;
        return view('antecedentes.create',compact('padre'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $this->validate($request, [
            'serie' => 'required|max:255',
            'nombre' => 'required|max:400',
            'tipo' => 'required',
            'numero' => 'required|max:50',
            'padre' => 'required|max:255',
            'fecha' => 'required|max:255',
            'detalle' => 'required|max:8000',
            'categorias'=>'required',
            'corte'=> 'required'
    
        ]);
        
        // $contra = $request->contra;
        //  dd($request->categorias[1]);
        $padre  =  DB::table('cambios')->where('id',$request->padre)->get();
        $padre_id = $padre[0]->id;
        $detalle = $request->detalle;
       
        
        if($detalle =="@[vacio]"){
            $detalle ="";
        }


        if($request->tipo =="@[vacio]"){
            $request->tipo ="";
        }

        if($request->parrafo =="@[vacio]"){
            $request->parrafo ="";
        }
       
        
        $newId = DB::table('antecedentecambios')
        ->insertGetId(['Nombre'=> $request->nombre,'Detalle'=> $detalle,'Fecha'=> $request->fecha, 'serie' =>$request->serie,
        'Tipo'=> $request->tipo, 'Numero'=>$request->numero,'Contra'=>$request->contra,'Contenido_Html'=>$request->detalle,'Fecha'=>$request->fecha,
        'Parrafo'=>$request->parrafo,'Corte'=>$request->corte,'added_by'=>Auth::user()->id,'created_at'=>Carbon::now()->format('Y-m-d H:i:s'),'explicacion'=>$request->explicacion]);
        
        DB::table('cambio_antecedente')->insert(['id_cambio'=>$padre_id,'id_antecedente'=>$newId]);
        // dd($newId);
        foreach ($request->input('categorias') as $key => $value) {
           DB::table('categoriaantecedentes')->insert(['id_antecedente'=>$newId,'categoria'=>$value,'added_by'=>Auth::user()->id,'created_at'=>Carbon::now()->format('Y-m-d H:i:s')]);
        }

        // return redirect()->route('documentos.index')
        // ->with('success','Segmento modificado exitosamente');
        $cambio = cambio::find($padre_id);
        $ar  =  DB::table('seccioncambio')->where('id', $cambio->id_seccion)->select('Titulo','id','Autor')->get();
        $secTitulo = $ar[0]->Titulo;
        $documento = $ar[0]->id;
        $autor = $ar[0]->Autor;
        $p = DB::table('cambio_antecedente')->where('id_cambio', $padre_id)->select('id_antecedente')->get();
        $antecedentes = array();
        if(sizeof($p) >0 ){
            foreach($p as $hijo)
            {
                $ant =DB::table('antecedentecambios')->where('id', $hijo->id_antecedente)->get();
                $antecedentes[] = $ant[0];
            }
            
        }


        return view('antecedentes.show',compact('cambio','secTitulo','antecedentes','autor','documento'))->with('success','Antecedente eliminado exitosamente');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        
        $cambio = cambio::find($id);
        $ar  =  DB::table('seccioncambio')->where('id', $cambio->id_seccion)->select('Titulo','id','Autor')->get();
        $secTitulo = $ar[0]->Titulo;
        $documento = $ar[0]->id;
        $autor = $ar[0]->Autor;
        $p = DB::table('cambio_antecedente')->where('id_cambio', $id)->select('id_antecedente')->get();
        $antecedentes = array();
        if(sizeof($p) >0 ){
            foreach($p as $hijo)
            {
                $ant =DB::table('antecedentecambios')->where('id', $hijo->id_antecedente)->get();
                $antecedentes[] = $ant[0];
            }
            
        }
        
        // dd($antecedentes);


        return view('antecedentes.show',compact('cambio','secTitulo','antecedentes','autor','documento'));
    }

    public function atras($id){
        // dd($id);
      

        $seccion = DB::table('seccioncambio')->where('id', $id)->get();
        $seccion = $seccion[0];
        // $cam= DB::table('cambio_antecedente')->where('id_cambio', $seccion->id)->get();
        $cambios = DB::table('cambios')->where('id_seccion', $id)->get();
         
        return view('seccioncambios.show',compact('seccion','cambios'));

    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $antecedente  =  DB::table('antecedentecambios')->where('id',$id)->get();
        $p = DB::table('cambio_antecedente')->where('id_antecedente',$id)->get();
        $padre = DB::table('cambios')->where('id',$p[0]->id_cambio)->get();
        $padre = $padre[0]->id;
        $categoria = DB::table('categoriaantecedentes')->where('id_antecedente',$id)->select('categoria')->get();
        $categorias = array();
       for($i =0;$i< sizeof($categoria);$i++)
        {
            $categorias[] = $categoria[$i]->categoria;
        }
        $antecedente = $antecedente[0];
    //    dd(sizeof($categorias));
        return view('antecedentes.edit',compact('antecedente','categorias','padre'));
  
    }

    // public function administrar($id)
    // {
    //     $cambio = cambio::find($id);
    //     $segmento = Documento::find($cambio->id_segmento);
    //     //dd($documentos);
       
    //     return view('antecedentes.show',compact('segmento','cambio'));
  
    // }
    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */

    public function editar(Request $request,$id){
        
        $this->validate($request, [
            'serie' => 'required|max:255',
            'nombre' => 'required|max:400',
            'tipo' => 'required',
            'numero' => 'required|max:2000',
            'padre' => 'required|max:255',
            'fecha' => 'required|max:200',
            'detalle' => 'required|max:8000',
            'categorias'=>'required',
            'corte'=> 'required'
    
        ]);

        $padre  =  DB::table('cambios')->where('id',$request->padre)->get();
        $padre_id = $padre[0]->id;
        $detalle = $request->detalle;
       
        if($detalle =="@[vacio]"){
            $detalle ="";
        }

    
        if($request->tipo =="@[vacio]"){
            $request->tipo ="";
        }

        if($request->parrafo =="@[vacio]"){
            $request->parrafo ="";
        }

        $newId = DB::table('antecedentecambios')->where('id', $id)
        ->update(['Nombre'=> $request->nombre,'Detalle'=> $detalle,'Fecha'=> $request->fecha, 'serie' =>$request->serie,
        'Tipo'=> $request->tipo, 'Numero'=>$request->numero,'Contra'=>$request->contra,'Contenido_Html'=>$request->detalle,'Fecha'=>$request->fecha,
        'Parrafo'=>$request->parrafo,'Corte'=>$request->corte,'updated_by'=>Auth::user()->id,'updated_at'=>Carbon::now()->format('Y-m-d H:i:s'),'explicacion'=>$request->explicacion]);
        
        // dd($request->input('categorias'));
        DB::table('categoriaantecedentes')->where('id_antecedente',$id)->delete();
        foreach ($request->input('categorias') as $key => $value) {
           DB::table('categoriaantecedentes')->insert(['id_antecedente'=>$id,'categoria'=>$value,'added_by'=>Auth::user()->id,'created_at'=>Carbon::now()->format('Y-m-d H:i:s')]);
           
        }

        // return redirect()->route('documentos.index')
        // ->with('success','Segmento modificado exitosamente');
        $cambio = cambio::find($padre_id);
        $ar  =  DB::table('seccioncambio')->where('id', $cambio->id_seccion)->select('Titulo','id','Autor')->get();
        $secTitulo = $ar[0]->Titulo;
        $documento = $ar[0]->id;
        $autor = $ar[0]->Autor;
        $p = DB::table('cambio_antecedente')->where('id_cambio', $padre_id)->select('id_antecedente')->get();
        $antecedentes = array();
        if(sizeof($p) >0 ){
            foreach($p as $hijo)
            {
                $ant =DB::table('antecedentecambios')->where('id', $hijo->id_antecedente)->get();
                $antecedentes[] = $ant[0];
            }
            
        }


        return view('antecedentes.show',compact('cambio','secTitulo','antecedentes','autor','documento'))->with('success','Antecedente eliminado exitosamente');

    }
    public function update(Request $request, $id)
    {
        // dd($request);
        // $this->validate($request, [
        //     'Titulo' => 'required|max:255',
        //     'PAT' => 'required',
        //     'PAC' => 'required',
        //     'PAF' => 'required',
        //     'MAT' => 'required',
        //     'MAC' => 'required',
        //     'MAF' => 'required',

        // ]);

        // $id_doc = $request->only('documentos');
        // $titulo = $request->only('Titulo');
        // $tipo = $request->only('tipo');

        // // $id_sec =  DB::table('cambios')->where('id', $id)->select('id_secCambio')->get();
        // // $id_sec=$id_sec[0];
       
        // dd($id);
        // DB::table('primerantecedentes')
        // ->insert(['id_secCambio' => $id,'Titulo'=> $request->PAT,'Contenido'=> $request->PAC,'Fecha'=> $request->PAF]);
        
        // DB::table('miantecedentes')
        // ->insert(['id_secCambio' =>  $id,'Titulo'=> $request->MAT,'Contenido'=> $request->MAC,'Fecha'=> $request->MAF]);

        // return redirect()->route('documentos.index')
        //     ->with('success','Segmento modificado exitosamente');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy(Request $request, $id)
    {
        $idPadre = $request->padre;
        // dd( $idPadre);
        DB::table('antecedentecambios')->where('id',$id)->delete();

        $cambio = cambio::find($idPadre);
        $ar  =  DB::table('seccioncambio')->where('id', $cambio->id_seccion)->select('Titulo','id','Autor')->get();
        $secTitulo = $ar[0]->Titulo;
        $documento = $ar[0]->id;
        $autor = $ar[0]->Autor;
        $p = DB::table('cambio_antecedente')->where('id_cambio', $id)->select('id_antecedente')->get();
        $antecedentes = array();
        if(sizeof($p) >0 ){
            foreach($p as $hijo)
            {
                $ant =DB::table('antecedentecambios')->where('id', $hijo->id_antecedente)->get();
                $antecedentes[] = $ant[0];
            }
            
        }
        
        // dd($antecedentes);


        return view('antecedentes.show',compact('cambio','secTitulo','antecedentes','autor','documento'));

        // $cambio = cambio::find($idPadre);
        // $ar  =  DB::table('seccioncambio')->where('id', $cambio->id_seccion)->select('Titulo','id','Autor')->get();
        // $secTitulo = $ar[0]->Titulo;
        // $documento = $ar[0]->id;
        // $autor = $ar[0]->Autor;
        // // $primer = DB::table('segmentos')->where('id', $cambio->id_segmento)->select('Titulo','documento_titulo')->get();
        // $antecedentes =DB::table('antecedenteCambios')->where('id_secCambio', $idPadre)->get();


        // return view('antecedentes.show',compact('cambio','secTitulo','antecedentes','autor','documento'))->with('success','Antecedente eliminado exitosamente');

        // return redirect()->route($s2)
        //     ->with('success','Antecedente eliminado exitosamente');
    }

    public function buscar(Request $request)
    {
       

        // Gets the query string from our form submission 
            $query = $request->input('search');
            // Returns an array of articles that have the query string located somewhere within 
            // our articles titles. Paginates them so we can break up lots of search results.
            $segmentos = DB::table('segmentos')->where('Titulo', 'LIKE', '%' . $query . '%')->paginate(10);

         
                
            // returns a view and passes the view the list of articles and the original query.
            return view('segmentos.index',compact('segmentos'))
            ->with('i', ($request->input('page', 1) - 1) * 5);

          
    }
}