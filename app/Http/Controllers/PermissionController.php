<?php


namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Role;
use App\Permission;
use DB;
//use Request;

class PermissionController extends Controller
{
    /**
     * Display a listing of the permissions.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $permissions = Permission::orderBy('id','DESC')->paginate(100);
        return view('permissions.index',compact('permissions'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */


     /*revisar!!!!! */
    public function create()
    {
        $permissions = Permission::pluck('display_name','id');
        return view('permissions.create',compact('permissions')); //return the view with the list of permissions passed as an array
    }
    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:roles,name',
            'display_name' => 'required',
            'description' => 'required',
        ]);
        //create the new permission
        $permission = new Permission();
        $permission->name = $request->input('name');
        $permission->display_name = $request->input('display_name');
        $permission->description = $request->input('description');
        $permission->save();
       
        return redirect()->route('permissions.index')
            ->with('success','Permission created successfully');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $permission = Permission::find($id); //Find the requested permission
       
        
        //return the view with the role info and its permissions
        return view('permissions.show',compact('permission'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $permission = Permission::find($id);//Find the requested role
        
        
        return view('permissions.edit',compact('permission'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'display_name' => 'required',
            'description' => 'required',
            
        ]);
        //Find the role and update its details
        $permission = Permission::find($id);
        $permission->display_name = $request->input('display_name');
        $permission->description = $request->input('description');
        $permission->save();
       
        return redirect()->route('permissions.index')
            ->with('success','Permission updated successfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        DB::table("permissions")->where('id',$id)->delete();
        return redirect()->route('permissions.index')
            ->with('success','permission deleted successfully');
    }

    public function buscar(Request $request)
    {
       

        // Gets the query string from our form submission 
            $query = $request->input('search');
            // Returns an array of articles that have the query string located somewhere within 
            // our articles titles. Paginates them so we can break up lots of search results.
            $permissions = DB::table('permissions')->where('name', 'LIKE', '%' . $query . '%')->paginate(10);

            info('This is some useful information.');
                
            // returns a view and passes the view the list of articles and the original query.
            return view('permissions.index',compact('permissions'))
            ->with('i', ($request->input('page', 1) - 1) * 5);

          
    }
}