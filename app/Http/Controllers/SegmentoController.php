<?php
namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;
use App\Documento;
use App\Segmento;
use App\Seccion;
use App\Cambio;
use App\User;
use DB;

class SeccionesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $segmentos = Segmento::orderBy('id','DESC')->paginate(10);
        return view('secciones.index',compact('segmentos'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $documentos = Documento::pluck('Titulo','id');
        return view('secciones.create',compact('documentos')); //return the view with the list of roles passed as an array
      // return view('segmentos.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */

     //funcion para guardar las secciones de cambio dentro de un articulo
    public function store(Request $request)
    {
        $this->validate($request, [
            'Titulo' => 'required|max:255',
            'tipo' => 'required',
            'documentos'=>'required',
            'Contenido'=>'required'

        ]);
        $docID = $request->only('documentos');
        $documento = Documento::find($docID)->first();
        
        //nombre guardados en la sesion 
        $nombre = Auth::user()->name;
        $id = Auth::user()->id;
        $texto = $request->Contenido;

        //se separa del string completo la parte que contiene dicho tag
        // $matches contiene todos los segmentos con cambios
        $patron = '#<span class="CADH">(.*?)</span>#s';
        $filtro =   preg_match_all ($patron, $texto, $matches);

       // se incerta el cambio
        DB::beginTransaction();
        DB::table('segmentos')->insert(
            ['documento_id' => $documento->id, 'documento_titulo'=>$documento->Titulo,'Titulo'=> $request->Titulo , 'tipo'=>$request->tipo, 'contenido'=>$request->Contenido, 'id_Autor'=> $id, 'NombreAutor'=>$nombre ]
        );

        //obtengo el el id del ultimo insert         
        $id_seg =  DB::table('segmentos')->where('documento_id', $documento->id)->where('Titulo', $request->Titulo)->select('id')->get();
        //agrego los cambios a la base de datos 
        for ($i = 0 ; $i<sizeof($matches[1]);$i++){
            $cam = html_entity_decode($matches[1][$i], ENT_QUOTES, 'UTF-8');
           
            DB::table('cambios')->insert(
                ['id_autor' => $id,'id_segmento'=>$id_seg[0]->id,'Titulo'=> $cam, 'Contenido'=>$cam ]
            );
        }
        
        DB::commit();
        return redirect()->route('secciones.index')
            ->with('success','Segmento creato exitosamente');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $segmento = Segmento::find($id);
        return view('secciones.show',compact('segmento'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $segmento = Segmento::find($id);
        $documentos = Documento::get();
        //dd($documentos);
       
        return view('secciones.edit',compact('segmento','documentos'));
  
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'Titulo' => 'required|max:255',
            'tipo' => 'required',
            'documentos'=> 'required'
        ]);

        $id_doc = $request->only('documentos');
        $titulo = $request->only('Titulo');
        $tipo = $request->only('tipo');

        $documento = Documento::find($id_doc)->first();
        //dd($documento->id);
        
        DB::table('segmentos')
            ->where('id', $id)
            ->update(['documento_id' => $documento->id,'documento_titulo'=> $documento->Titulo,'Titulo'=>$request->Titulo,'tipo'=>$request->tipo[0]]);
        
        return redirect()->route('secciones.index')
            ->with('success','Segmento modificado exitosamente');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        Segmento::find($id)->delete();
        return redirect()->route('secciones.index')
            ->with('success','Segmento eliminado exitosamente ');
    }

    public function buscar(Request $request)
    {
       

        // Gets the query string from our form submission 
            $query = $request->input('search');
            // Returns an array of articles that have the query string located somewhere within 
            // our articles titles. Paginates them so we can break up lots of search results.
            $segmentos = DB::table('segmentos')->where('Titulo', 'LIKE', '%' . $query . '%')->paginate(10);

         
                
            // returns a view and passes the view the list of articles and the original query.
            return view('secciones.index',compact('segmentos'))
            ->with('i', ($request->input('page', 1) - 1) * 5);

          
    }
}