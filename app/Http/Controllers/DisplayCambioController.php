<?php
namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;
use App\Documento;
use App\Segmento;
use App\cambio;
use App\User;
use DB;

class DisplayCambioController extends Controller
{

   
    
    public function show($id)
    {
    
        $seccion = DB::table('seccionCambio')->where('id', $id)->get();
        $seccion = $seccion[0];
        $cambios= DB::table('cambios')->where('id_seccion', $id)->get();
        $antecedentes = array();
        $categorias = array();
        foreach($cambios as $camb){

            $p = DB::table('cambio_antecedente')->where('id_cambio', $camb->id)->select('id_antecedente')->get();
            $antecedentes1 = array();
            if(sizeof($p) >0 ){
                $antecedentes1[] = $camb->id;
                foreach($p as $hijo)
                {
                    $ant =DB::table('antecedenteCambios')->where('id', $hijo->id_antecedente)->get();
                    $antecedentes1[] = $ant[0];
                    // $cont++;
                }
            $antecedentes[] = $antecedentes1;
            $cat =array();
                    
                    foreach($antecedentes1 as $ant){
                        if((String)gettype($ant) != 'integer'){
                            $cat[]= $ant->id;
                            $categoriaA = DB::table('categoriaantecedentes')->where('id_antecedente',$ant->id)->select('categoria')->get();
                            $categoriaAs = array();
                           for($i =0;$i< sizeof($categoriaA);$i++)
                            {
                                $categoriaAs[] = $categoriaA[$i]->categoria;
                            }
                            // dd($categorias);
                            $cat[] = $categoriaAs;
                        }
                       
                    }
                    $categorias[] = $cat;
            }
        }
       
        // dd(($categorias));
        
         
        return view('displayCambio.show',compact('seccion','cambios','antecedentes','cat'));
    }
}
