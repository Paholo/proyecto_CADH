<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;
use Auth;
use Illuminate\Http\Request;
use App\Documento;
use App\Segmento;
use App\Seccione;
use App\Cambio;
use App\User;
use DB;

class SeccionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $secciones = Seccione::orderBy('id','DESC')->paginate(10);
        // $secciones = DB::table('secciones')->orderBy('id','DESC')->get();
       
        return view('secciones.index',compact('secciones'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $documento = Input::get('titulo'); 
        $iden = Input::get('padre');   
        return view('secciones.create',compact('documento','iden')); //return the view with the list of roles passed as an array
      // return view('segmentos.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'Titulo' => 'required|max:255',
            'tipo' => 'required',
            'Titulo_Padre'=>'required',
            'Contenido'=>'required'

        ]);
        $id_padre = intval($request->padre);
        $documento = $request->only('Titulo_Padre');
        $docID = DB::table('documentos')->where('id', $id_padre)->where('Titulo', $documento)->select('id')->get();
        $seccionID =  DB::table('secciones')->where('id', $id_padre)->where('Titulo', $documento)->select('id')->get();
        // dd($docID);
        $nombre = Auth::user()->name;
        $id = Auth::user()->id;
        $texto = $request->Contenido;
        $patron = '#<span class="CADH">(.*?)</span>#s';
        $filtro =   preg_match_all ($patron, $texto, $matches);
        $tclean = strip_tags($texto);
        // dd($tclean);
        $idnuevo = "";
      

        if(sizeof($docID) == 0)
        {
            //secion en seccion 
  
            DB::beginTransaction();
            $idnuevo = DB::table('secciones')->insertGetId(
                ['Titulo'=> $request->Titulo , 'tipo'=>$request->tipo, 'Contenido'=>$tclean,'Contenido_html'=>$request->Contenido,  'added_by'=> $id, 'Autor'=>$request->Autor ]
            );
            DB::commit();

            DB::beginTransaction();
            DB::table('sec_sec')->insert(
                ['id_secPadre'=> $seccionID[0]->id , 'id_secHijo' => $idnuevo ]
            );
            DB::commit();

        }else{
            //secion en documento principal
            DB::beginTransaction();
            $idnuevo = DB::table('secciones')->insertGetId(
                ['Titulo'=> $request->Titulo , 'tipo'=>$request->tipo, 'Contenido'=>$tclean,'Contenido_html'=>$request->Contenido, 'added_by'=> $id, 'Autor'=>$request->Autor ]
            );
            DB::commit();

            DB::beginTransaction();
            DB::table('doc_sec')->insert(
                ['id_doc'=> $docID[0]->id , 'id_secHijo' => $idnuevo ]
            );
            DB::commit();

        }
      
        
        
        if(sizeof($matches[0]) == 0)
        {

        }else{
            for ($i = 0 ; $i<sizeof($matches[1]);$i++){
                $cam = html_entity_decode($matches[1][$i], ENT_QUOTES, 'UTF-8');
                // dd($cam);
                DB::beginTransaction();
                DB::table('cambios')->insert(
                    ['id_autor' => $id,'id_seccion'=>$idnuevo,'Titulo'=> $cam, 'Contenido'=>$cam ]
                );
                DB::commit();
            }
        }
       
        return redirect()->route('documentos.index')
            ->with('success','Segmento creato exitosamente');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $iden = Input::get('padre'); 
        $seccion =  DB::table('secciones')->where('id', $iden)->get();
        // $hijos =  DB::table('sec_sec')->where('id_secPadre', $iden)->get();
       

        // $seccion =$seccion[0];
        // $seccionHijo = array();
        // foreach($hijos as $hijo)
        // {
        //     $hijos_id =  DB::table('secciones')->where('id', $hijo->id_secHijo)->get();
            
        //     array_push($seccionHijo, $hijos_id[0]);

        // }

        $cambios =  DB::table('seccioncambio')->where('id_secPadre', $iden)->get();
        $seccion =$seccion[0];
        $secCambios = array();
        foreach($cambios as $cambio)
        {
            $hijos_id =  DB::table('seccioncambio')->where('id', $cambio->id)->get();
            
            array_push($secCambios, $hijos_id[0]);

        }
         
        return view('secciones.show',compact('seccion','secCambios'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $segmento = Segmento::find($id);
        $documentos = Documento::get();
        //dd($documentos);
       
        return view('segmentos.edit',compact('segmento','documentos'));
  
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'Titulo' => 'required|max:255',
            'tipo' => 'required',
            'documentos'=> 'required'
        ]);

        $id_doc = $request->only('documentos');
        $titulo = $request->only('Titulo');
        $tipo = $request->only('tipo');

        $documento = Documento::find($id_doc)->first();
        //dd($documento->id);
        
        DB::table('segmentos')
            ->where('id', $id)
            ->update(['documento_id' => $documento->id,'documento_titulo'=> $documento->Titulo,'Titulo'=>$request->Titulo,'tipo'=>$request->tipo[0]]);
        
        return redirect()->route('segmentos.index')
            ->with('success','Segmento modificado exitosamente');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        Segmento::find($id)->delete();
        return redirect()->route('segmentos.index')
            ->with('success','Segmento eliminado exitosamente ');
    }

    public function buscar(Request $request)
    {
       

        // Gets the query string from our form submission 
            $query = $request->input('search');
            // Returns an array of articles that have the query string located somewhere within 
            // our articles titles. Paginates them so we can break up lots of search results.
            $segmentos = DB::table('segmentos')->where('Titulo', 'LIKE', '%' . $query . '%')->paginate(10);

         
                
            // returns a view and passes the view the list of articles and the original query.
            return view('segmentos.index',compact('segmentos'))
            ->with('i', ($request->input('page', 1) - 1) * 5);

          
    }
}