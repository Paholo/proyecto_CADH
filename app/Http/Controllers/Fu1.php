<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;
use App\Documento;
use App\Segmento;
use App\User;
use DB;

 

  function arbolJerarquia($hijos_id,$contador){
    $hijos_sec =  DB::table('sec_sec')->where('id_secPadre',$hijos_id)->get();
    
    $sec = array();
    foreach($hijos_sec as $hs)
    {
        $hijosSec_id =  DB::table('secciones')->where('id', $hs->id_secHijo)->get();
        array_push($sec, $hijosSec_id[0]);

            $sec1 = array();
            $hijos_secSec =  DB::table('sec_sec')->where('id_secPadre',$hijosSec_id[0]->id)->get();
            foreach($hijos_secSec as $hijo)
              {
                  $hijosSec_id =  DB::table('secciones')->where('id', $hijo->id_secHijo)->get();
                  array_push($sec1, $hijosSec_id[0]);

                  $sec2 =array();
                  $hijos_secSec1 =  DB::table('sec_sec')->where('id_secPadre',$hijosSec_id[0]->id)->get();

                    foreach($hijos_secSec1 as $hijo)
                    {
                        $hijosSec_id =  DB::table('secciones')->where('id', $hijo->id_secHijo)->get();
                        array_push($sec2, $hijosSec_id[0]);

                        $sec3 = array();
                        $hijos_secSec2 =  DB::table('sec_sec')->where('id_secPadre',$hijosSec_id[0]->id)->get();
                        foreach($hijos_secSec2 as $hijo)
                        {
                          $hijosSec_id =  DB::table('secciones')->where('id', $hijo->id_secHijo)->get();
                          array_push($sec3, $hijosSec_id[0]);
                          
                          $sec4= array();
                          $hijos_secSec3 =  DB::table('sec_sec')->where('id_secPadre',$hijosSec_id[0]->id)->get();
                          foreach($hijos_secSec3 as $hijo)
                            {
                              $hijosSec_id =  DB::table('secciones')->where('id', $hijo->id_secHijo)->get();
                              array_push($sec4, $hijosSec_id[0]);

                              $sec5 =array();
                              $hijos_secSec4 =  DB::table('sec_sec')->where('id_secPadre',$hijosSec_id[0]->id)->get();
                              foreach($hijos_secSec4 as $hijo)
                              {
                                $hijosSec_id =  DB::table('secciones')->where('id', $hijo->id_secHijo)->get();
                                array_push($sec5, $hijosSec_id[0]);
                              }
                              if(sizeof($sec5)>0){
                                array_push($sec4, $sec5);
                                }
                            }

                            if(sizeof($sec4)>0){
                              array_push($sec3, $sec4);
                              }

                         }
                    
                      if(sizeof($sec3)>0){
                        array_push($sec2, $sec3);
                        }
                    }
                if(sizeof($sec2)>0){
                  array_push($sec1, $sec2);
                   }
              }
            if(sizeof($sec1)>0){
              array_push($sec, $sec1);
               }
        
        
    }
   
    return $sec;
  }

  function arbolPresentacion($hijos_id,$contador){
    $hijos_sec =  DB::table('sec_sec')->where('id_secPadre',$hijos_id)->select('Contenido_html')->get();
    
    $sec = array();
    foreach($hijos_sec as $hs)
    {
        $hijosSec_id =  DB::table('secciones')->where('id', $hs->id_secHijo)->get();
        array_push($sec, $hijosSec_id[0]);

            $sec1 = array();
            $hijos_secSec =  DB::table('sec_sec')->where('id_secPadre',$hijosSec_id[0]->id)->get();
            foreach($hijos_secSec as $hijo)
              {
                  $hijosSec_id =  DB::table('secciones')->where('id', $hijo->id_secHijo)->get();
                  array_push($sec1, $hijosSec_id[0]);

                  $sec2 =array();
                  $hijos_secSec1 =  DB::table('sec_sec')->where('id_secPadre',$hijosSec_id[0]->id)->get();

                    foreach($hijos_secSec1 as $hijo)
                    {
                        $hijosSec_id =  DB::table('secciones')->where('id', $hijo->id_secHijo)->get();
                        array_push($sec2, $hijosSec_id[0]);

                        $sec3 = array();
                        $hijos_secSec2 =  DB::table('sec_sec')->where('id_secPadre',$hijosSec_id[0]->id)->get();
                        foreach($hijos_secSec2 as $hijo)
                        {
                          $hijosSec_id =  DB::table('secciones')->where('id', $hijo->id_secHijo)->get();
                          array_push($sec3, $hijosSec_id[0]);
                          
                          $sec4= array();
                          $hijos_secSec3 =  DB::table('sec_sec')->where('id_secPadre',$hijosSec_id[0]->id)->get();
                          foreach($hijos_secSec3 as $hijo)
                            {
                              $hijosSec_id =  DB::table('secciones')->where('id', $hijo->id_secHijo)->get();
                              array_push($sec4, $hijosSec_id[0]);

                              $sec5 =array();
                              $hijos_secSec4 =  DB::table('sec_sec')->where('id_secPadre',$hijosSec_id[0]->id)->get();
                              foreach($hijos_secSec4 as $hijo)
                              {
                                $hijosSec_id =  DB::table('secciones')->where('id', $hijo->id_secHijo)->get();
                                array_push($sec5, $hijosSec_id[0]);
                              }
                              if(sizeof($sec5)>0){
                                array_push($sec4, $sec5);
                                }
                            }

                            if(sizeof($sec4)>0){
                              array_push($sec3, $sec4);
                              }

                         }
                    
                      if(sizeof($sec3)>0){
                        array_push($sec2, $sec3);
                        }
                    }
                if(sizeof($sec2)>0){
                  array_push($sec1, $sec2);
                   }
              }
            if(sizeof($sec1)>0){
              array_push($sec, $sec1);
               }
        
        
    }
   
    return $sec;
  }  
?>