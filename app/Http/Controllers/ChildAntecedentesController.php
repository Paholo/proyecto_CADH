<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;
use Auth;
use Illuminate\Http\Request;
use App\Documento;
use App\Segmento;
use App\antecedenteCambio;
use App\cambio;
use App\User;
use Carbon\Carbon;
use DB;

class ChildAntecedentesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $cambios = cambio::orderBy('id','DESC')->paginate(10);
        $documento = array();
        $segmento = array();
        foreach($cambios as $cambio){
            $id_seg =  DB::table('seccioncambio')->where('id', $cambio->id_seccion)->select('Titulo')->get();
            
            array_push($segmento, $id_seg[0]->Titulo);
            // array_push($documento, $id_seg[0]->id_seccion);
        }
        return view('cambios.index',compact('cambios','segmento'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $idPadre = Input::get('iden'); 
        // dd($idPadre);

        $padre  =  DB::table('antecedentecambios')->where('id',(int) $idPadre)->get();
        // $padre = cambio::find($idPadre);
        // dd($padre);
        $padre = $padre[0]->id;
        return view('ChildAntecedentes.create',compact('padre'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $this->validate($request, [
            'serie' => 'required|max:255',
            'nombre' => 'required|max:400',
            'tipo' => 'required',
            'numero' => 'required|max:50',
            'padre' => 'required|max:255',
            'fecha' => 'required|max:255',
            'detalle' => 'required|max:8000',
            'categorias'=>'required',
            'corte'=> 'required'
    
        ]);
        
        // $contra = $request->contra;
        //  dd($request->categorias[1]);
        
        $padre  =  DB::table('antecedentecambios')->where('id',$request->padre)->get();
        $padre_id = $padre[0]->id;
        // dd($padre_id);
        $detalle = $request->detalle;
        
         
         if($detalle =="@[vacio]"){
             $detalle ="";
         }
 
 
         if($request->tipo =="@[vacio]"){
             $request->tipo ="";
         }
 
         if($request->parrafo =="@[vacio]"){
             $request->parrafo ="";
         }

        $newId = DB::table('antecedentecambios')
        ->insertGetId(['Nombre'=> $request->nombre,'Detalle'=> $detalle,'Fecha'=> $request->fecha, 'serie' =>$request->serie,
        'Tipo'=> $request->tipo, 'Numero'=>$request->numero,'Contra'=>$request->contra,'Contenido_Html'=>$request->detalle,'Fecha'=>$request->fecha,
        'Parrafo'=>$request->parrafo,'Corte'=>$request->corte,'added_by'=>Auth::user()->id,'created_at'=>Carbon::now()->format('Y-m-d H:i:s')]);
        
        // dd($newId);

        DB::table('antecedente_antecedente')->insert(['id_antPadre'=>$padre_id,'id_antHijo'=>$newId]);

        foreach ($request->input('categorias') as $key => $value) {
           DB::table('categoriaantecedentes')->insert(['id_antecedente'=>$newId,'categoria'=>$value,'added_by'=>Auth::user()->id,'created_at'=>Carbon::now()->format('Y-m-d H:i:s')]);
        }

      

        $cambio = DB::table('antecedentecambios')->where('id', $padre_id)->get();
        $p = DB::table('antecedente_antecedente')->where('id_antPadre', $padre_id)->select('id_antHijo')->get();
        $antecedentes = array();
        if(sizeof($p) >0 ){
            foreach($p as $hijo)
            {
                $ant =DB::table('antecedentecambios')->where('id', $hijo->id_antHijo)->get();
                $antecedentes[] = $ant[0];
            }
            
        }
        // $antecedentes =DB::table('antecedenteCambios')->where('id_secCambio', $padre_id)->get();
        $cambio = $cambio[0];
        $padre = $padre_id;

        return view('ChildAntecedentes.show',compact('cambio','antecedentes','padre'))->with('success','Antecedente eliminado exitosamente');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $hijo1 = Input::get('hijo1'); 
        
       
        $cambio = DB::table('antecedentecambios')->where('id', $id)->get();
        $p = DB::table('antecedente_antecedente')->where('id_antPadre', $id)->select('id_antHijo')->get();
        $antecedentes = array();
        if(sizeof($p) >0 ){
            foreach($p as $hijo)
            {
                $ant =DB::table('antecedentecambios')->where('id', $hijo->id_antHijo)->get();
                $antecedentes[] = $ant[0];
            }
            
        }

        $padre = DB::table('antecedente_antecedente')->where('id_antHijo', $id)->select('id_antPadre')->get();
        
        if(sizeof($padre)>0){
            $padre = $padre[0]->id_antPadre;
        }else{
            $padre = DB::table('cambio_antecedente')->where('id_antecedente', $id)->select('id_cambio')->get();
            $padre = $padre[0]->id_cambio;
            
        }
        if($hijo1 != null)
        {
            $padre = $hijo1;
        }
       
        $cambio = $cambio[0];
        return view('ChildAntecedentes.show',compact('cambio','antecedentes','padre'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {

        $antecedente  =  DB::table('antecedentecambios')->where('id',$id)->get();
        $p = DB::table('antecedente_antecedente')->where('id_antHijo',$id)->get();
        $padre = DB::table('antecedentecambios')->where('id',$p[0]->id_antHijo)->get();
        $padre = $padre[0]->id;
        $categoria = DB::table('categoriaantecedentes')->where('id_antecedente',$id)->select('categoria')->get();
        $categorias = array();
       for($i =0;$i< sizeof($categoria);$i++)
        {
            $categorias[] = $categoria[$i]->categoria;
        }
        $antecedente = $antecedente[0];
    //    dd(sizeof($categorias));
        return view('ChildAntecedentes.edit',compact('antecedente','categorias','padre'));

  
    }

    // public function administrar($id)
    // {
    //     $cambio = cambio::find($id);
    //     $segmento = Documento::find($cambio->id_segmento);
    //     //dd($documentos);
       
    //     return view('antecedentes.show',compact('segmento','cambio'));
  
    // }
    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */

    public function atras($id){

        $hijo1 = DB::table('antecedente_antecedente')->where('id_antPadre', $id)->select('id_antHijo')->get();
        $actual = DB::table('cambio_antecedente')->where('id_cambio', $id)->select('id_antecedente')->get();
        $ca = false;
        $cambioP = DB::table('cambios')->where('id', $id)->get();
        if(sizeof($cambioP)>0){
                
            $hi = DB::table('cambio_antecedente')->where('id_cambio', $cambioP[0]->id)->select('id_antecedente')->get();
            foreach($hi as $hij)
            {
                
                if($hij->id_antecedente == $actual[0]->id_antecedente)
                {
                    $ca = true;
                }
            }
        }


        if( $ca == true){
          
           
            $cambio = cambio::find($id);
           
            // $p = DB::table('cambio_antecedente')->where('id_cambio', $id)->select('id_cambio')->get();

            $ar  =  DB::table('seccioncambio')->where('id', $cambio->id_seccion)->select('Titulo','id','Autor')->get();
            
            $secTitulo = $ar[0]->Titulo;
            $documento = $ar[0]->id;
            $autor = $ar[0]->Autor;
            $y = DB::table('cambio_antecedente')->where('id_cambio', $id)->select('id_antecedente')->get();
            $antecedentes = array();
            if(sizeof($y) >0 ){
                foreach($y as $hijo)
                {
                    $ant =DB::table('antecedentecambios')->where('id', $hijo->id_antecedente)->get();
                    $antecedentes[] = $ant[0];
                }
                
            }

    
            return view('antecedentes.show',compact('cambio','secTitulo','antecedentes','autor','documento'));
        }

        $cambio = DB::table('antecedentecambios')->where('id', $id)->get();
        $hijo1 = $cambio;
        // dd($hijo1);
        $p = DB::table('antecedente_antecedente')->where('id_antPadre', $hijo1[0]->id)->select('id_antHijo')->get();
        $antecedentes = array();
        if(sizeof($p) >0 ){
            foreach($p as $hijo)
            {
                $ant =DB::table('antecedentecambios')->where('id', $hijo->id_antHijo)->get();
                $antecedentes[] = $ant[0];
            }
            
        }
        $q = DB::table('antecedente_antecedente')->where('id_antHijo', $hijo1[0]->id)->select('id_antPadre')->get();
        $cambio = $cambio[0];
        if(sizeof($q) ==0){
            $r = DB::table('cambio_antecedente')->where('id_antecedente', $hijo1[0]->id)->select('id_cambio')->get();
            $padre = $r[0]->id_cambio;
        }else{
            $padre = $q[0]->id_antPadre;
        }

        return view('ChildAntecedentes.show',compact('cambio','antecedentes','padre'))->with('success','Antecedente eliminado exitosamente');
        
        // dd($cambio);
      

    }
    public function editar(Request $request,$id){
        
        $this->validate($request, [
            'serie' => 'required|max:255',
            'nombre' => 'required|max:400',
            'tipo' => 'required',
            'numero' => 'required|max:50',
            'padre' => 'required|max:255',
            'fecha' => 'required|max:255',
            'detalle' => 'required|max:8000',
            'categorias'=>'required',
            'corte'=> 'required'
    
        ]);
        
        $padre  =  DB::table('antecedentecambios')->where('id',$request->padre)->get();
        $padre_id = $padre[0]->id;

        $detalle = $request->detalle;
        
         if($detalle =="@[vacio]"){
             $detalle ="";
         }
 
     
         if($request->tipo =="@[vacio]"){
             $request->tipo ="";
         }
 
         if($request->parrafo =="@[vacio]"){
             $request->parrafo ="";
         }

        $newId = DB::table('antecedentecambios')->where('id', $id)
        ->update(['Nombre'=> $request->nombre,'Detalle'=> $detalle,'Fecha'=> $request->fecha, 'serie' =>$request->serie,
        'Tipo'=> $request->tipo, 'Numero'=>$request->numero,'Contra'=>$request->contra,'Contenido_Html'=>$request->detalle,'Fecha'=>$request->fecha,
        'Parrafo'=>$request->parrafo,'Corte'=>$request->corte,'updated_by'=>Auth::user()->id,'updated_at'=>Carbon::now()->format('Y-m-d H:i:s')]);
        
        // dd($newId);
        DB::table('categoriaantecedentes')->where('id_antecedente',$id)->delete();
        foreach ($request->input('categorias') as $key => $value) {
           DB::table('categoriaantecedentes')->insert(['id_antecedente'=>$id,'categoria'=>$value,'added_by'=>Auth::user()->id,'created_at'=>Carbon::now()->format('Y-m-d H:i:s')]);
           
        }
        $s = DB::table('antecedente_antecedente')->where('id_antHijo', $padre_id)->select('id_antPadre')->get();
        $cambio = DB::table('antecedentecambios')->where('id', $s[0]->id_antPadre)->get();
        $p = DB::table('antecedente_antecedente')->where('id_antPadre', $s[0]->id_antPadre)->select('id_antHijo')->get();
        $antecedentes = array();
        if(sizeof($p) >0 ){
            foreach($p as $hijo)
            {
                $ant =DB::table('antecedentecambios')->where('id', $hijo->id_antHijo)->get();
                $antecedentes[] = $ant[0];
            }
            
        }
        // $antecedentes =DB::table('antecedenteCambios')->where('id_secCambio', $padre_id)->get();
        $cambio = $cambio[0];
        $padre = $padre_id;

        return view('ChildAntecedentes.show',compact('cambio','antecedentes','padre'))->with('success','Antecedente eliminado exitosamente');
    }
    public function update(Request $request, $id)
    {
        // dd($request);
        // $this->validate($request, [
        //     'Titulo' => 'required|max:255',
        //     'PAT' => 'required',
        //     'PAC' => 'required',
        //     'PAF' => 'required',
        //     'MAT' => 'required',
        //     'MAC' => 'required',
        //     'MAF' => 'required',

        // ]);

        // $id_doc = $request->only('documentos');
        // $titulo = $request->only('Titulo');
        // $tipo = $request->only('tipo');

        // // $id_sec =  DB::table('cambios')->where('id', $id)->select('id_secCambio')->get();
        // // $id_sec=$id_sec[0];
       
        // dd($id);
        // DB::table('primerantecedentes')
        // ->insert(['id_secCambio' => $id,'Titulo'=> $request->PAT,'Contenido'=> $request->PAC,'Fecha'=> $request->PAF]);
        
        // DB::table('miantecedentes')
        // ->insert(['id_secCambio' =>  $id,'Titulo'=> $request->MAT,'Contenido'=> $request->MAC,'Fecha'=> $request->MAF]);

        // return redirect()->route('documentos.index')
        //     ->with('success','Segmento modificado exitosamente');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy(Request $request, $id)
    {
        $idPadre = $request->padre;
        // dd( $idPadre);
        // DB::table('antecedenteCambios')->where('id',$id)->delete();

        // $cambio = DB::table('antecedenteCambios')->where('id', $idPadre)->get();
 
        // $antecedentes =DB::table('antecedenteCambios')->where('id_secCambio', $padre_id)->get();
        // $cambio = $cambio[0];

        $idPadre = $request->padre;
        // dd( $idPadre);
        DB::table('antecedentecambios')->where('id',$id)->delete();

        

        $cambio = DB::table('antecedentecambios')->where('id', $idPadre)->get();
        $p = DB::table('antecedente_antecedente')->where('id_antPadre', $idPadre)->select('id_antHijo')->get();
        $antecedentes = array();
        if(sizeof($p) >0 ){
            foreach($p as $hijo)
            {
                $ant =DB::table('antecedentecambios')->where('id', $hijo->id_antHijo)->get();
                $antecedentes[] = $ant[0];
            }
            
        }

        $padre = DB::table('antecedente_antecedente')->where('id_antHijo', $idPadre)->select('id_antPadre')->get();
        
        if(sizeof($padre)>0){
            $padre = $padre[0]->id_antPadre;
        }else{
            $padre = DB::table('cambio_antecedente')->where('id_antecedente', $idPadre)->select('id_cambio')->get();
            $padre = $padre[0]->id_cambio;
            
        }
       
        $cambio = $cambio[0];
        return view('ChildAntecedentes.show',compact('cambio','antecedentes','padre'));

        
    }

    public function buscar(Request $request)
    {
       

        // Gets the query string from our form submission 
            $query = $request->input('search');
            
            $segmentos = DB::table('segmentos')->where('Titulo', 'LIKE', '%' . $query . '%')->paginate(10);

         
                
            // returns a view and passes the view the list of articles and the original query.
            return view('segmentos.index',compact('segmentos'))
            ->with('i', ($request->input('page', 1) - 1) * 5);

          
    }
}