<?php
namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;
use App\Documento;
use App\Segmento;
use App\cambio;
use App\User;
use DB;

class CambiosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $cambios = cambio::orderBy('id','DESC')->paginate(10);
        $documento = array();
        $segmento = array();
        foreach($cambios as $cambio){
            $id_seg =  DB::table('seccioncambio')->where('id', $cambio->id_seccion)->select('Titulo')->get();
            
            array_push($segmento, $id_seg[0]->Titulo);
            // array_push($documento, $id_seg[0]->id_seccion);
        }
        return view('cambios.index',compact('cambios','segmento'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
    //     $documentos = Documento::pluck('Titulo','id');
    //     return view('segmentos.create',compact('documentos')); 
    //   // return view('segmentos.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        // $this->validate($request, [
        //     'Titulo' => 'required|max:255',
        //     'tipo' => 'required',
        //     'documentos'=>'required',
        //     'Contenido'=>'required'

        // ]);
        // $docID = $request->only('documentos');
        // $documento = Documento::find($docID)->first();
        // // $usuario = User::find()
        // $nombre = Auth::user()->name;
        // $id = Auth::user()->id;
        // $texto = $request->Contenido;
        // $patron = '#<span class="CADH">(.*?)</span>#s';
       
        // $filtro =   preg_match_all ($patron, $texto, $matches);
        // // dd($matches);
        // // dd(html_entity_decode($matches[1][0]));
        // // dd(sizeof($matches[1]));
        // DB::beginTransaction();
        // DB::table('segmentos')->insert(
        //     ['documento_id' => $documento->id, 'documento_titulo'=>$documento->Titulo,'Titulo'=> $request->Titulo , 'tipo'=>$request->tipo, 'contenido'=>$request->Contenido, 'id_Autor'=> $id, 'NombreAutor'=>$nombre ]
        // );
      // $id_seg =  DB::table('segmentos')->where('documento_id', $documento->id)->where('Titulo', $request->Titulo)->select('id')->get();
        // // dd($id_seg[0]->id);
        // for ($i = 0 ; $i<sizeof($matches[1]);$i++){
        //     $cam = html_entity_decode($matches[1][$i], ENT_QUOTES, 'UTF-8');
        //     // dd($cam);
        //     DB::table('cambios')->insert(
        //         ['id_autor' => $id,'id_segmento'=>$id_seg[0]->id,'Titulo'=> $cam, 'Contenido'=>$cam ]
        //     );
        // }
        
        // DB::commit();
        // return redirect()->route('segmentos.index')
        //     ->with('success','Segmento creato exitosamente');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $cambio = cambio::find($id);
        $ar  =  DB::table('seccioncambio')->where('id', $cambio->id_seccion)->select('Titulo','id','Autor','id_secPadre')->get();
        $secTitulo = $ar[0]->Titulo;
        $documento = $ar[0]->id;
        $autor = $ar[0]->Autor;
       
        $padre  =  DB::table('sec_sec')->where('id_secHijo', $ar[0]->id_secPadre)->select('id_secPadre')->get();

        $sec  =  DB::table('secciones')->where('id', $padre[0]->id_secPadre)->select('Titulo','id')->get();

        $sec = $sec[0];

     
        $p = DB::table('cambio_antecedente')->where('id_cambio', $id)->select('id_antecedente')->get();
        $antecedentes = array();
        if(sizeof($p) >0 ){
            foreach($p as $hijo)
            {
                $ant =DB::table('antecedentecambios')->where('id', $hijo->id_antecedente)->get();
                $antecedentes[] = $ant[0];
            }
            
        }
        $cat =array();

        foreach($antecedentes as $ant){
            $categoria = DB::table('categoriaantecedentes')->where('id_antecedente',$ant->id)->select('categoria')->get();
            $categorias = array();
           for($i =0;$i< sizeof($categoria);$i++)
            {
                $categorias[] = $categoria[$i]->categoria;
            }
            // dd($categorias);
            $cat[] = $categorias;
        }
       
        // dd($cat);
     

        return view('cambios.show',compact('cambio','secTitulo','antecedentes','sec','autor','documento','cat'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $cambio = cambio::find($id);
        $segmento = Documento::find($cambio->id_segmento);
        //dd($documentos);
       
        return view('cambios.edit',compact('segmento','cambio'));
  
    }

    public function administrar($id)
    {
        $cambio = cambio::find($id);
        $segmento = Documento::find($cambio->id_segmento);
        //dd($documentos);
       
        return view('antecedentes.show',compact('segmento','cambio'));
  
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'Titulo' => 'required|max:255',
            'PAT' => 'required',
            'PAC' => 'required',
            'PAF' => 'required',
            'MAT' => 'required',
            'MAC' => 'required',
            'MAF' => 'required',

        ]);

        $id_doc = $request->only('documentos');
        $titulo = $request->only('Titulo');
        $tipo = $request->only('tipo');

        // $id_sec =  DB::table('cambios')->where('id', $id)->select('id_secCambio')->get();
        // $id_sec=$id_sec[0];
       
        dd($id);
        DB::table('primerantecedentes')
        ->insert(['id_secCambio' => $id,'Titulo'=> $request->PAT,'Contenido'=> $request->PAC,'Fecha'=> $request->PAF]);
        
        DB::table('miantecedentes')
        ->insert(['id_secCambio' =>  $id,'Titulo'=> $request->MAT,'Contenido'=> $request->MAC,'Fecha'=> $request->MAF]);

        return redirect()->route('documentos.index')
            ->with('success','Segmento modificado exitosamente');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        Segmento::find($id)->delete();
        return redirect()->route('segmentos.index')
            ->with('success','Segmento eliminado exitosamente ');
    }

    

    public function buscar(Request $request)
    {
       

        // Gets the query string from our form submission 
            $query = $request->input('search');
            // Returns an array of articles that have the query string located somewhere within 
            // our articles titles. Paginates them so we can break up lots of search results.
            $segmentos = DB::table('segmentos')->where('Titulo', 'LIKE', '%' . $query . '%')->paginate(10);

         
                
            // returns a view and passes the view the list of articles and the original query.
            return view('segmentos.index',compact('segmentos'))
            ->with('i', ($request->input('page', 1) - 1) * 5);

          
    }
}