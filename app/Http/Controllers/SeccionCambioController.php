<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;
use Auth;
use Illuminate\Http\Request;
use App\Documento;
use App\Segmento;
use App\Seccione;
use App\Cambio;
use App\User;
use DB;

class SeccionCambioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        // $secciones = Seccione::orderBy('id','DESC')->paginate(10);
        $secciones = DB::table('seccioncambios')->orderBy('id','DESC')->get();
       
        return view('secciones.index',compact('secciones'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $documento = Input::get('titulo'); 
        $iden = Input::get('padre');  
        $seccionPadre =  DB::table('secciones')->where('id', $iden)->where('Titulo', $documento)->select()->get();
        $seccionPadre = $seccionPadre[0];
        // dd($seccionPadre);
        return view('seccionCambios.create',compact('seccionPadre')); //return the view with the list of roles passed as an array
      
    }
    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'Titulo' => 'required|max:255',
            'tipo' => 'required',
            'Titulo_Padre'=>'required',
            'Contenido'=>'required'

        ]);
        $id_padre = intval($request->padre);
        $documento = $request->only('Titulo_Padre');
        $seccionID =  DB::table('secciones')->where('id', $id_padre)->where('Titulo', $documento)->select('id')->get();
        // dd($seccionID);
        $nombre = Auth::user()->name;
        $id = Auth::user()->id;
        $texto = $request->Contenido;
        
        $patron = '#<span class="CADH">(.*?)</span>#s';
        $filtro =   preg_match_all ($patron, $texto, $matches);
        $tclean = strip_tags($texto);
        // dd($tclean);
        $idnuevo = "";
      
            DB::beginTransaction();
            $idnuevo = DB::table('seccioncambio')->insertGetId(
                ['id_SecPadre'=>$seccionID[0]->id,'Titulo'=> $request->Titulo , 'tipo'=>$request->tipo, 'Contenido'=>$tclean,'Contenido_html'=>$request->Contenido, 'added_by'=> $id, 'Autor'=>$request->Autor ]
            );
            DB::commit();
    
        // dd($matches[1]);
        if(sizeof($matches[0]) == 0)
        {

        }else{
            $pos =0;
            for ($i = 0 ; $i<sizeof($matches[1]);$i++){
                $cam = html_entity_decode($matches[1][$i], ENT_QUOTES, 'UTF-8');
                // dd($cam);
                DB::beginTransaction();
               $idCambio=  DB::table('cambios')->insertGetId(
                    ['id_autor' => $id,'id_seccion'=>$idnuevo,'Titulo'=> $cam, 'Contenido'=>$cam ]
                );
                $mystring = $texto;
                $findme   = 'class="CADH"';
                $pos2 = strpos($mystring, $findme,$pos + strlen($findme)); 
                $pos = $pos2;
                $texto = substr_replace($mystring,"id='t".$idCambio."'",$pos,0);
                DB::table('seccioncambio')
                ->where('id', $idnuevo)
                ->update(['Contenido_html' => $texto]);
                DB::commit();
            }
        }
       
        return redirect()->route('documentos.index')
            ->with('success','Segmento-Cambio creato exitosamente');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
    
        $seccion = DB::table('seccioncambio')->where('id', $id)->get();
        $seccion = $seccion[0];
        $cambios= DB::table('cambios')->where('id_seccion', $id)->get();
    
         
        return view('seccionCambios.show',compact('seccion','cambios'));
    }

    public function atras($id){
        
        
        $seccion =  DB::table('secciones')->where('id', $id)->get();


        $cambios =  DB::table('seccioncambio')->where('id_secPadre', $id)->get();
        $seccion =$seccion[0];
        $secCambios = array();
        foreach($cambios as $cambio)
        {
            $hijos_id =  DB::table('seccioncambio')->where('id', $cambio->id)->get();
            
            array_push($secCambios, $hijos_id[0]);

        }
         
        return view('secciones.show',compact('seccion','secCambios'));
        
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $seccion = DB::table('seccioncambio')->where('id', $id)->select('id')->get();
        // $documentos = Documento::get();
        //dd($documentos);
       
        return view('seccionCambio.edit',compact('seccion'));
  
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'Titulo' => 'required|max:255',
            'tipo' => 'required',
            'documentos'=> 'required'
        ]);

        $id_doc = $request->only('documentos');
        $titulo = $request->only('Titulo');
        $tipo = $request->only('tipo');

        $documento = Documento::find($id_doc)->first();
        //dd($documento->id);
        
        DB::table('segmentos')
            ->where('id', $id)
            ->update(['documento_id' => $documento->id,'documento_titulo'=> $documento->Titulo,'Titulo'=>$request->Titulo,'tipo'=>$request->tipo[0]]);
        
        return redirect()->route('seccionCambios.index')
            ->with('success','Segmento modificado exitosamente');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        Segmento::find($id)->delete();
        return redirect()->route('segmentos.index')
            ->with('success','Segmento eliminado exitosamente ');
    }

    public function buscar(Request $request)
    {
       

        // Gets the query string from our form submission 
            $query = $request->input('search');
            // Returns an array of articles that have the query string located somewhere within 
            // our articles titles. Paginates them so we can break up lots of search results.
            $segmentos = DB::table('segmentos')->where('Titulo', 'LIKE', '%' . $query . '%')->paginate(10);

         
                
            // returns a view and passes the view the list of articles and the original query.
            return view('segmentos.index',compact('segmentos'))
            ->with('i', ($request->input('page', 1) - 1) * 5);

          
    }
}