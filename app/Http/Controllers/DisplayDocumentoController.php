<?php
namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;
use App\Documento;
use App\Segmento;
use App\User;
use DB;

class DisplayDocumentoController extends Controller
{

    public function index(Request $request)
    {
        $documentos = Documento::orderBy('id','DESC')->paginate(10);
        return view('displayDocumento.index',compact('documentos'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function show($id)
    {
    
        // $seccion = DB::table('seccioncambio')->where('id', $id)->get();
        // $seccion = $seccion[0];
        // $cambios= DB::table('cambios')->where('id_seccion', $id)->get();
    
         
        // return view('displayCambio.show',compact('seccion','cambios'));


        $cambiosIDs = array(37,39,40,42,47,45,71,70,50,51,59,60,48,65,52,67,53,54,49,56,62,66,57,64,68,58);
        
        $aCat = array();
        $aAntecedentes = array();

        
        $seccion = DB::table('seccionCambio')->where('id', 37)->get();
        $seccion = $seccion[0];
        foreach($cambiosIDs as $camID){
            $cambios= DB::table('cambios')->where('id_seccion', $camID)->get();
            $antecedentes = array();
            $categorias = array();
            foreach($cambios as $camb){
    
                $p = DB::table('cambio_antecedente')->where('id_cambio', $camb->id)->select('id_antecedente')->get();
                $antecedentes1 = array();
                if(sizeof($p) >0 ){
                    $antecedentes1[] = $camb->id;
                    foreach($p as $hijo)
                    {
                        $ant =DB::table('antecedenteCambios')->where('id', $hijo->id_antecedente)->get();
                        $antecedentes1[] = $ant[0];
                        // $cont++;
                    }
                $antecedentes[] = $antecedentes1;
                $cat =array();
                        
                        foreach($antecedentes1 as $ant){
                            if((String)gettype($ant) != 'integer'){
                                $cat[]= $ant->id;
                                $categoriaA = DB::table('categoriaantecedentes')->where('id_antecedente',$ant->id)->select('categoria')->get();
                                $categoriaAs = array();
                               for($i =0;$i< sizeof($categoriaA);$i++)
                                {
                                    $categoriaAs[] = $categoriaA[$i]->categoria;
                                }
                                // dd($categorias);
                                $cat[] = $categoriaAs;
                            }
                           
                        }
                        $categorias[] = $cat;
            
                     }
                    
        }

        $aAntecedentes[] = $antecedentes;
        $aCat[] =$categorias;
        
    }
            
        
        
        // dd($aCat);
       
        dd($aAntecedentes);
     

        return view('displayDocumento.show',compact('seccion','cambios','aAntecedentes','aCat'));

    }

    

    

}