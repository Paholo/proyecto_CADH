<?php
namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;
use App\Documento;
use App\Segmento;
use App\User;
use DB;

class DocumentoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $documentos = Documento::orderBy('id','DESC')->paginate(10);
        return view('documentos.index',compact('documentos'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //$roles = Role::pluck('display_name','id');
       // return view('users.create',compact('roles')); //return the view with the list of roles passed as an array
       return view('documentos.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'Titulo' => 'required|max:255',
            'tipo' => 'required',
    
        ]);
        $id = Auth::user()->id;
        $input = $request->only('Titulo','tipo','Contenido');
        $texto = $request->Contenido;
        $patron = '#<span class="CADH">(.*?)</span>#s';
        $filtro =   preg_match_all ($patron, $texto, $matches);
        $tclean = strip_tags ($request->Contenido);
        //  dd( strlen ($tclean));
        DB::table('documentos')->insert(
            ['Titulo'=> $request->Titulo , 'tipo'=>$request->tipo, 'Contenido_html'=>$request->Contenido, 'Contenido'=> $tclean, 'added_by'=> $id ]
        );

        
        // $documento = Documento::create($input); //Create User table entry
        //Attach the selected Roles
      
        return redirect()->route('documentos.index')
            ->with('success','Documento creado exitosamente');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */

    public function show($id)
    {
        $documento = Documento::find($id);
        $hijos =  DB::table('doc_sec')->where('id_doc', $id)->get();
        $seccion = array();
        $seccionesHijo = array();
        $contador = 0;
        include 'Fu1.php';

        foreach($hijos as $hijo)
        {
            $div = array();
            array_push($seccion, $div);
            $hijos_id =  DB::table('secciones')->where('id', $hijo->id_secHijo)->get();
            array_push($seccion[$contador], $hijos_id[0]);

            $arbol = arbolJerarquia($hijos_id[0]->id,$contador);
            
            if(sizeof($arbol)>0){
                array_push($seccion[$contador], $arbol);
            }
            
            $contador++;

        }
        // dd($seccion);
        return view('documentos.show',compact('documento','seccion'));
    }

    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $documento = Documento::find($id);
       // $roles = Role::get(); //get all roles
      //  $userRoles = $user->roles->pluck('id')->toArray();
        return view('documentos.edit',compact('documento'));
     // return view('documentos.edit');
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'Titulo' => 'required|max:255',
            'tipo' => 'required',
        ]);

        $input = $request->only('Titulo', 'tipo');
       
        // $documento = Documento::find($id);
        // $documento->update($input); //update the documento info
        
        DB::table('documentos')
        ->where('id', $id)
        ->update( ['Titulo'=> $request->Titulo , 'tipo'=>$request->tipo, 'Contenido'=>$request->Contenido, 'Contenido_html'=>htmlspecialchars_decode($request->Contenido), 'updated_by'=> $id ]);

        return redirect()->route('documentos.index')
            ->with('success','Documento modificado exitosamente');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        documento::find($id)->delete();
        return redirect()->route('documentos.index')
            ->with('success','Documento eliminado exitosamente ');
    }

    public function buscar(Request $request)
    {
       

        // Gets the query string from our form submission 
            $query = $request->input('search');
            // Returns an array of articles that have the query string located somewhere within 
            // our articles titles. Paginates them so we can break up lots of search results.
            $documentos = DB::table('documentos')->where('Titulo', 'LIKE', '%' . $query . '%')->paginate(10);

         
                
            // returns a view and passes the view the list of articles and the original query.
            return view('documentos.index',compact('documentos'))
            ->with('i', ($request->input('page', 1) - 1) * 5);

          
    }
}