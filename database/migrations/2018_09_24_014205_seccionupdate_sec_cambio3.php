<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SeccionupdateSecCambio3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seccioncambio', function($table) {    
            $table->increments('id');
            $table->string('Titulo', 2000);
            $table->string('Contenido', 2000);
            $table->string('Contenido_html', 2000);
            $table->string('Tipo', 2000)->default("no establecido");
            $table->string('Autor', 2000)->default("no establecido");
            $table->integer('added_by')->unsigned();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->timestamps();
        
        });

        Schema::create('cambios', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_autor');
            $table->unsignedInteger('id_seccion');
            $table->string('Titulo', 2000);
            $table->string('Contenido', 2000);
            $table->timestamps();
            $table->foreign('id_seccion')->references('id')->on('seccioncambio')
            ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
