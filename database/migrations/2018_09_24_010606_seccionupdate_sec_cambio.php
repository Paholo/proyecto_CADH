<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SeccionupdateSecCambio extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('secciones', function($table) {       
            $table->dropColumn('Autor');      
           
           
             
         });

         Schema::table('secciones', function($table) {       
              
            $table->string('Autor', 2000)->default("Original");
           
             
         });
        
         Schema::create('seccion_cambio', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_autor');
            $table->unsignedInteger('id_seccion');
            $table->string('Titulo', 2000);
            $table->string('Contenido', 2000);
            $table->timestamps();
            $table->foreign('id_seccion')->references('id')->on('secciones')
            ->onUpdate('cascade')->onDelete('cascade');
        });
        
        Schema::create('cambios', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_autor');
            $table->unsignedInteger('id_seccion');
            $table->string('Titulo', 2000);
            $table->string('Contenido', 2000);
            $table->timestamps();
            $table->foreign('id_seccion')->references('id')->on('seccion_cambio')
            ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
