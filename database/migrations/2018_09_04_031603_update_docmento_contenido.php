<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDocmentoContenido extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('documentos', function($table) {
            
         
               
              $table->string('Contenido', 2000)->after('tipo');
               
           });

           Schema::table('segmentos', function($table) {
            
         
               
              $table->string('Contenido', 2000)->after('tipo');
               
           });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
