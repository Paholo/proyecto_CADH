<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Antecedentes23 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::beginTransaction();

        Schema::create('antecedenteCambios', function($table) {    
            $table->increments('id');
            $table->unsignedInteger('id_secCambio');
            $table->string('Detalle', 2000);
            $table->string('Contenido_Html', 2000);
            $table->string('Nombre', 200);
            $table->string('Contra', 200)->nullable();
            $table->string('Tipo', 100);
            $table->string('Serie', 100);
            $table->integer('Numero');
            $table->string('Fecha',200);
            $table->string('Parrafo',200);
            $table->string('Corte',200);
            $table->integer('added_by')->unsigned();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->timestamps();
            $table->foreign('id_secCambio')->references('id')->on('cambios')
            ->onUpdate('cascade')->onDelete('cascade');
        });   
        
            Schema::create('categoriaAntecedentes', function($table) {    
                $table->increments('id');
                $table->unsignedInteger('id_antecedente');
                $table->string('categoria', 300);
                $table->integer('added_by')->unsigned();
                $table->integer('updated_by')->unsigned()->nullable();
                $table->timestamps();
                $table->foreign('id_antecedente')->references('id')->on('antecedenteCambios')
                ->onUpdate('cascade')->onDelete('cascade');
            });   
        
        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
