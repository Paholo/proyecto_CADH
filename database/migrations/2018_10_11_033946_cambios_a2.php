<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CambiosA2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('antecedente_antecedente', function($table) {    
            $table->increments('id');
            $table->integer('id_antPadre')->unsigned();
            $table->integer('id_antHijo')->unsigned();
            $table->foreign('id_antPadre')->references('id')->on('antecedenteCambios')
            ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_antHijo')->references('id')->on('antecedenteCambios')
            ->onUpdate('cascade')->onDelete('cascade');
        });   
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
