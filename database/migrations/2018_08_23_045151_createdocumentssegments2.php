<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Createdocumentssegments2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::beginTransaction();

        Schema::create('documentos', function (Blueprint $table) {
            $table->increments('id');
            $table->text('Titulo');
            $table->string('tipo', 100);
            $table->timestamps();
        });


        Schema::create('segmentos', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('documento_id');
            $table->text('Titulo');
            $table->string('tipo', 100);
            $table->timestamps();

       
            $table->foreign('documento_id')->references('id')->on('documentos')
            ->onUpdate('cascade')->onDelete('cascade');
        });

        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documentos');
        Schema::dropIfExists('segmentos');
    }
}
