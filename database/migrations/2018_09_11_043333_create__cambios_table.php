<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCambiosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::beginTransaction();
        // Schema::create('cambios', function (Blueprint $table) {
        //     $table->increments('id');
        //     $table->unsignedInteger('id_autor');
        //     $table->unsignedInteger('id_segmento');
        //     $table->string('Titulo', 2000);
        //     $table->string('Contenido', 2000);
        //     $table->timestamps();
        //     $table->foreign('id_segmento')->references('id')->on('segmentos')
        //     ->onUpdate('cascade')->onDelete('cascade');
        // });

        // Schema::table('segmentos', function($table) {    
        //     $table->string('id_Autor', 2000)->after('documento_titulo')->nullable($value = true);
        //     $table->string('NombreAutor', 2000)->after('tipo');
               
        //    });

        Schema::create('primerAntecedentes', function($table) {    
            $table->increments('id');
            $table->unsignedInteger('id_segmento');
            $table->string('Titulo', 2000);
            $table->string('Contenido', 2000);
            $table->string('Fecha', 2000);
            $table->foreign('id_segmento')->references('id')->on('segmentos')
            ->onUpdate('cascade')->onDelete('cascade');
        });   

        Schema::create('mIantecedentes', function($table) {    
            $table->increments('id');
            $table->unsignedInteger('id_segmento');
            $table->string('Titulo', 2000);
            $table->string('Contenido', 2000);
            $table->string('Fecha', 2000);
            $table->foreign('id_segmento')->references('id')->on('segmentos')
            ->onUpdate('cascade')->onDelete('cascade');
        }); 

        Schema::create('ultimoAntecedentes', function($table) {    
            $table->increments('id');
            $table->unsignedInteger('id_segmento');
            $table->string('Titulo', 2000);
            $table->string('Contenido', 2000);
            $table->string('Fecha', 2000);
            $table->foreign('id_segmento')->references('id')->on('segmentos')
            ->onUpdate('cascade')->onDelete('cascade');
        }); 

        Schema::create('cambioJurisprudencias', function($table) {    
            $table->increments('id');
            $table->unsignedInteger('id_segmento');
            $table->string('Titulo', 2000);
            $table->string('Contenido', 2000);
            $table->string('Fecha', 2000);
            $table->foreign('id_segmento')->references('id')->on('segmentos')
            ->onUpdate('cascade')->onDelete('cascade');
        }); 

        Schema::create('origenFueras', function($table) {    
            $table->increments('id');
            $table->unsignedInteger('id_segmento');
            $table->string('Titulo', 2000);
            $table->string('Contenido', 2000);
            $table->string('Fecha', 2000);
            $table->foreign('id_segmento')->references('id')->on('segmentos')
            ->onUpdate('cascade')->onDelete('cascade');
        }); 

        Schema::create('precedenteIntermedios', function($table) {    
            $table->increments('id');
            $table->unsignedInteger('id_segmento');
            $table->string('Titulo', 2000);
            $table->string('Contenido', 2000);
            $table->string('Fecha', 2000);
            $table->foreign('id_segmento')->references('id')->on('segmentos')
            ->onUpdate('cascade')->onDelete('cascade');
        }); 

        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_cambios');
    }
}
