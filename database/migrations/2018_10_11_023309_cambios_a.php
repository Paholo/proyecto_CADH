<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CambiosA extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::beginTransaction();
        
                Schema::create('cambio_antecedente', function($table) {    
                    $table->increments('id');
                    $table->integer('id_cambio')->unsigned();
                    $table->integer('id_antecedente')->unsigned();
                    $table->foreign('id_cambio')->references('id')->on('cambios')
                    ->onUpdate('cascade')->onDelete('cascade');
                    $table->foreign('id_antecedente')->references('id')->on('antecedenteCambios')
                    ->onUpdate('cascade')->onDelete('cascade');
                });   

                Schema::create('antecedenteHijo', function($table) {    
                    $table->increments('id');
                    $table->string('Detalle', 2000);
                    $table->string('Contenido_Html', 2000);
                    $table->string('Nombre', 200);
                    $table->string('Contra', 200)->nullable();
                    $table->string('Tipo', 100);
                    $table->string('Serie', 100);
                    $table->integer('Numero');
                    $table->string('Fecha',200);
                    $table->string('Parrafo',200);
                    $table->string('Corte',200);
                    $table->integer('added_by')->unsigned();
                    $table->integer('updated_by')->unsigned()->nullable();
                    $table->timestamps();
                });   
                
                    Schema::create('antecedente_antecedente', function($table) {    
                        $table->increments('id');
                        $table->integer('id_antPadre')->unsigned();
                        $table->integer('id_antHijo')->unsigned();
                        $table->foreign('id_antPadre')->references('id')->on('antecedenteCambios')
                        ->onUpdate('cascade')->onDelete('cascade');
                        $table->foreign('id_antHijo')->references('id')->on('antecedenteHijo')
                        ->onUpdate('cascade')->onDelete('cascade');
                    });   
                
                DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
