<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Seccion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::beginTransaction();
        Schema::create('secciones', function($table) {    
            $table->increments('id');
            $table->unsignedInteger('id_seccionPadre');
            $table->string('Titulo', 2000);
            $table->string('Contenido', 2000);
            $table->string('Contenido_html', 2000);
            $table->string('Tipo', 2000)->default("no establecido");
            $table->string('Autor', 2000)->default("no establecido");
            $table->integer('added_by')->unsigned();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->timestamps();
            $table->foreign('id_seccionPadre')->references('id')->on('secciones')
            ->onUpdate('cascade')->onDelete('cascade');
        });
        DB::commit();
        DB::beginTransaction();
        Schema::table('documentos', function($table) {             
              $table->string('Contenido_html', 2000)->after('tipo');
              $table->integer('added_by')->unsigned()->after('Contenido');;
              $table->integer('updated_by')->unsigned()->nullable()->after('added_by');
               
           });
        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
