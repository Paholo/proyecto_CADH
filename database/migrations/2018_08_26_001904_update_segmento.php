<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateSegmento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('segmentos', function($table) {
         
      
            $table->dropColumn('Titulo');
           // $table->string('Titulo', 100)->after('documento_titulo')->unique();
            
        });

        Schema::table('documentos', function($table) {
           
            $table->dropColumn('Titulo');
           // $table->string('Titulo', 100)->after('id')->unique();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       
        
    }
}
