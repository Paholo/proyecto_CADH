<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComentarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::beginTransaction();
        
                // Create table for storing roles
                Schema::create('Comentarios_Cambios', function (Blueprint $table) {
                    $table->increments('id');
                    $table->unsignedInteger('user_id');
                    $table->text('contenido');
                    $table->timestamps();

                    $table->foreign('user_id')->references('id')->on('users');
                });
        
                Schema::table('users', function($table) {
                    $table->boolean('activado')->default(true)->after('password');
                });
        
            
        
                DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Comentarios_Cambios');

        Schema::table('users', function($table) {
            $table->dropColumn('completename');
        });
    }
}
