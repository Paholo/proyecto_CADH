<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Cambioup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::beginTransaction();
        Schema::create('cambios', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_autor');
            $table->unsignedInteger('id_seccion');
            $table->string('Titulo', 2000);
            $table->string('Contenido', 2000);
            $table->timestamps();
            $table->foreign('id_seccion')->references('id')->on('secciones')
            ->onUpdate('cascade')->onDelete('cascade');
        });

       
        Schema::create('primerAntecedentes', function($table) {    
            $table->increments('id');
            $table->unsignedInteger('id_seccion');
            $table->string('Titulo', 2000);
            $table->string('Contenido', 2000);
            $table->string('Fecha', 2000);
            $table->foreign('id_seccion')->references('id')->on('secciones')
            ->onUpdate('cascade')->onDelete('cascade');
        });   

        Schema::create('mIantecedentes', function($table) {    
            $table->increments('id');
            $table->unsignedInteger('id_seccion');
            $table->string('Titulo', 2000);
            $table->string('Contenido', 2000);
            $table->string('Fecha', 2000);
            $table->foreign('id_seccion')->references('id')->on('secciones')
            ->onUpdate('cascade')->onDelete('cascade');
        }); 

        Schema::create('ultimoAntecedentes', function($table) {    
            $table->increments('id');
            $table->unsignedInteger('id_seccion');
            $table->string('Titulo', 2000);
            $table->string('Contenido', 2000);
            $table->string('Fecha', 2000);
            $table->foreign('id_seccion')->references('id')->on('secciones')
            ->onUpdate('cascade')->onDelete('cascade');
        }); 

        Schema::create('cambioJurisprudencias', function($table) {    
            $table->increments('id');
            $table->unsignedInteger('id_seccion');
            $table->string('Titulo', 2000);
            $table->string('Contenido', 2000);
            $table->string('Fecha', 2000);
            $table->foreign('id_seccion')->references('id')->on('secciones')
            ->onUpdate('cascade')->onDelete('cascade');
        }); 

        Schema::create('origenFueras', function($table) {    
            $table->increments('id');
            $table->unsignedInteger('id_seccion');
            $table->string('Titulo', 2000);
            $table->string('Contenido', 2000);
            $table->string('Fecha', 2000);
            $table->foreign('id_seccion')->references('id')->on('secciones')
            ->onUpdate('cascade')->onDelete('cascade');
        }); 

        Schema::create('precedenteIntermedios', function($table) {    
            $table->increments('id');
            $table->unsignedInteger('id_seccion');
            $table->string('Titulo', 2000);
            $table->string('Contenido', 2000);
            $table->string('Fecha', 2000);
            $table->foreign('id_seccion')->references('id')->on('secciones')
            ->onUpdate('cascade')->onDelete('cascade');
        }); 

        DB::commit();
       

     
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
