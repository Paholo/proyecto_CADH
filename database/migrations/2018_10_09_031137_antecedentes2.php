<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Antecedentes2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::beginTransaction();
        
            Schema::create('categoriaAntecedente', function($table) {    
                $table->increments('id');
                $table->unsignedInteger('id_antecedente');
                $table->string('categoria', 300);
                $table->integer('added_by')->unsigned();
                $table->integer('updated_by')->unsigned()->nullable();
                $table->timestamps();
                $table->foreign('id_antecedente')->references('id')->on('antecedentecambios')
                ->onUpdate('cascade')->onDelete('cascade');
            });   
        
        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
