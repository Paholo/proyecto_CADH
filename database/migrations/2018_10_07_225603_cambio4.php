<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Cambio4 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::beginTransaction();
        
    
           
            Schema::create('antecedenteCambio', function($table) {    
                $table->increments('id');
                $table->unsignedInteger('id_secCambio');
                $table->string('Detalle', 2000);
                $table->string('Contenido_Html', 2000);
                $table->string('Nombre', 200);
                $table->string('Contra', 200)->nullable();
                $table->string('Tipo', 100);
                $table->string('Serie', 100);
                $table->integer('Numero');
                $table->string('Fecha',200);
                $table->string('Parrafo',200);
                $table->string('Corte',200);
                $table->integer('added_by')->unsigned();
                $table->integer('updated_by')->unsigned()->nullable();
                $table->timestamps();
                $table->foreign('id_secCambio')->references('id')->on('seccioncambio')
                ->onUpdate('cascade')->onDelete('cascade');
            });   
        
        DB::commit();
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
