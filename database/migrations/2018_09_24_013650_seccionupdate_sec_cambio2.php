<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SeccionupdateSecCambio2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //.
        Schema::create('seccionCambio', function($table) {    
            $table->increments('id');
            $table->string('Titulo', 2000);
            $table->string('Contenido', 2000);
            $table->string('Contenido_html', 2000);
            $table->string('Tipo', 2000)->default("no establecido");
            $table->string('Autor', 2000)->default("no establecido");
            $table->integer('added_by')->unsigned();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->timestamps();
        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
