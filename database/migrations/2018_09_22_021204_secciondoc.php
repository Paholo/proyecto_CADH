<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Secciondoc extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       

         Schema::create('secciones', function($table) {    
            $table->increments('id');
            $table->string('Titulo', 2000);
            $table->string('Contenido', 2000);
            $table->string('Contenido_html', 2000);
            $table->string('Tipo', 2000)->default("no establecido");
            $table->string('Autor', 2000)->default("no establecido");
            $table->integer('added_by')->unsigned();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->timestamps();
        
        });

         Schema::create('doc_sec', function($table) {    
            $table->increments('id');
            $table->unsignedInteger('id_doc');
            $table->unsignedInteger('id_secHijo');
            $table->foreign('id_doc')->references('id')->on('documentos')
            ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_secHijo')->references('id')->on('secciones')
            ->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::create('sec_sec', function($table) {    
            $table->increments('id');
            $table->unsignedInteger('id_secPadre');
            $table->unsignedInteger('id_secHijo');
            $table->foreign('id_secPadre')->references('id')->on('secciones')
            ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_secHijo')->references('id')->on('secciones')
            ->onUpdate('cascade')->onDelete('cascade');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
