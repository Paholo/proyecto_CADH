<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// 

Route::get('displayAntecedenteAnt/search', 'DisplayAntecedenteAntController@buscar');
Route::resource('displayAntecedenteAnt','DisplayAntecedenteAntController');

Route::get('displayAntecedente/search', 'DisplayAntecedenteController@buscar');
Route::resource('displayAntecedente','DisplayAntecedenteController');

Route::get('displayCambio/search', 'DisplayCambioController@buscar');
Route::resource('displayCambio','DisplayCambioController');

Route::get('displayDocumento/search', 'DisplayDocumentoController@buscar');
Route::resource('displayDocumento','DisplayDocumentoController');

Route::group(['prefix' => 'admin', 'middleware' => ['role:admin|Editor']], function() {
    
    Route::get('/roles/search', 'RoleController@buscar');
    Route::resource('roles','RoleController');

    Route::get('users/search', 'UserController@buscar');
    Route::resource('users','UserController');

    Route::get('permissions/search', 'PermissionController@buscar');
    Route::resource('permissions','PermissionController');

    Route::get('productos/search', 'ProductoController@buscar');
    Route::resource('productos','ProductoController');

    Route::get('documentos/search', 'DocumentoController@buscar');
    Route::resource('documentos','DocumentoController');

    Route::get('segmentos/search', 'SegmentoController@buscar');
    Route::resource('segmentos','SegmentoController');

    Route::get('secciones/search', 'SeccionController@buscar');
    Route::resource('secciones','SeccionController');

    Route::get('seccionCambios/back/{id}', 'SeccionCambioController@atras');
    Route::get('seccionCambios/search', 'SeccionCambioController@buscar');
    Route::resource('seccionCambios','SeccionCambioController');


    Route::get('cambios/search', 'CambiosController@buscar');
    Route::resource('cambios','CambiosController');
    
    Route::get('antecedentes/back/{id}', 'AntecedentesController@atras');
    Route::get('antecedentes/search', 'AntecedentesController@buscar');
    Route::post('antecedentes/editar/{id}', 'AntecedentesController@editar');
    Route::resource('antecedentes','AntecedentesController');

    Route::get('ChildAntecedentes/search', 'ChildAntecedentesController@buscar');
    Route::post('ChildAntecedentes/editar/{id}', 'ChildAntecedentesController@editar');
    Route::get('ChildAntecedentes/back/{id}', 'ChildAntecedentesController@atras');
    Route::resource('ChildAntecedentes','ChildAntecedentesController');
    
    
});



// Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', function () {
    return view('home');
});

Route::get('/inicio', function () {
    
    return view('home');
});

Route::get('/test', function () {
    return view('test');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
