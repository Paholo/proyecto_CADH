<?php ?>
@extends('layouts.app')

@section('content')
    <div class="container">
    
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Documentos Management</div>

                    <div class="panel-body">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                        {!! Form::open(['method'=>'GET','url'=>'admin\documentos\search','class'=>'row','role'=>'search'])  !!}
                        
                    <div id="custom-search-input">
                                            <div class="input-group col-md-12">
                                                <input type="text" name ="search" class="  search-query form-control" placeholder="Search" />
                                                <span class="input-group-btn">
                                                    <button class="btn btn-danger" type="submit">
                                                        <span class=" glyphicon glyphicon-search"></span>
                                                    </button>
                                                </span>
                                            </div>
                                        </div>
                       
                     
                        {!! Form::close() !!}
                        @if(sizeof($documentos) >0 )  
                        <table class="table table-striped table-bordered table-condensed">
                      
                       
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Titulo</th>
                                <th>Tipo</th>
                                <th>Secciones</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach ($documentos as $key => $document)
                                                
                                <tr class="list-users">
                                    <td>{{ ++$i }}</td>
                                    <td>{{ $document->Titulo }} 
                                    
                                    </td>
                                    <td>{{ $document->tipo }}</td>
                                    <td> <a class="btn btn-info" href="{{ route('documentos.show',$document->id) }}">Ver</a></td>
                                    <td>
                                    @role('admin')
               
                                   
                                        <a class="btn btn-primary" href="{{ route('documentos.edit',$document->id) }}">Edit</a>
                                        </td><td>
                                       

                                        <form action="{{ url('admin/documentos/'.$document->id) }}" method="POST" style="display: inline-block">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}

                                            <button type="submit" id="delete-task-{{ $document->id }}" class="btn btn-danger">
                                                <i class="fa fa-btn fa-trash"></i>Delete
                                            </button>
                                    @endrole       
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        @else
                        <h2> No hay resultados </h2>
                        <br>
                        @endif

                        @role('admin')
                        <a href="{{ route('documentos.create') }}" class="btn btn-success">New Documento</a>
                        <a href="{{ route('documentos.index') }}" class="btn btn-info">Ver todo</a>
                        @endrole  
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection