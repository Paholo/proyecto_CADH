<?php ?>
@extends('layouts.app')

@section('content')
    <div class="container">
  <!-- Example split danger button -->

</div>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Informacion Documento</div>

                    <div class="panel-body">


                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Título</label>
                            {{ $documento->Titulo }}
                        </div>

                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Tipo</label>
                            {{ $documento->tipo }}
                        </div>

                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Contenido</label>
                            <?php echo htmlspecialchars_decode( $documento->Contenido) ?>
                        </div>

                     
                    </div>
                    <a href="{{ route('secciones.create',['titulo'=>$documento->Titulo,'padre'=>$documento->id]) }}" class="btn btn-success">Crear Sección</a>
                </div>

            <!-- <div data-role="main" class="ui-content"> -->
                @if(sizeof($seccion) >0 )  <!-- if 1 -->
                <ul>
                @for($i = 0; $i < sizeof($seccion); $i++) <!-- for 1 -->
                <li>
                {{ $seccion[$i][0]->Titulo }}
                <div class="panel panel-default">
                        <div class="panel-heading">Informacion Seccion  {{ $seccion[$i][0]->Titulo }}
                        @role('admin') <a style="float:right;" href="{{ route('secciones.create',['titulo'=>$seccion[$i][0]->Titulo,'padre'=>$seccion[$i][0]->id]) }}" class="btn btn-success">Crear Sección</a> @endrole
                            <a style="float:right;" href="{{ route('secciones.show',['titulo'=>$seccion[$i][0]->Titulo,'padre'=>$seccion[$i][0]->id]) }}" class="btn btn-success">Ver Sección</a>
                         </div>
                        
                            </div>
                          
                        
                      
                        @if(is_array($seccion[$i]) ) <!-- if 2 -->
                        @for($j = 0; $j < sizeof($seccion[$i]); $j++) <!-- for 2 -->
     

                            @if(is_array($seccion[$i][$j]) ) <!-- if 3 -->
                           
                                @for($k = 0; $k < sizeof($seccion[$i][$j]); $k++) <!-- for 3 -->
                                <ul>
                                @if(!is_array($seccion[$i][$j][$k]) ) <!-- if 4 -->
                                <li>
                                <div class="panel panel-default">
                                            <div class="panel-heading">Informacion Seccion {{ $seccion[$i][$j][$k]->Titulo }}
                                            @role('admin') <a  style="float:right;" href="{{ route('secciones.create',['titulo'=>$seccion[$i][$j][$k]->Titulo,'padre'=>$seccion[$i][$j][$k]->id]) }}" class="btn btn-success">Crear Sección</a> @endrole
                                            <a  style="float:right;" href="{{ route('secciones.show',['titulo'=>$seccion[$i][$j][$k]->Titulo,'padre'=>$seccion[$i][$j][$k]->id]) }}" class="btn btn-success">Ver Sección</a>
                                            </div>


        
                                     </div>
                                           
                                        
                          
                                @endif <!-- if 4 -->
                                        @if(is_array($seccion[$i][$j][$k]) ) <!-- if 5 -->
                                        
                                            @for($t = 0; $t < sizeof($seccion[$i][$j][$k]); $t++) <!-- for 4 -->
                                              <ul>
                                                 @if(!is_array($seccion[$i][$j][$k][$t]) ) <!-- if 6 -->
                                                 <li>
                                                 <div class="panel panel-default">
                                                        <div class="panel-heading">Informacion Seccion {{ $seccion[$i][$j][$k][$t]->Titulo }}
                                                        @role('admin') <a style="float:right;" href="{{ route('secciones.create',['titulo'=>$seccion[$i][$j][$k][$t]->Titulo,'padre'=>$seccion[$i][$j][$k][$t]->id]) }}" class="btn btn-success">Crear Sección</a> @endrole
                                                        <a style="float:right;" href="{{ route('secciones.show',['titulo'=>$seccion[$i][$j][$k][$t]->Titulo,'padre'=>$seccion[$i][$j][$k][$t]->id]) }}" class="btn btn-success">Ver Sección</a>
                                                        </div>

                                                          
                                                        
                                                        </div>
                                                        
                                       

                                                    
                                                 @endif<!-- if 6 -->
                                                 <!-- AQUI -->
                                                    @if(is_array($seccion[$i][$j][$k][$t]) ) <!-- if  -->
                                                    
                                                    
                                                        @for($u = 0; $u < sizeof($seccion[$i][$j][$k][$t]); $u++) <!-- for 5 -->
                                                        <ul>
                                                            @if(!is_array($seccion[$i][$j][$k][$t][$u]) )
                                                            <li>
                                                            <div class="panel panel-default">
                                                                    <div class="panel-heading">Informacion Seccion {{ $seccion[$i][$j][$k][$t][$u]->Titulo }}
                                                                    @role('admin')<a  style="float:right;" href="{{ route('secciones.create',['titulo'=>$seccion[$i][$j][$k][$t][$u]->Titulo,'padre'=>$seccion[$i][$j][$k][$t][$u]->id]) }}" class="btn btn-success">Crear Sección</a> @endrole
                                                                    <a  style="float:right;" href="{{ route('secciones.show',['titulo'=>$seccion[$i][$j][$k][$t][$u]->Titulo,'padre'=>$seccion[$i][$j][$k][$t][$u]->id]) }}" class="btn btn-success">Ver Sección</a>
                                                                    </div>

                                                                      

                                                                    
                                                                    </div>
                                                                
                                                                
                                                                
                                                            @endif
                                                            <!-- Empieza -->
                                                            @if(is_array($seccion[$i][$j][$k][$t][$u]) ) <!-- if  -->
                                                            
                                                                @for($x = 0; $x < sizeof($seccion[$i][$j][$k][$t][$u]); $x++) <!-- for 6 -->
                                                                <ul>
                                                                
                                                                     @if(!is_array($seccion[$i][$j][$k][$t][$u][$x]) )
                                                                     <il>
                                                                     <div class="panel panel-default">
                                                                            <div class="panel-heading">Informacion Seccion {{ $seccion[$i][$j][$k][$t][$u][$x]->Titulo }}
                                                                            @role('admin')   <a style="float:right;" href="{{ route('secciones.create',['titulo'=>$seccion[$i][$j][$k][$t][$u][$x]->Titulo,'padre'=>$seccion[$i][$j][$k][$t][$u][$x]->id]) }}" class="btn btn-success">Crear Sección</a> @endrole
                                                                            <a style="float:right;" href="{{ route('secciones.show',['titulo'=>$seccion[$i][$j][$k][$t][$u][$x]->Titulo,'padre'=>$seccion[$i][$j][$k][$t][$u][$x]]) }}" class="btn btn-success">Ver Sección</a>
                                                                            </div>

                                                                            

                                                                            
                                                                            </div>

                                                                   
                                                                    
                                                                     @endif
                                                                    <!-- Empieza pen ultimo-->
                                                                    
                                                                    @if(is_array($seccion[$i][$j][$k][$t][$u][$x]) ) <!-- if  -->
                                                                   
                                                                   
                                                                        @for($y = 0; $y < sizeof($seccion[$i][$j][$k][$t][$u][$x]); $y++) <!-- for 7 -->
                                                                        <ul>
                                                                          
                                                                            @if(!is_array($seccion[$i][$j][$k][$t][$u][$x][$y]) )
                                                                            <il>
                                                                            <div class="panel panel-default">
                                                                                <div class="panel-heading">Informacion Seccion {{ $seccion[$i][$j][$k][$t][$u][$x][$y]->Titulo }}
                                                                                @role('admin')  <a style="float:right;" href="{{ route('secciones.create',['titulo'=>$seccion[$i][$j][$k][$t][$u][$x][$y]->Titulo,'padre'=>$seccion[$i][$j][$k][$t][$u][$x][$y]]) }}" class="btn btn-success">Crear Sección</a> @endrole
                                                                                <a style="float:right;" href="{{ route('secciones.show',['titulo'=>$seccion[$i][$j][$k][$t][$u][$x][$y]->Titulo,'padre'=>$seccion[$i][$j][$k][$t][$u][$x][$y]]) }}" class="btn btn-success">Ver Sección</a>
                                                                                </div>


                                                                                
                                                                                </div>
                                                                                
                                                                           
                                                                            @endif
                                                                        <!-- Empieza  ultimo-->
                                                                            @if(is_array($seccion[$i][$j][$k][$t][$u][$x][$y]) ) <!-- if  -->
                                                                            
                                                                                @for($z = 0; $z < sizeof($seccion[$i][$j][$k][$t][$u][$x][$y]); $z++) <!-- for 8 -->
                                                                                <ul>
                                                                        
                                                                                     @if(!is_array($seccion[$i][$j][$k][$t][$u][$x][$y][$z]) )
                                                                                     <il>
                                                                                     <div class="panel panel-default">
                                                                                        <div class="panel-heading">Informacion Seccion {{ $seccion[$i][$j][$k][$t][$u][$x][$y][$z]->Titulo }}
                                                                                      
                                                                                        <a style="float:right;" href="{{ route('secciones.show',['titulo'=>$seccion[$i][$j][$k][$t][$u][$x][$y][$z]->Titulo,'padre'=>$seccion[$i][$j][$k][$t][$u][$x][$y][$z]]) }}" class="btn btn-success">Ver Sección</a>
                                                                                        </div>

                                                                                          
                                                                                        
                                                                                        </div>
                                                                                       
                                                                                     
                                                                                    
                                                                                     </il>
                                                                                     @endif
                                                                                     
                                                                                     </ul>
                                                                                @endfor <!-- end for 8 -->
                                                                                </il>
                                                                            @endif
                                                                        <!-- Termina ultimo -->
                                                                            </ul>
                                                                        @endfor <!-- end for 7 -->
                                                                       </il>
                                                                    @endif
                                                                    
                                                                    <!-- Termina  pen ultimo-->
                                                                    </ul>
                                                                @endfor <!-- end for 6 -->
                                                                </li>
                                                            @endif
                                                            <!-- Termina -->
                                                            </ul>
                                                        @endfor    <!-- end for 5 -->
                                                       </li> 
                                                    @endif<!-- if 7-->
                                                 <!-- TERMINA ACA -->
                                                 </ul>
                                            @endfor <!-- end for 4 -->
                                            </li>
                                         @endif <!-- if 5 -->

                                     </ul>    
                                    @endfor <!-- end for 3 -->
                                    
                                @endif  <!-- if 3 -->
                        @endfor<!-- end for 2 -->
                        @endif <!-- if 2 -->

                    </li>
                @endfor
                </ul>
                @endif <!-- if 1 -->
           
        </div> <!-- row -->

    </div> <!-- container -->
    


    </div>

@endsection