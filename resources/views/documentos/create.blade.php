<?php ?>
@extends('layouts.app')

@section('content')
<script src='https://cloud.tinymce.com/stable/tinymce.min.js'></script>
<script>

tinymce.init({
  selector: 'textarea',
  height: 500,
  plugins: ['advlist autolink lists link image charmap print preview anchor textcolor',
    'searchreplace visualblocks code fullscreen',
    'insertdatetime media table contextmenu paste code help wordcount'],
   // toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
  content_css: [
    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tinymce.com/css/codepen.min.css'],
  
  style_formats: [
    { title: 'Red text', inline: 'span',classes: 'CADH'},
 
  ],
//   formats: {
//     alignleft: { selector: 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', classes: 'left' },
//     aligncenter: { selector: 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', classes: 'center' },
//     alignright: { selector: 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', classes: 'right' },
//     alignfull: { selector: 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', classes: 'full' },
//     bold: { inline: 'span', 'classes': 'bold' },
//     italic: { inline: 'span', 'classes': 'italic' },
//     underline: { inline: 'span', 'classes': 'underline', exact: true },
//     strikethrough: { inline: 'del' },
//     customformat: { inline: 'span', styles: { color: '#00ff00', fontSize: '20px' }, attributes: { title: 'My custom format' }, classes: 'example1' },
//   }
});
</script>

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Crear nuevo documento</div>

                    <div class="panel-body">
                        <!-- Display Validation Errors -->
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form class="form-horizontal" role="form" method="POST" action="{{ url('admin/documentos') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('Titulo') ? ' has-error' : '' }}">
                                <label for="Titulo" class="col-md-4 control-label">Titulo</label>

                                <div class="col-md-6">
                                    <input id="Titulo" type="text" class="form-control" name="Titulo" value="{{ old('Titulo') }}"
                                           required autofocus>

                                    @if ($errors->has('Titulo'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('Titulo') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                       

                  
                         <div class="form-group{{ $errors->has('tipo') ? ' has-error' : '' }}">
                            <label for="tipo" class="col-md-4 control-label">Tipos</label>

                            <div class="col-md-6">
                                <select id="tipos" name="tipo">
                               
                                        <option value="Carta">Carta</option>
                                        <option value="Ley">Ley</option>
                                        <option value="Ley">Convención</option>
                              
                                </select>


                                @if ($errors->has('tipo'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('tipo') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <textarea  id="Contenido" name="Contenido">

                        </textarea>


                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Save
                                    </button>

                                    <a class="btn btn-link" href="{{ url('admin/documentos') }}">
                                        Cancel
                                    </a>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection