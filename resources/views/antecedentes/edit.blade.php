<?php ?>
@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Editar Antecedente</div>

                    <div class="panel-body" onload="inicio();">
                        <!-- Display Validation Errors -->
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    

                        <form class="form-horizontal" role="form" method="POST" action="{{ url('admin/antecedentes/editar/'.$antecedente->id) }}">
                            {{ csrf_field() }}

                            
                            <div class="form-group{{ $errors->has('serie') ? ' has-error' : '' }}">
                                <label for="serie" class="col-md-4 control-label">Serie</label>

                                <div class="col-md-6">

                                 
                                        <ul class="list-group">
                                            <li class="list-group-item">
                                               Caso Contencioso
                                                <div class="material-switch pull-right">
                                                    <input id="Check1"  value="A" name="serie" type="checkbox" onclick="selectOnlyThis(this.id)" />
                                                    <label for="Check1" class="label-danger"></label>
                                                </div>
                                            </li>
                                            <li class="list-group-item">
                                                Consultivo
                                                <div class="material-switch pull-right">
                                                    <input id="Check2"  value="C" name="serie" type="checkbox" onclick="selectOnlyThis(this.id)"/>
                                                    <label for="Check2" class="label-primary"></label>
                                                </div>
                                            </li>
                                            <li class="list-group-item">
                                                Comentario con cita
                                                <div class="material-switch pull-right">
                                                    <input id="Check3"  value="W" name="serie" type="checkbox" onclick="selectOnlyThis(this.id)"/>
                                                    <label for="Check3" class="label-success"></label>
                                                </div>
                                            </li>
                                        </ul>
                                    @if ($errors->has('serie'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('serie') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div id="exp" style="display:none" class="form-group{{ $errors->has('explicacion') ? ' has-error' : '' }}">
                                <label for="explicacion" class="col-md-4 control-label">Explicación</label>

                                <div class="col-md-6">
                                    <input id="explicacion" type="text" class="form-control" name="explicacion" value="{{ $antecedente->explicacion }}"
                                           >

                                    @if ($errors->has('explicacion'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('explicacion') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('nombre') ? ' has-error' : '' }}">
                                <label for="nombre" class="col-md-4 control-label">Nombre</label>

                                <div class="col-md-6">
                                    <input id="nombre" type="text" class="form-control" name="nombre" value="{{ $antecedente->Nombre }}"
                                           required autofocus>

                                    @if ($errors->has('nombre'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('nombre') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div id="con" class="form-group{{ $errors->has('contra') ? ' has-error' : '' }}">
                                <label for="contra" class="col-md-4 control-label">Contra</label>

                                <div class="col-md-6">
                                    <input id="contra" type="text" class="form-control" name="contra" value="{{ $antecedente->Contra }}"
                                           >

                                    @if ($errors->has('contra'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('contra') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div  class="form-group{{ $errors->has('corte') ? ' has-error' : '' }}">
                                <label for="corte" class="col-md-4 control-label">Corte</label>

                                <div class="col-md-6">
                                    <input id="corte" type="text" class="form-control" name="corte" value="{{$antecedente->Corte}}"
                                           >

                                    @if ($errors->has('corte'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('corte') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div  class="form-group{{ $errors->has('tipo') ? ' has-error' : '' }}">
                                <label for="tipo" class="col-md-4 control-label">Tipo</label>

                                <div class="col-md-6">
                                    <input id="tipo" type="text" class="form-control" name="tipo" value="{{ $antecedente->Tipo }}"
                                           required autofocus>

<!--                                       
                                                <select id="tipos" name="tipo">

                                                        <option value="Sentencia">Sentencia</option>
                                                        <option value="Fondo">Fondo</option>
                                                        <option value="Reparaciones">Reparaciones</option>
                                                        <option value="Costos">Costos</option>
                                            
                                                </select> -->


                            
                                    @if ($errors->has('tipo'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('tipo') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <!-- <select class="selectpicker" > dfd
                            <option>Mustard</option>
                            <option>Ketchup</option>
                            <option>Relish</option>
                            </select> -->

                            
                            <input id="padre" type="text" class="form-control" name="padre" value="{{$padre}}" style="display:none"
                                           required autofocus>
                            <div class="form-group{{ $errors->has('numero') ? ' has-error' : '' }}">
                                <label for="numero" class="col-md-4 control-label">No:</label>

                                <div class="col-md-6">
                                    <input id="numero" type="text" class="form-control" name="numero" value="{{ $antecedente->Numero }}"
                                           required autofocus>

                                    @if ($errors->has('numero'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('numero') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('fecha') ? ' has-error' : '' }}">
                                <label for="fecha" class="col-md-4 control-label">Fecha:</label>

                                <div class="col-md-6">
                                    <input id="fecha" type="text" class="form-control" name="fecha" value="{{ $antecedente->Fecha }}"
                                           required autofocus>

                                    @if ($errors->has('fecha'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('fecha') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('parrafo') ? ' has-error' : '' }}">
                                <label for="parrafo" class="col-md-4 control-label">Parrafo:</label>

                                <div class="col-md-6">
                                    <input id="parrafo" type="text" class="form-control" name="parrafo" value="{{ $antecedente->Parrafo }}"
                                           required autofocus>

                                    @if ($errors->has('parrafo'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('parrafo') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('detalle') ? ' has-error' : '' }}">
                                <label for="detalle" class="col-md-4 control-label">Detalle:</label>

                                <div class="col-md-6">
                                    <textarea rows="4" cols="50" name="detalle" id="detalle" class="form-control">{{ $antecedente->Detalle }}</textarea>

                                    @if ($errors->has('detalle'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('detalle') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            
                            <div class="form-group{{ $errors->has('categorias') ? ' has-error' : '' }}">
                                <label for="categorias" class="col-md-4 control-label">Categoria</label>

                                <div class="col-md-6">
                
                                     
                                        <ul class="list-group">
                                            <li class="list-group-item">
                                            Primer precedente
                                                <div class="material-switch pull-right">
                                                    <input id="primerPresedente"  value="Primer precedente" name="categorias[]" type="checkbox"/>
                                                    <label for="primerPresedente" class="label-danger"></label>
                                                </div>
                                            </li>
                                            <li class="list-group-item">
                                            Precedente más relevante
                                                <div class="material-switch pull-right">
                                                    <input id="precedenteImportante"  value="Precedente más relevante" name="categorias[]" type="checkbox"/>
                                                    <label for="precedenteImportante" class="label-primary"></label>
                                                </div>
                                            </li>
                                            <li class="list-group-item">
                                            Último precedente
                                                <div class="material-switch pull-right">
                                                    <input id="ultimoPrecedente"  value="Último precedente" name="categorias[]" type="checkbox"/>
                                                    <label for="ultimoPrecedente" class="label-success"></label>
                                                </div>
                                            </li>
                                            <li class="list-group-item">
                                            Origen fuera del sistema
                                                <div class="material-switch pull-right">
                                                    <input id="origenFeura"  value="Origen fuera del sistema" name="categorias[]" type="checkbox"/>
                                                    <label for="origenFeura" class="label-info"></label>
                                                </div>
                                            </li>
                                            <li class="list-group-item">
                                            Cambio de criterio
                                                <div class="material-switch pull-right">
                                                    <input id="cambioCri"  value="Cambio de criterio" name="categorias[]" type="checkbox"/>
                                                    <label for="cambioCri" class="label-default"></label>
                                                </div>
                                            </li>
                                            <li class="list-group-item">
                                            Otro
                                                <div class="material-switch pull-right">
                                                    <input id="precedenteOtro"  value="Otro" name="categorias[]" type="checkbox"/>
                                                    <label for="precedenteOtro" class="label-warning"></label>
                                                </div>
                                            </li>
                                            
                                        </ul>

                                 

                                    @if ($errors->has('categorias'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('categorias') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                           

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Save
                                    </button>

                                    <a class="btn btn-link" href="{{ route('antecedentes.show',$padre) }}">
                                        Cancel
                                    </a>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
    {{$antecedente->Serie}}
    <div id="te">div</div>
    <script>
      
            // var div = document.getElementById('te');
            // div.innerHTML += 'Extra stuff';
            var tipo = "<?php echo $antecedente->Serie ?>";
            var tamano = <?php echo sizeof($categorias)?>;
            var cat = <?php echo json_encode($categorias); ?>;
            // div.innerHTML += String(cat[0]);
    if(tipo === "A")
    {
       
        document.getElementById("Check1").checked = true;
        document.getElementById("con").style.display = "none";
    }

    if(tipo === "C")
    {
        document.getElementById("Check2").checked = true;
    }

    if(tipo === "W")
    {
        document.getElementById("Check3").checked = true;
        document.getElementById("exp").style.display = "block";
        document.getElementById("con").style.display = "none";
    }

    for(var j =0;j<tamano;j++)
    {
        if(String(cat[j])== "Primer precedente")
        {
            document.getElementById("primerPresedente").checked = true;
        }

        if(String(cat[j])== "Precedente más relevante")
        {
            document.getElementById("precedenteImportante").checked = true;
        }

        if(String(cat[j])== "Último precedente")
        {
            document.getElementById("ultimoPrecedente").checked = true;
        }

        if(String(cat[j])== "Origen fuera del sistema")
        {
            document.getElementById("origenFeura").checked = true;
        }

        if(String(cat[j])== "Otro")
        {
            document.getElementById("precedenteOtro").checked = true;
        }

        if(String(cat[j])== "Cambio de criterio")
        {
            document.getElementById("cambioCri").checked = true;
        }
        
    }

           
    function inicio(){
        var div = document.getElementById('te');
        div.innerHTML += 'Extra stuff';
    var tipo = <?php echo $antecedente->Serie ?>;
    if(tipo === "A")
    {
        document.getElementById("t").innerHTML += 'Extra stuff';
        document.getElementById("Check1").checked = true;
    }

    if(tipo === "C")
    {
        document.getElementById("Check2").checked = true;
    }

    if(tipo === "W")
    {
        document.getElementById("Check3").checked = true;
       
    }
    }
       

   function selectOnlyThis(id) {
    for (var i = 1;i <= 3; i++)
    {
        document.getElementById("Check" + i).checked = false;
    }
    if(id == "Check2" || id == "Check3" ){
        document.getElementById("con").style.display = "none";

    }else{
        document.getElementById("con").style.display = "block";
    }

    if( id == "Check3" ){
        document.getElementById("exp").style.display = "block";
        
    }else{
        document.getElementById("exp").style.display = "none";
    }
    var d = document.getElementById(id);
    d.checked = true;
}
    </script>
@endsection