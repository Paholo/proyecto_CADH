<?php ?>
@extends('layouts.app')

@section('content')
    <div class="container">
      <a id="back" href="{{  url('admin/antecedentes/back/'.$cambio->id_seccion) }}" class="previous round">&#8249;</a>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Informacion Antecedente antecedente</div>

                    <div class="panel-body">
                   

                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Título</label>
                            {{ $cambio->Titulo }}
                        </div>
                
                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Autor</label>
                            {{ $autor }}
                        </div>
                     
                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Contenido</label>
                            <br>
                            <br>
                            <div class ="conDiv">
                            <?php echo htmlspecialchars_decode( $cambio->Contenido) ?>
                            </div>
                        </div>
                       
                    </div>
                </div>
                <?php $i =0; ?>

                @if(sizeof($antecedentes) >0 )  
                        <h2 >Listado de antecedentes </h2>
                        <table class="table table-striped table-bordered table-condensed">
                      
                       
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Nombre</th>
                               
                                <!-- <th>Autor</th> -->
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $contador =0 ?>
                            @foreach ($antecedentes as $key => $antecedente)
                                                
                                <tr class="list-users">
                                    <td>{{ ++$i }}</td>
                                    <td>{{ $antecedente->Nombre }} 
                                
                                    </td>
                                    
                                   
                                    <!-- <td>{{ $cambio->id_autor }}</td> -->
                                   
                                    <td>
                                        <!-- <a class="btn btn-info" href="{{ route('antecedentes.show',$antecedente->id) }}">Show</a> -->
                                        <a class="btn btn-info" href="{{ route('ChildAntecedentes.show',[$antecedente->id,'hijo1'=>$cambio->id]) }}">Administrar</a>
                                        <a class="btn btn-primary" href="{{ route('antecedentes.edit',$antecedente->id) }}">Edit</a>
                                    </td>
                                    @role('admin')<td>
                                        <form action="{{ url('admin/antecedentes/'.$antecedente->id) }}" method="POST" style="display: inline-block">
                                        <input id="padre" type="text" class="form-control" name="padre" value="{{$cambio->id}}" style="display:none"
                                           required autofocus>
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}

                                            <button type="submit" id="delete-task-{{ $antecedente->id }}" class="btn btn-danger">
                                                <i class="fa fa-btn fa-trash"></i>Delete
                                            </button>
                                        </form>
                                  </td>  @endrole
                                </tr>
                                <?php $contador++ ?>
                            @endforeach
                            </tbody>
                        </table>
                        @else
                        <h2> No hay antecedentes </h2>
                        <br>
                        @endif
                      
                        <a href="{{ route('antecedentes.create', ['iden'=>$cambio->id]) }}" class="btn btn-success">Nuevo Antecedente</a>
                        
                
            
            </div>
        </div>
    </div>

@endsection