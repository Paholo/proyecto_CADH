<?php ?>
@extends('layouts.app')

@section('content')
    <div class="container">
  
      <a id="back" href="{{  url('admin/ChildAntecedentes/back/'.$padre) }}" class="previous round">&#8249;</a>
  
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Informacion Antecedente</div>

                    <div class="panel-body">


                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Nombre</label>
                            {{ $cambio->Nombre }}
                        </div>
                        
                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Serie</label>
                            {{  $cambio->Serie }}
                        </div>

                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Tipo</label>
                            {{ $cambio->Tipo }}
                        </div>
                     
                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Contenido</label>
                            <br>
                            <br>
                            <div class ="conDiv">
                            <?php echo htmlspecialchars_decode( $cambio->Detalle) ?>
                            </div>
                        </div>

                        @if($cambio->Serie == "W")
                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Explicación cita</label>
                            <br>
                            <br>
                            <div class ="conDiv">
                            <?php echo htmlspecialchars_decode( $cambio->explicacion) ?>
                            </div>
                        </div>
                        @endif

                    </div>
                </div>
                <?php $i =0; ?>

                @if(sizeof($antecedentes) >0 )  
                        <h2 >Listado de antecedentes </h2>
                        <table class="table table-striped table-bordered table-condensed">
                      
                       
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Nombre</th>
                               
                                <!-- <th>Autor</th> -->
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $contador =0 ?>
                            @foreach ($antecedentes as $key => $antecedente)
                                                
                                <tr class="list-users">
                                    <td>{{ ++$i }}</td>
                                    <td>{{ $antecedente->Nombre }} 
                                
                                    </td>
                                    
                                   
                                    
                                   
                                    <td>
                                        <!-- <a class="btn btn-info" href="{{ route('antecedentes.show',$antecedente->id) }}">Show</a> -->
                                        <a class="btn btn-info" href="{{ route('ChildAntecedentes.show',$antecedente->id) }}">Administrar</a>
                                        <a class="btn btn-primary" href="{{ route('ChildAntecedentes.edit',$antecedente->id) }}">Edit</a>
                                    </td>
                                    @role('admin')<td>
                                        <form action="{{ url('admin/ChildAntecedentes/'.$antecedente->id) }}" method="POST" style="display: inline-block">
                                        <input id="padre" type="text" class="form-control" name="padre" value="{{$cambio->id}}" style="display:none"
                                           required autofocus>
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}

                                            <button type="submit" id="delete-task-{{ $antecedente->id }}" class="btn btn-danger">
                                                <i class="fa fa-btn fa-trash"></i>Delete
                                            </button>
                                        </form>
                                    </td>@endrole
                                </tr>
                                <?php $contador++ ?>
                            @endforeach
                            </tbody>
                        </table>
                        @else
                        <h2> No hay antecedentes </h2>
                        <br>
                        @endif
                      
                        <a href="{{ route('ChildAntecedentes.create', ['iden'=>$cambio->id]) }}" class="btn btn-success">Nuevos Antecedente</a>
                        
                
            
            </div>
        </div>
    </div>

@endsection