<?php ?>
@extends('layouts.app')

@section('content')
<script src="{{ asset('js/vis.js') }}"></script>

<style type="text/css">
    #mynetwork {
    
      border: 1px solid lightgray;
    }
  </style>
  <?php  function cita($ante){
    $nueva;
    $rest = substr($ante->Fecha, -2);
    //consultiva
    if($ante->Serie == "A"){

        $nueva = $ante->Nombre . '. Opinión Consultiva OC-'.$ante->Numero.'/'.$rest.', ' . $ante->Corte . '. (serie A) No.' .$ante->Numero . ' ('.$ante->Fecha .') &#182; '.$ante->Parrafo. '.';
    }

    //contenciosa
    if($ante->Serie == "C"){
        $nueva = $ante->Nombre . " v. " .$ante->Contra .'. '.$ante->Tipo .'. Sentencia. ' . $ante->Corte . '. (serie C) No.' .$ante->Numero . ' ('.$ante->Fecha .') &#182; '.$ante->Parrafo. '.';
    }

    //otra
    if($ante->Serie == "W"){
        $nueva = $ante->explicacion.'. '.$ante->Nombre . '. Opinión Consultiva OC-'.$ante->Numero.'/'.$rest.', ' . $ante->Corte . '. (serie A) No.' .$ante->Numero . ' ('.$ante->Fecha .') &#182; '.$ante->Parrafo. '.';
    }
    return $nueva; 
} ?>
    <div class="container">
  
        <div class="row">
            <div class="col-md-8 col-md-offset-2" >
                <div class="panel panel-default">
                    <div class="panel-heading">Informacion Cambio</div>

                    <div class="panel-body">


                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Título</label>
                           
                        </div>

                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Autor</label>
                          
                        </div>

                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Cambio sobre Seccion</label>
                          
                        </div>


                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Contenido</label>
                            <br>
                            <br>
                             <div class ="conDiv">
                            <?php echo htmlspecialchars_decode( $cambio->Detalle) ?>
                            </div>
                        </div>

                   </div>
              </div>

           
            </div>



            @if(sizeof($antecedentes) >0 ) 
            <?php $cont = 0;?>
            @foreach ($antecedentes as $key => $antecedente)
           
            <div class="col-md-8 col-md-offset-2" >
                <div class="panel panel-default">
                    <div >

                            <div id="cuadrados">
                            @foreach ($cat[$cont] as $key => $cate)
                                @if($cate == "Primer precedente")
                                <div  class="primero" data-toggle="tooltip" data-placement="top" title="Primer precedente" ></div>
                                
                                @endif

                                @if($cate == "Precedente más relevante")
                                <div  class="masImpo"  data-toggle="tooltip" data-placement="top" title="Precedente más relevante"></div>
                                @endif

                                @if($cate == "Último precedente")
                                <div class="ultimo" data-toggle="tooltip" data-placement="top" title="Último precedente"></div>
                                @endif

                                @if($cate == "Origen fuera del sistema")
                                <div class="fuera" data-toggle="tooltip" data-placement="top" title="Origen fuera del sistema"></div>
                                @endif

                                @if($cate == "Cambio de criterio")
                                <div class="cambio" data-toggle="tooltip" data-placement="top" title="Cambio de criterio"></div>
                                @endif

                                @if($cate == "Otro")
                                <div class="pOtro" data-toggle="tooltip" data-placement="top" title="Otro"></div>
                                @endif


                                
                            @endforeach
                            </div>
                </div >
                <!-- nombre, -->
                    <div class="panel-heading"><?php $comp = cita($antecedente); echo $comp; ?> </div>
                    <div class="panel-body">

                        <div class="form-group">
                                <label for="name" class="col-md-4 control-label">Nombre:</label>
                                {{ $antecedente->Nombre .' v. '. $antecedente->Contra }}
                            </div>

                            <div class="form-group">
                                <label for="name" class="col-md-4 control-label">Fecha:</label>
                                {{ $antecedente->Fecha }}
                            </div>


                            <div class="form-group">
                                <label for="name" class="col-md-4 control-label">Contenido:</label>
                                <br>
                                <br>
                                    <div class ="conDiv">
                                <?php echo htmlspecialchars_decode( $antecedente->Detalle) ?>
                                 </div>
                                 <a href="{{ route('displayAntecedenteAnt.show',$antecedente->id) }}" class="btn btn-success">Antecedentes</a>
                            </div>
                    
                   </div>
                </div>

           
            </div>
            <?php $cont++ ;?>
            @endforeach
            @endif
 
                    
         </div>
    </div>

@endsection
