@extends('layouts.app')

@section('content')
<script src="{{ asset('js/vis.js') }}"></script>

<style type="text/css">
    #mynetwork {
      width: 600px;
      height: 400px;
      border: 1px solid lightgray;
    }
  </style>
  <style>
.mySlides {display:none}
.w3-left, .w3-right, .w3-badge {cursor:pointer}
.w3-badge {height:13px;width:13px;padding:0}
</style>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                    <span id="n1">Text</span>
                    <!-- <script>
                        tippy('.hola', { content: "I'm a tooltip!" })
                    </script>

                    <button data-tippy="I'm a tooltip!">Text</button>
                    <button class="btn" data-tippy-content="Tooltip A">Text</button>
<button class="btn" data-tippy-content="Tooltip B">Text</button>
<button class="btn" data-tippy-content="Tooltip C">Text</button>
                    <button class="btn" title="I'm a tooltip!">Text</button>
                  <script>
                tippy('#hola', {
                        delay: 25,
                        flip: true,
                        arrow: false,
                        arrowType: 'round',
                        size: 'large',
                        interactive: true,
                        interactiveBorder: 5,
                        content: "I'm a tooltip!" 
                    
                      
                        })
                                        </script>

                    <div id="myTemplate" style="display: none;">
                    <h3>Cool <span style="color: pink;">HTML</span> inside here!</h3>
                    <h3>Cool <span style="color: blue;">HTML</span> inside here!</h3>
                    <h3>Cool <span style="color: purple;">HTML</span> inside here!</h3>
                    <button class="btn1" title="I'm a tooltip!">Text</button>
                    </div> -->
                   
                </div>
            </div>
        </div>
    </div>
</div>



<div id = "test4" style="" >
    <div class="w3-content w3-display-container" style="width:250px; /* or whatever width you want. */
        max-width:250px; height:250px; background-color:black; ">  <div id="tipi"><h3>Cool <span style="color: pink;">HTML</span> inside here!</h3></div>
        <div class="mySlides"><h3>Cool <span style="color: pink;">HTML</span> inside here!</h3></div>
        <div class="mySlides"><h3>Cool <span style="color: blue;">HTML</span> inside here!</h3></div>
        <div class="mySlides"><h3>Cool <span style="color: red;">HTML</span> inside here!</h3></div>
        <div class="w3-center w3-container w3-section w3-large w3-text-white w3-display-bottommiddle" style="width:100%">
            <div class="w3-left w3-hover-text-khaki" onclick="plusDivs(-1)">&#10094;</div>
            <div class="w3-right w3-hover-text-khaki" onclick="plusDivs(1)">&#10095;</div>
            <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv(1)"></span>
            <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv(2)"></span>
            <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv(3)"></span>
        </div>
    </div>
  </div>

<script> 
                 tippy('#n1', {
                    // html: '#test4',
                    html: document.querySelector('#test4'),
                    delay: 25,
                        flip: true,
                        arrow: true,
                        arrowType: 'round',
                        size: 'large',
                        interactive: true,
                        interactiveBorder: 5,
                    // ...or...
                   // html: document.querySelector('#myTemplate'),
                    // ...or you can clone a direct element too...
                    //html: document.querySelector('#myTemplate').cloneNode(true)
                    })
                    </script>
  
  <!-- <h1>"the value for number is: " <span id="total"></span></h1>
  <h1>"the value for number total: " <span id="myText"></span></h1> -->
<script>
var slideIndex = 1;
showDivs(slideIndex);

function plusDivs(n) {
  showDivs(slideIndex += n);
}

function currentDiv(n) {
  showDivs(slideIndex = n);
}

function showDivs(n) {
  var i;
  var x = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  document.getElementById("myText").innerHTML = x.length;
  document.getElementById("total").innerHTML = n;
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length}
  for (i = 0; i < x.length; i++) {
     x[i].style.display = "none";  
  }
  for (i = 0; i < dots.length; i++) {
     dots[i].className = dots[i].className.replace(" w3-white", "");
  }
  x[slideIndex-1].style.display = "block";  
  dots[slideIndex-1].className += " w3-white";
  document.getElementById("tipi").innerHTML = "";

}
</script>
<!-- <p>
  Create a simple network with some nodes and edges.
</p>

<div id="mynetwork"></div> -->

<script type="text/javascript">
  // create an array with nodes
  var nodes = new vis.DataSet([
    {id: 1, label: 'Node 1',color:{border: 'black',
    background: 'red'}
    },
    {id: 2, label: 'Node 2'},
    {id: 3, label: 'Node 3'},
    {id: 4, label: 'Node 4'},
    {id: 5, label: 'Node 5'}
  ]);

  // create an array with edges
  var edges = new vis.DataSet([
    {from: 1, to: 3},
    {from: 1, to: 2},
    {from: 2, to: 4},
    {from: 2, to: 5},
    {from: 3, to: 3}
  ]);

  // create a network
  var container = document.getElementById('mynetwork');
  var data = {
    nodes: nodes,
    edges: edges
  };
  var options = {
    nodes:{
         color: '#00cc00'
    }
};
  var network = new vis.Network(container, data, options);
</script>
<!-- <div>holaaa esta es una prueba hiper mega rara dasdfasdf <span id = "xxx" class="x" title="I'm a tooltip!" >itentoo </span> jsfdasldjuds hhola q hace ola k ase </div>
<p onclick="myFunction()"> holaaaa si funicona lloro</p> -->
<!-- <div class="prueba" title="I'm a tooltip!">Text</div> -->


<script>
function myFunction() {
  var list = document.getElementById("xxx");
  list.className = "prueba";
  list.innerHTML = "Milk";
  var list = document.getElementById("test5");
  list.style.display = "inline";
  
  tippy('.prueba', {
                    // html: '#test4',
                    html: document.querySelector('#test5'),
                    delay: 25,
                        flip: true,
                        arrow: true,
                        arrowType: 'round',
                        size: 'large',
                        interactive: true,
                        interactiveBorder: 5,
                    // ...or...
                   // html: document.querySelector('#myTemplate'),
                    // ...or you can clone a direct element too...
                    //html: document.querySelector('#myTemplate').cloneNode(true)
                    })
}


</script>
<div id = "test5" style="display:none" >
    <div class="w3-content w3-display-container" style="width:250px; /* or whatever width you want. */
        max-width:250px; height:250px; background-color:black; ">  <div id="tipi"><h3>Cool <span style="color: pink;">HTML</span> inside here!</h3></div>
        <div class="mySlides"><h3>Cool <span style="color: pink;">HTML</span> inside here!</h3></div>
        <div class="mySlides"><h3>Cool <span style="color: blue;">HTML</span> inside here!</h3></div>
        <div class="mySlides"><h3>Cool <span style="color: red;">HTML</span> inside here!</h3></div>
        <div class="w3-center w3-container w3-section w3-large w3-text-white w3-display-bottommiddle" style="width:100%">
            <div class="w3-left w3-hover-text-khaki" onclick="plusDivs(-1)">&#10094;</div>
            <div class="w3-right w3-hover-text-khaki" onclick="plusDivs(1)">&#10095;</div>
            <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv(1)"></span>
            <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv(2)"></span>
            <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv(3)"></span>
        </div>
    </div>
  </div>
  <script> 
                 
                    </script>
  

@endsection

 <div class="x_panel">
                  <div class="x_title">
                    <h2>Default Buttons </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <button type="button" class="btn btn-default">Default</button>

                    <button type="button" class="btn btn-primary">Primary</button>

                    <button type="button" class="btn btn-success">Success</button>

                    <button type="button" class="btn btn-info">Info</button>

                    <button type="button" class="btn btn-warning">Warning</button>

                    <button type="button" class="btn btn-danger">Danger</button>

                    <button type="button" class="btn btn-dark">Dark</button>

                    <button type="button" class="btn btn-link">Link</button>
                  </div>
                </div>