<?php ?>
@extends('layouts.app')

@section('content')
    <div class="container">
  
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">User Info</div>

                    <div class="panel-body">


                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Name</label>
                            {{ $user->name }}
                        </div>

                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">NombreCompleto</label>
                            {{ $user->completename }}
                        </div>


                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">E-mail</label>
                            {{ $user->email }}
                        </div>


                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Roles</label>
                            @if(!empty($user->roles))
                                @foreach($user->roles as $role)
                                    <label class="label label-success">{{ $role->display_name }}</label>
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('js/vis.js') }}"></script>

<style type="text/css">
    #mynetwork {
      width: 600px;
      height: 400px;
      border: 1px solid lightgray;
    }
  </style>

<p>
  Create a simple network with some nodes and edges.
</p>

<div id="mynetwork"></div>

<script type="text/javascript">
  // create an array with nodes
  var nodes = new vis.DataSet([
    {id: 1, label: 'Node 1',color:{border: 'black',
    background: 'red'}
    },
    {id: 2, label: '{{ $user->name }}'},
    {id: 3, label: '{{ $user->completename }}'},
    {id: 4, label: '{{ $user->email }}'},
    {id: 5, label: ' {{ $user->email }}'}
  ]);

  // create an array with edges
  var edges = new vis.DataSet([
    {from: 1, to: 3},
    {from: 1, to: 2},
    {from: 2, to: 4},
    {from: 2, to: 5},
    {from: 3, to: 3}
  ]);

  // create a network
  var container = document.getElementById('mynetwork');
  var data = {
    nodes: nodes,
    edges: edges
  };
  var options = {
    nodes:{
         color: '#00cc00'
    }
};
  var network = new vis.Network(container, data, options);
</script>
@endsection