<?php ?>
@extends('layouts.app')

@section('content')
    <div class="container">
    
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">User Management</div>

                    <div class="panel-body">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                        {!! Form::open(['method'=>'GET','url'=>'admin\users\search','class'=>'row','role'=>'search'])  !!}
                        
                    <div id="custom-search-input">
                                            <div class="input-group col-md-12">
                                                <input type="text" name ="search" class="  search-query form-control" placeholder="Search" />
                                                <span class="input-group-btn">
                                                    <button class="btn btn-danger" type="submit">
                                                        <span class=" glyphicon glyphicon-search"></span>
                                                    </button>
                                                </span>
                                            </div>
                                        </div>
                       
                     
                        {!! Form::close() !!}
                        @if(sizeof($users) >0 )  
                        <table class="table table-striped table-bordered table-condensed">
                      
                       
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Roles</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach ($users as $key => $user)
                                                
                                <tr class="list-users">
                                    <td>{{ ++$i }}</td>
                                    <td>{{ $user->name }} 
                                    <button class="btn" title='{{ $user->name }} '>Text</button>
                                        <script>
                                        tippy('.btn')
                                        </script>
                                    </td>
                                    <td>{{ $user->email }}</td>
                                    <td>
                                        @if(!empty($user->roles))
                                            @foreach($user->roles as $role)
                                                <label class="label label-success">{{ $role->display_name }}</label>
                                            @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        <a class="btn btn-info" href="{{ route('users.show',$user->id) }}">Show</a>
                                        <a class="btn btn-primary" href="{{ route('users.edit',$user->id) }}">Edit</a>

                                        <form action="{{ url('admin/users/'.$user->id) }}" method="POST" style="display: inline-block">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}

                                            <button type="submit" id="delete-task-{{ $user->id }}" class="btn btn-danger">
                                                <i class="fa fa-btn fa-trash"></i>Delete
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        @else
                        <h2> No hay resultados </h2>
                        <br>
                        @endif
                        <a href="{{ route('users.create') }}" class="btn btn-success">New User</a>
                        <a href="{{ route('users.index') }}" class="btn btn-info">Ver todo</a>
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection