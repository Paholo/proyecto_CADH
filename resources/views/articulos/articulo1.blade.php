
      <head>

    
     <link rel="stylesheet" type="text/css" href="{{ asset('tooltipster-master/dist/css/tooltipster.bundle.min.css') }}" />
     <link rel="stylesheet" type="text/css" href="{{ asset('css/styles_tool.css') }}" />
     <link rel="stylesheet" type="text/css" href="{{ asset('tooltipster-master/dist/css/plugins/tooltipster/sideTip/themes/tooltipster-sideTip-punk.min.css') }}" />
     <script src="{{ asset('jquery/jquery-3.3.1.js') }}"></script>
     <script src="{{ asset('js/tooltipster.bundle.min.js') }}"></script>
     <script src="https://unpkg.com/tippy.js@2.5.4/dist/tippy.all.min.js"></script>
         <!-- <script type="text/javascript" src="../../public/tooltipster-master/dist/js/tooltipster.bundle.min.js"></script> -->
         <!-- <link href="../assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
         
         <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
  -->
              
          <style>
                  #K {
                      background-color: yellow;
                  }
                  </style>
          <script>
              $(document).ready(function() {
                  $('.referencia').tooltipster(
                      { theme: 'tooltipster-punk',
                        interactive: 'true'}
                  );
              });
  
              $(document).ready(function() {
                  $('.referencia1').tooltipster(
                      { 
                        interactive: 'true'}
                  );
              });
  
  
              $(document).ready(function() {
  
                  var $sex = $('#test2');
                   var t = " Atala-Riffo e hijas v. Chile <br> <a href='custom-colored/tree.html' style='color:#22bfe2; text-decoration: none'>1. Primer precedente: Atala-Riffo e hijas v. Chile</a><br> <a href='custom-colored/tree.html' style='text-decoration: none; color:#00dd37' >2.	Precedente mas importante:  Atala-Riffo e hijas v. Chile</a><br><a href='custom-colored/tree.html' style='text-decoration: none; color:#ffa100' >3.	Ultimo precedente: Flor Freire contra Ecuador </a><br><a href='custom-colored/tree.html'style='text-decoration: none; color:#fc3200' >4.	Cambio de jurisprudencia: Mouta contra Portugal</a>";
                                                                         
              $sex.tooltipster({
                  content: t,
                  side: 'right',
                  interactive: 'true',
                  theme: 'tooltipster-punk',
                  contentAsHTML: true
              });
              
              $sex.tooltipster({
                  content: 'Alvaro Paul',
                  multiple: true,
                  side: 'bottom',interactive: 'true',theme: 'light'
              });
  
              var instances = $.tooltipster.instances($sex);
  
              instances[0];
              instances[1];
  
              });
  
              //funcion base para hacer los tooltips
  
              $(document).ready(function() {
                
              var $myElement = $('#element');
              var titulo = " Juridical Conditions and Rights of  the Undocumented Migrants<br> ";
              var primer = "<a href='connectors/refere.html' style='color:#22bfe2; text-decoration: none'>1.	Primer precedente</a><br> " ;
              var  mimportante = " <a href='connectors/refere.html' style='text-decoration: none; color:#37dd27' >2.	El mas importante</a><br> ";
              var u = " <a href='connectors/refere.html' style='text-decoration: none; color:#ffa100' >3.	Ultimo precedente</a><br> ";
              var cambio = " <a href='connectors/refere.html 'style='text-decoration: none; color:#ff66dd' >4.	cambio de jurisprudencia</a><br> ";
              var t = titulo + primer+ mimportante+ u+ cambio;
  
              $myElement.tooltipster({
                  content: t,
                  side: 'right',
                  interactive: 'true',
                  theme: 'tooltipster-punk',
                  contentAsHTML: true
              });
              $(document).ready(function() {
                  $('.Autor').tooltipster(
                      { theme: 'tooltipster-punk'}
                  );
              });
  
              $myElement.tooltipster({
                  content: 'Alvaro Paul',
                  multiple: true,
                  side: 'left',interactive: 'true',theme: 'light'
              });
  
              var instances = $.tooltipster.instances($myElement);
  
              instances[0];
              instances[1];
  
              });
              
              // C1
              $(document).ready(function() {
          
              var $myElement = $('#C1');
              var titulo = " Juridical Conditions and Rights of  the Undocumented Migrants<br> ";
              var primer = "<a href='connectors/refere.html' style='color:#22bfe2; text-decoration: none'>1.	Primer precedente</a><br> " ;
              var  mimportante = " <a href='connectors/refere.html' style='text-decoration: none; color:#37dd27' >2.	El mas importante</a><br> ";
              var u = " <a href='connectors/refere.html' style='text-decoration: none; color:#ffa100' >3.	Ultimo precedente</a><br> ";
              var cambio = " <a href='connectors/refere.html 'style='text-decoration: none; color:#ff66dd' >4.	cambio de jurisprudencia</a><br> ";
              var t = titulo + primer+ mimportante+ u+ cambio;
  
              $myElement.tooltipster({
                  content: t,
                  side: 'top',
                  interactive: 'true',
                  theme: 'tooltipster-punk',
                  contentAsHTML: true
              });
              $(document).ready(function() {
                  $('.Autor').tooltipster(
                      { theme: 'tooltipster-punk'}
                  );
              });
  
              $myElement.tooltipster({
                  content: 'Alvaro Paul',
                  multiple: true,
                  side: 'bottom',interactive: 'true',theme: 'light'
              });
  
              var instances = $.tooltipster.instances($myElement);
  
              instances[0];
              instances[1];
  
              });
  
              // C2
              $(document).ready(function() {
          
              var $myElement = $('#C2');
              var titulo = " Juridical Conditions and Rights of  the Undocumented Migrants<br> ";
              var primer = "<a href='connectors/refere.html' style='color:#22bfe2; text-decoration: none'>1.	Primer precedente</a><br> " ;
              var  mimportante = " <a href='connectors/refere.html' style='text-decoration: none; color:#37dd27' >2.	El mas importante</a><br> ";
              var u = " <a href='connectors/refere.html' style='text-decoration: none; color:#ffa100' >3.	Ultimo precedente</a><br> ";
              var cambio = " <a href='connectors/refere.html 'style='text-decoration: none; color:#ff66dd' >4.	cambio de jurisprudencia</a><br> ";
              var t = titulo + primer+ mimportante+ u+ cambio;
  
              $myElement.tooltipster({
                  content: t,
                  side: 'top',
                  interactive: 'true',
                  theme: 'tooltipster-punk',
                  contentAsHTML: true
              });
              $(document).ready(function() {
                  $('.Autor').tooltipster(
                      { theme: 'tooltipster-punk'}
                  );
              });
  
              $myElement.tooltipster({
                  content: 'Alvaro Paul',
                  multiple: true,
                  side: 'bottom',interactive: 'true',theme: 'light'
              });
  
              var instances = $.tooltipster.instances($myElement);
  
              instances[0];
              instances[1];
  
              });
  
              // C3
              $(document).ready(function() {
          
              var $myElement = $('#C3');
              var titulo = " Juridical Conditions and Rights of  the Undocumented Migrants<br> ";
              var primer = "<a href='connectors/refere.html' style='color:#22bfe2; text-decoration: none'>1.	Primer precedente</a><br> " ;
              var  mimportante = " <a href='connectors/refere.html' style='text-decoration: none; color:#37dd27' >2.	El mas importante</a><br> ";
              var u = " <a href='connectors/refere.html' style='text-decoration: none; color:#ffa100' >3.	Ultimo precedente</a><br> ";
              var cambio = " <a href='connectors/refere.html 'style='text-decoration: none; color:#ff66dd' >4.	cambio de jurisprudencia</a><br> ";
              var t = titulo + primer+ mimportante+ u+ cambio;
  
              $myElement.tooltipster({
                  content: t,
                  side: 'top',
                  interactive: 'true',
                  theme: 'tooltipster-punk',
                  contentAsHTML: true
              });
              $(document).ready(function() {
                  $('.Autor').tooltipster(
                      { theme: 'tooltipster-punk'}
                  );
              });
  
              $myElement.tooltipster({
                  content: 'Alvaro Paul',
                  multiple: true,
                  side: 'bottom',interactive: 'true',theme: 'light'
              });
  
              var instances = $.tooltipster.instances($myElement);
  
              instances[0];
              instances[1];
  
              });
  
              // C4
              $(document).ready(function() {
          
              var $myElement = $('#C4');
              var titulo = " Juridical Conditions and Rights of  the Undocumented Migrants<br> ";
              var primer = "<a href='connectors/refere.html' style='color:#22bfe2; text-decoration: none'>1.	Primer precedente</a><br> " ;
              var  mimportante = " <a href='connectors/refere.html' style='text-decoration: none; color:#37dd27' >2.	El mas importante</a><br> ";
              var u = " <a href='connectors/refere.html' style='text-decoration: none; color:#ffa100' >3.	Ultimo precedente</a><br> ";
              var cambio = " <a href='connectors/refere.html 'style='text-decoration: none; color:#ff66dd' >4.	cambio de jurisprudencia</a><br> ";
              var t = titulo + primer+ mimportante+ u+ cambio;
  
              $myElement.tooltipster({
                  content: t,
                  side: 'top',
                  interactive: 'true',
                  theme: 'tooltipster-punk',
                  contentAsHTML: true
              });
              $(document).ready(function() {
                  $('.Autor').tooltipster(
                      { theme: 'tooltipster-punk'}
                  );
              });
  
              $myElement.tooltipster({
                  content: 'Alvaro Paul',
                  multiple: true,
                  side: 'bottom',interactive: 'true',theme: 'light'
              });
  
              var instances = $.tooltipster.instances($myElement);
  
              instances[0];
              instances[1];
  
              });
  
              // C5
              $(document).ready(function() {
          
              var $myElement = $('#C5');
              var titulo = " Juridical Conditions and Rights of  the Undocumented Migrants<br> ";
              var primer = "<a href='connectors/refere.html' style='color:#22bfe2; text-decoration: none'>1.	Primer precedente</a><br> " ;
              var  mimportante = " <a href='connectors/refere.html' style='text-decoration: none; color:#37dd27' >2.	El mas importante</a><br> ";
              var u = " <a href='connectors/refere.html' style='text-decoration: none; color:#ffa100' >3.	Ultimo precedente</a><br> ";
              var cambio = " <a href='connectors/refere.html 'style='text-decoration: none; color:#ff66dd' >4.	cambio de jurisprudencia</a><br> ";
              var t = titulo + primer+ mimportante+ u+ cambio;
  
              $myElement.tooltipster({
                  content: t,
                  side: 'top',
                  interactive: 'true',
                  theme: 'tooltipster-punk',
                  contentAsHTML: true
              });
              $(document).ready(function() {
                  $('.Autor').tooltipster(
                      { theme: 'tooltipster-punk'}
                  );
              });
  
              $myElement.tooltipster({
                  content: 'Alvaro Paul',
                  multiple: true,
                  side: 'bottom',interactive: 'true',theme: 'light'
              });
  
              var instances = $.tooltipster.instances($myElement);
  
              instances[0];
              instances[1];
  
              });
  
              // C6
              $(document).ready(function() {
          
              var $myElement = $('#C6');
              var titulo = " Juridical Conditions and Rights of  the Undocumented Migrants<br> ";
              var primer = "<a href='connectors/refere.html' style='color:#22bfe2; text-decoration: none'>1.	Primer precedente</a><br> " ;
              var  mimportante = " <a href='connectors/refere.html' style='text-decoration: none; color:#37dd27' >2.	El mas importante</a><br> ";
              var u = " <a href='connectors/refere.html' style='text-decoration: none; color:#ffa100' >3.	Ultimo precedente</a><br> ";
              var cambio = " <a href='connectors/refere.html 'style='text-decoration: none; color:#ff66dd' >4.	cambio de jurisprudencia</a><br> ";
              var t = titulo + primer+ mimportante+ u+ cambio;
  
              $myElement.tooltipster({
                  content: t,
                  side: 'top',
                  interactive: 'true',
                  theme: 'tooltipster-punk',
                  contentAsHTML: true
              });
              $(document).ready(function() {
                  $('.Autor').tooltipster(
                      { theme: 'tooltipster-punk'}
                  );
              });
  
              $myElement.tooltipster({
                  content: 'Alvaro Paul',
                  multiple: true,
                  side: 'bottom',interactive: 'true',theme: 'light'
              });
  
              var instances = $.tooltipster.instances($myElement);
  
              instances[0];
              instances[1];
  
              });
  
              // C7
              $(document).ready(function() {
          
              var $myElement = $('#C7');
              var titulo = " Juridical Conditions and Rights of  the Undocumented Migrants<br> ";
              var primer = "<a href='connectors/refere.html' style='color:#22bfe2; text-decoration: none'>1.	Primer precedente</a><br> " ;
              var  mimportante = " <a href='connectors/refere.html' style='text-decoration: none; color:#37dd27' >2.	El mas importante</a><br> ";
              var u = " <a href='connectors/refere.html' style='text-decoration: none; color:#ffa100' >3.	Ultimo precedente</a><br> ";
              var cambio = " <a href='connectors/refere.html 'style='text-decoration: none; color:#ff66dd' >4.	cambio de jurisprudencia</a><br> ";
              var t = titulo + primer+ mimportante+ u+ cambio;
  
              $myElement.tooltipster({
                  content: t,
                  side: 'top',
                  interactive: 'true',
                  theme: 'tooltipster-punk',
                  contentAsHTML: true
              });
              $(document).ready(function() {
                  $('.Autor').tooltipster(
                      { theme: 'tooltipster-punk'}
                  );
              });
  
              $myElement.tooltipster({
                  content: 'Alvaro Paul',
                  multiple: true,
                  side: 'bottom',interactive: 'true',theme: 'light'
              });
  
              var instances = $.tooltipster.instances($myElement);
  
              instances[0];
              instances[1];
  
              });
  
              // C8
              $(document).ready(function() {
          
              var $myElement = $('#C8');
              var titulo = " Juridical Conditions and Rights of  the Undocumented Migrants<br> ";
              var primer = "<a href='connectors/refere.html' style='color:#22bfe2; text-decoration: none'>1.	Primer precedente</a><br> " ;
              var  mimportante = " <a href='connectors/refere.html' style='text-decoration: none; color:#37dd27' >2.	El mas importante</a><br> ";
              var u = " <a href='connectors/refere.html' style='text-decoration: none; color:#ffa100' >3.	Ultimo precedente</a><br> ";
              var cambio = " <a href='connectors/refere.html 'style='text-decoration: none; color:#ff66dd' >4.	cambio de jurisprudencia</a><br> ";
              var t = titulo + primer+ mimportante+ u+ cambio;
  
              $myElement.tooltipster({
                  content: t,
                  side: 'top',
                  interactive: 'true',
                  theme: 'tooltipster-punk',
                  contentAsHTML: true
              });
              $(document).ready(function() {
                  $('.Autor').tooltipster(
                      { theme: 'tooltipster-punk'}
                  );
              });
  
              $myElement.tooltipster({
                  content: 'Alvaro Paul',
                  multiple: true,
                  side: 'bottom',interactive: 'true',theme: 'light'
              });
  
              var instances = $.tooltipster.instances($myElement);
  
              instances[0];
              instances[1];
  
              });
  
              // C9
              $(document).ready(function() {
          
              var $myElement = $('#C9');
              var titulo = " Juridical Conditions and Rights of  the Undocumented Migrants<br> ";
              var primer = "<a href='connectors/refere.html' style='color:#22bfe2; text-decoration: none'>1.	Primer precedente</a><br> " ;
              var  mimportante = " <a href='connectors/refere.html' style='text-decoration: none; color:#37dd27' >2.	El mas importante</a><br> ";
              var u = " <a href='connectors/refere.html' style='text-decoration: none; color:#ffa100' >3.	Ultimo precedente</a><br> ";
              var cambio = " <a href='connectors/refere.html 'style='text-decoration: none; color:#ff66dd' >4.	cambio de jurisprudencia</a><br> ";
              var t = titulo + primer+ mimportante+ u+ cambio;
  
              $myElement.tooltipster({
                  content: t,
                  side: 'top',
                  interactive: 'true',
                  theme: 'tooltipster-punk',
                  contentAsHTML: true
              });
              $(document).ready(function() {
                  $('.Autor').tooltipster(
                      { theme: 'tooltipster-punk'}
                  );
              });
  
              $myElement.tooltipster({
                  content: 'Alvaro Paul',
                  multiple: true,
                  side: 'bottom',interactive: 'true',theme: 'light'
              });
  
              var instances = $.tooltipster.instances($myElement);
  
              instances[0];
              instances[1];
  
              });
  
              // C10
              $(document).ready(function() {
          
              var $myElement = $('#C10');
              var titulo = " Juridical Conditions and Rights of  the Undocumented Migrants<br> ";
              var primer = "<a href='connectors/refere.html' style='color:#22bfe2; text-decoration: none'>1.	Primer precedente</a><br> " ;
              var  mimportante = " <a href='connectors/refere.html' style='text-decoration: none; color:#37dd27' >2.	El mas importante</a><br> ";
              var u = " <a href='connectors/refere.html' style='text-decoration: none; color:#ffa100' >3.	Ultimo precedente</a><br> ";
              var cambio = " <a href='connectors/refere.html 'style='text-decoration: none; color:#ff66dd' >4.	cambio de jurisprudencia</a><br> ";
              var t = titulo + primer+ mimportante+ u+ cambio;
  
              $myElement.tooltipster({
                  content: t,
                  side: 'top',
                  interactive: 'true',
                  theme: 'tooltipster-punk',
                  contentAsHTML: true
              });
              $(document).ready(function() {
                  $('.Autor').tooltipster(
                      { theme: 'tooltipster-punk'}
                  );
              });
  
              $myElement.tooltipster({
                  content: 'Alvaro Paul',
                  multiple: true,
                  side: 'bottom',interactive: 'true',theme: 'light'
              });
  
              var instances = $.tooltipster.instances($myElement);
  
              instances[0];
              instances[1];
  
              });
  
               // C11
               $(document).ready(function() {
          
              var $myElement = $('#C11');
              var titulo = " Juridical Conditions and Rights of  the Undocumented Migrants<br> ";
              var primer = "<a href='connectors/refere.html' style='color:#22bfe2; text-decoration: none'>1.	Primer precedente</a><br> " ;
              var  mimportante = " <a href='connectors/refere.html' style='text-decoration: none; color:#37dd27' >2.	El mas importante</a><br> ";
              var u = " <a href='connectors/refere.html' style='text-decoration: none; color:#ffa100' >3.	Ultimo precedente</a><br> ";
              var cambio = " <a href='connectors/refere.html 'style='text-decoration: none; color:#ff66dd' >4.	cambio de jurisprudencia</a><br> ";
              var t = titulo + primer+ mimportante+ u+ cambio;
  
              $myElement.tooltipster({
                  content: t,
                  side: 'top',
                  interactive: 'true',
                  theme: 'tooltipster-punk',
                  contentAsHTML: true
              });
              $(document).ready(function() {
                  $('.Autor').tooltipster(
                      { theme: 'tooltipster-punk'}
                  );
              });
  
              $myElement.tooltipster({
                  content: 'Alvaro Paul',
                  multiple: true,
                  side: 'bottom',interactive: 'true',theme: 'light'
              });
  
              var instances = $.tooltipster.instances($myElement);
  
              instances[0];
              instances[1];
  
              });
  
               // C12
               $(document).ready(function() {
          
              var $myElement = $('#C12');
              var titulo = " Juridical Conditions and Rights of  the Undocumented Migrants<br> ";
              var primer = "<a href='connectors/refere.html' style='color:#22bfe2; text-decoration: none'>1.	Primer precedente</a><br> " ;
              var  mimportante = " <a href='connectors/refere.html' style='text-decoration: none; color:#37dd27' >2.	El mas importante</a><br> ";
              var u = " <a href='connectors/refere.html' style='text-decoration: none; color:#ffa100' >3.	Ultimo precedente</a><br> ";
              var cambio = " <a href='connectors/refere.html 'style='text-decoration: none; color:#ff66dd' >4.	cambio de jurisprudencia</a><br> ";
              var t = titulo + primer+ mimportante+ u+ cambio;
  
              $myElement.tooltipster({
                  content: t,
                  side: 'top',
                  interactive: 'true',
                  theme: 'tooltipster-punk',
                  contentAsHTML: true
              });
              $(document).ready(function() {
                  $('.Autor').tooltipster(
                      { theme: 'tooltipster-punk'}
                  );
              });
  
              $myElement.tooltipster({
                  content: 'Alvaro Paul',
                  multiple: true,
                  side: 'bottom',interactive: 'true',theme: 'light'
              });
  
              var instances = $.tooltipster.instances($myElement);
  
              instances[0];
              instances[1];
  
              });
  
               // C13
               $(document).ready(function() {
          
              var $myElement = $('#C13');
              var titulo = " Juridical Conditions and Rights of  the Undocumented Migrants<br> ";
              var primer = "<a href='connectors/refere.html' style='color:#22bfe2; text-decoration: none'>1.	Primer precedente</a><br> " ;
              var  mimportante = " <a href='connectors/refere.html' style='text-decoration: none; color:#37dd27' >2.	El mas importante</a><br> ";
              var u = " <a href='connectors/refere.html' style='text-decoration: none; color:#ffa100' >3.	Ultimo precedente</a><br> ";
              var cambio = " <a href='connectors/refere.html 'style='text-decoration: none; color:#ff66dd' >4.	cambio de jurisprudencia</a><br> ";
              var t = titulo + primer+ mimportante+ u+ cambio;
  
              $myElement.tooltipster({
                  content: t,
                  side: 'top',
                  interactive: 'true',
                  theme: 'tooltipster-punk',
                  contentAsHTML: true
              });
              $(document).ready(function() {
                  $('.Autor').tooltipster(
                      { theme: 'tooltipster-punk'}
                  );
              });
  
              $myElement.tooltipster({
                  content: 'Alvaro Paul',
                  multiple: true,
                  side: 'bottom',interactive: 'true',theme: 'light'
              });
  
              var instances = $.tooltipster.instances($myElement);
  
              instances[0];
              instances[1];
  
              });
  
               // C14
               $(document).ready(function() {
          
              var $myElement = $('#C14');
              var titulo = " Juridical Conditions and Rights of  the Undocumented Migrants<br> ";
              var primer = "<a href='connectors/refere.html' style='color:#22bfe2; text-decoration: none'>1.	Primer precedente</a><br> " ;
              var  mimportante = " <a href='connectors/refere.html' style='text-decoration: none; color:#37dd27' >2.	El mas importante</a><br> ";
              var u = " <a href='connectors/refere.html' style='text-decoration: none; color:#ffa100' >3.	Ultimo precedente</a><br> ";
              var cambio = " <a href='connectors/refere.html 'style='text-decoration: none; color:#ff66dd' >4.	cambio de jurisprudencia</a><br> ";
              var t = titulo + primer+ mimportante+ u+ cambio;
  
              $myElement.tooltipster({
                  content: t,
                  side: 'top',
                  interactive: 'true',
                  theme: 'tooltipster-punk',
                  contentAsHTML: true
              });
              $(document).ready(function() {
                  $('.Autor').tooltipster(
                      { theme: 'tooltipster-punk'}
                  );
              });
  
              $myElement.tooltipster({
                  content: 'Alvaro Paul',
                  multiple: true,
                  side: 'bottom',interactive: 'true',theme: 'light'
              });
  
              var instances = $.tooltipster.instances($myElement);
  
              instances[0];
              instances[1];
  
              });
  
               // C15
               $(document).ready(function() {
          
              var $myElement = $('#C15');
              var titulo = " Juridical Conditions and Rights of  the Undocumented Migrants<br> ";
              var primer = "<a href='connectors/refere.html' style='color:#22bfe2; text-decoration: none'>1.	Primer precedente</a><br> " ;
              var  mimportante = " <a href='connectors/refere.html' style='text-decoration: none; color:#37dd27' >2.	El mas importante</a><br> ";
              var u = " <a href='connectors/refere.html' style='text-decoration: none; color:#ffa100' >3.	Ultimo precedente</a><br> ";
              var cambio = " <a href='connectors/refere.html 'style='text-decoration: none; color:#ff66dd' >4.	cambio de jurisprudencia</a><br> ";
              var t = titulo + primer+ mimportante+ u+ cambio;
  
              $myElement.tooltipster({
                  content: t,
                  side: 'top',
                  interactive: 'true',
                  theme: 'tooltipster-punk',
                  contentAsHTML: true
              });
              $(document).ready(function() {
                  $('.Autor').tooltipster(
                      { theme: 'tooltipster-punk'}
                  );
              });
  
              $myElement.tooltipster({
                  content: 'Alvaro Paul',
                  multiple: true,
                  side: 'bottom',interactive: 'true',theme: 'light'
              });
  
              var instances = $.tooltipster.instances($myElement);
  
              instances[0];
              instances[1];
  
              });
  
               // C16
               $(document).ready(function() {
          
              var $myElement = $('#C16');
              var titulo = " Juridical Conditions and Rights of  the Undocumented Migrants<br> ";
              var primer = "<a href='connectors/refere.html' style='color:#22bfe2; text-decoration: none'>1.	Primer precedente</a><br> " ;
              var  mimportante = " <a href='connectors/refere.html' style='text-decoration: none; color:#37dd27' >2.	El mas importante</a><br> ";
              var u = " <a href='connectors/refere.html' style='text-decoration: none; color:#ffa100' >3.	Ultimo precedente</a><br> ";
              var cambio = " <a href='connectors/refere.html 'style='text-decoration: none; color:#ff66dd' >4.	cambio de jurisprudencia</a><br> ";
              var t = titulo + primer+ mimportante+ u+ cambio;
  
              $myElement.tooltipster({
                  content: t,
                  side: 'top',
                  interactive: 'true',
                  theme: 'tooltipster-punk',
                  contentAsHTML: true
              });
              $(document).ready(function() {
                  $('.Autor').tooltipster(
                      { theme: 'tooltipster-punk'}
                  );
              });
  
              $myElement.tooltipster({
                  content: 'Alvaro Paul',
                  multiple: true,
                  side: 'bottom',interactive: 'true',theme: 'light'
              });
  
              var instances = $.tooltipster.instances($myElement);
  
              instances[0];
              instances[1];
  
              });
  
               // C17
               $(document).ready(function() {
          
              var $myElement = $('#C17');
              var titulo = " Juridical Conditions and Rights of  the Undocumented Migrants<br> ";
              var primer = "<a href='connectors/refere.html' style='color:#22bfe2; text-decoration: none'>1.	Primer precedente</a><br> " ;
              var  mimportante = " <a href='connectors/refere.html' style='text-decoration: none; color:#37dd27' >2.	El mas importante</a><br> ";
              var u = " <a href='connectors/refere.html' style='text-decoration: none; color:#ffa100' >3.	Ultimo precedente</a><br> ";
              var cambio = " <a href='connectors/refere.html 'style='text-decoration: none; color:#ff66dd' >4.	cambio de jurisprudencia</a><br> ";
              var t = titulo + primer+ mimportante+ u+ cambio;
  
              $myElement.tooltipster({
                  content: t,
                  side: 'top',
                  interactive: 'true',
                  theme: 'tooltipster-punk',
                  contentAsHTML: true
              });
              $(document).ready(function() {
                  $('.Autor').tooltipster(
                      { theme: 'tooltipster-punk'}
                  );
              });
  
              $myElement.tooltipster({
                  content: 'Alvaro Paul',
                  multiple: true,
                  side: 'bottom',interactive: 'true',theme: 'light'
              });
  
              var instances = $.tooltipster.instances($myElement);
  
              instances[0];
              instances[1];
  
              });
  
               // C18
               $(document).ready(function() {
          
              var $myElement = $('#C18');
              var titulo = " Juridical Conditions and Rights of  the Undocumented Migrants<br> ";
              var primer = "<a href='connectors/refere.html' style='color:#22bfe2; text-decoration: none'>1.	Primer precedente</a><br> " ;
              var  mimportante = " <a href='connectors/refere.html' style='text-decoration: none; color:#37dd27' >2.	El mas importante</a><br> ";
              var u = " <a href='connectors/refere.html' style='text-decoration: none; color:#ffa100' >3.	Ultimo precedente</a><br> ";
              var cambio = " <a href='connectors/refere.html 'style='text-decoration: none; color:#ff66dd' >4.	cambio de jurisprudencia</a><br> ";
              var t = titulo + primer+ mimportante+ u+ cambio;
  
              $myElement.tooltipster({
                  content: t,
                  side: 'top',
                  interactive: 'true',
                  theme: 'tooltipster-punk',
                  contentAsHTML: true
              });
              $(document).ready(function() {
                  $('.Autor').tooltipster(
                      { theme: 'tooltipster-punk'}
                  );
              });
  
              $myElement.tooltipster({
                  content: 'Alvaro Paul',
                  multiple: true,
                  side: 'bottom',interactive: 'true',theme: 'light'
              });
  
              var instances = $.tooltipster.instances($myElement);
  
              instances[0];
              instances[1];
  
              });
  
               // C19
               $(document).ready(function() {
          
              var $myElement = $('#C19');
              var titulo = " Juridical Conditions and Rights of  the Undocumented Migrants<br> ";
              var primer = "<a href='connectors/refere.html' style='color:#22bfe2; text-decoration: none'>1.	Primer precedente</a><br> " ;
              var  mimportante = " <a href='connectors/refere.html' style='text-decoration: none; color:#37dd27' >2.	El mas importante</a><br> ";
              var u = " <a href='connectors/refere.html' style='text-decoration: none; color:#ffa100' >3.	Ultimo precedente</a><br> ";
              var cambio = " <a href='connectors/refere.html 'style='text-decoration: none; color:#ff66dd' >4.	cambio de jurisprudencia</a><br> ";
              var t = titulo + primer+ mimportante+ u+ cambio;
  
              $myElement.tooltipster({
                  content: t,
                  side: 'top',
                  interactive: 'true',
                  theme: 'tooltipster-punk',
                  contentAsHTML: true
              });
              $(document).ready(function() {
                  $('.Autor').tooltipster(
                      { theme: 'tooltipster-punk'}
                  );
              });
  
              $myElement.tooltipster({
                  content: 'Alvaro Paul',
                  multiple: true,
                  side: 'bottom',interactive: 'true',theme: 'light'
              });
  
              var instances = $.tooltipster.instances($myElement);
  
              instances[0];
              instances[1];
  
              });
  
               // C20
               $(document).ready(function() {
          
              var $myElement = $('#C20');
              var titulo = " Juridical Conditions and Rights of  the Undocumented Migrants<br> ";
              var primer = "<a href='connectors/refere.html' style='color:#22bfe2; text-decoration: none'>1.	Primer precedente</a><br> " ;
              var  mimportante = " <a href='connectors/refere.html' style='text-decoration: none; color:#37dd27' >2.	El mas importante</a><br> ";
              var u = " <a href='connectors/refere.html' style='text-decoration: none; color:#ffa100' >3.	Ultimo precedente</a><br> ";
              var cambio = " <a href='connectors/refere.html 'style='text-decoration: none; color:#ff66dd' >4.	cambio de jurisprudencia</a><br> ";
              var t = titulo + primer+ mimportante+ u+ cambio;
  
              $myElement.tooltipster({
                  content: t,
                  side: 'top',
                  interactive: 'true',
                  theme: 'tooltipster-punk',
                  contentAsHTML: true
              });
              $(document).ready(function() {
                  $('.Autor').tooltipster(
                      { theme: 'tooltipster-punk'}
                  );
              });
  
              $myElement.tooltipster({
                  content: 'Alvaro Paul',
                  multiple: true,
                  side: 'bottom',interactive: 'true',theme: 'light'
              });
  
              var instances = $.tooltipster.instances($myElement);
  
              instances[0];
              instances[1];
  
              });
  
              // C21
              $(document).ready(function() {
          
              var $myElement = $('#C21');
              var titulo = " Juridical Conditions and Rights of  the Undocumented Migrants<br> ";
              var primer = "<a href='connectors/refere.html' style='color:#22bfe2; text-decoration: none'>1.	Primer precedente</a><br> " ;
              var  mimportante = " <a href='connectors/refere.html' style='text-decoration: none; color:#37dd27' >2.	El mas importante</a><br> ";
              var u = " <a href='connectors/refere.html' style='text-decoration: none; color:#ffa100' >3.	Ultimo precedente</a><br> ";
              var cambio = " <a href='connectors/refere.html 'style='text-decoration: none; color:#ff66dd' >4.	cambio de jurisprudencia</a><br> ";
              var t = titulo + primer+ mimportante+ u+ cambio;
  
              $myElement.tooltipster({
                  content: t,
                  side: 'top',
                  interactive: 'true',
                  theme: 'tooltipster-punk',
                  contentAsHTML: true
              });
              $(document).ready(function() {
                  $('.Autor').tooltipster(
                      { theme: 'tooltipster-punk'}
                  );
              });
  
              $myElement.tooltipster({
                  content: 'Alvaro Paul',
                  multiple: true,
                  side: 'bottom',interactive: 'true',theme: 'light'
              });
  
              var instances = $.tooltipster.instances($myElement);
  
              instances[0];
              instances[1];
  
              });
  
              // C22
              $(document).ready(function() {
          
              var $myElement = $('#C22');
              var titulo = " Juridical Conditions and Rights of  the Undocumented Migrants<br> ";
              var primer = "<a href='connectors/refere.html' style='color:#22bfe2; text-decoration: none'>1.	Primer precedente</a><br> " ;
              var  mimportante = " <a href='connectors/refere.html' style='text-decoration: none; color:#37dd27' >2.	El mas importante</a><br> ";
              var u = " <a href='connectors/refere.html' style='text-decoration: none; color:#ffa100' >3.	Ultimo precedente</a><br> ";
              var cambio = " <a href='connectors/refere.html 'style='text-decoration: none; color:#ff66dd' >4.	cambio de jurisprudencia</a><br> ";
              var t = titulo + primer+ mimportante+ u+ cambio;
  
              $myElement.tooltipster({
                  content: t,
                  side: 'top',
                  interactive: 'true',
                  theme: 'tooltipster-punk',
                  contentAsHTML: true
              });
              $(document).ready(function() {
                  $('.Autor').tooltipster(
                      { theme: 'tooltipster-punk'}
                  );
              });
  
              $myElement.tooltipster({
                  content: 'Alvaro Paul',
                  multiple: true,
                  side: 'bottom',interactive: 'true',theme: 'light'
              });
  
              var instances = $.tooltipster.instances($myElement);
  
              instances[0];
              instances[1];
  
              });
  
              // C23
              $(document).ready(function() {
          
              var $myElement = $('#C23');
              var titulo = " Juridical Conditions and Rights of  the Undocumented Migrants<br> ";
              var primer = "<a href='connectors/refere.html' style='color:#22bfe2; text-decoration: none'>1.	Primer precedente</a><br> " ;
              var  mimportante = " <a href='connectors/refere.html' style='text-decoration: none; color:#37dd27' >2.	El mas importante</a><br> ";
              var u = " <a href='connectors/refere.html' style='text-decoration: none; color:#ffa100' >3.	Ultimo precedente</a><br> ";
              var cambio = " <a href='connectors/refere.html 'style='text-decoration: none; color:#ff66dd' >4.	cambio de jurisprudencia</a><br> ";
              var t = titulo + primer+ mimportante+ u+ cambio;
  
              $myElement.tooltipster({
                  content: t,
                  side: 'top',
                  interactive: 'true',
                  theme: 'tooltipster-punk',
                  contentAsHTML: true
              });
              $(document).ready(function() {
                  $('.Autor').tooltipster(
                      { theme: 'tooltipster-punk'}
                  );
              });
  
              $myElement.tooltipster({
                  content: 'Alvaro Paul',
                  multiple: true,
                  side: 'bottom',interactive: 'true',theme: 'light'
              });
  
              var instances = $.tooltipster.instances($myElement);
  
              instances[0];
              instances[1];
  
              });
  
              // C24
              $(document).ready(function() {
          
              var $myElement = $('#C24');
              var titulo = " Juridical Conditions and Rights of  the Undocumented Migrants<br> ";
              var primer = "<a href='connectors/refere.html' style='color:#22bfe2; text-decoration: none'>1.	Primer precedente</a><br> " ;
              var  mimportante = " <a href='connectors/refere.html' style='text-decoration: none; color:#37dd27' >2.	El mas importante</a><br> ";
              var u = " <a href='connectors/refere.html' style='text-decoration: none; color:#ffa100' >3.	Ultimo precedente</a><br> ";
              var cambio = " <a href='connectors/refere.html 'style='text-decoration: none; color:#ff66dd' >4.	cambio de jurisprudencia</a><br> ";
              var t = titulo + primer+ mimportante+ u+ cambio;
  
              $myElement.tooltipster({
                  content: t,
                  side: 'top',
                  interactive: 'true',
                  theme: 'tooltipster-punk',
                  contentAsHTML: true
              });
              $(document).ready(function() {
                  $('.Autor').tooltipster(
                      { theme: 'tooltipster-punk'}
                  );
              });
  
              $myElement.tooltipster({
                  content: 'Alvaro Paul',
                  multiple: true,
                  side: 'bottom',interactive: 'true',theme: 'light'
              });
  
              var instances = $.tooltipster.instances($myElement);
  
              instances[0];
              instances[1];
  
              });
  
              // C24
              $(document).ready(function() {
          
              var $myElement = $('#C25');
              var titulo = " Juridical Conditions and Rights of  the Undocumented Migrants<br> ";
              var primer = "<a href='connectors/refere.html' style='color:#22bfe2; text-decoration: none'>1.	Primer precedente</a><br> " ;
              var  mimportante = " <a href='connectors/refere.html' style='text-decoration: none; color:#37dd27' >2.	El mas importante</a><br> ";
              var u = " <a href='connectors/refere.html' style='text-decoration: none; color:#ffa100' >3.	Ultimo precedente</a><br> ";
              var cambio = " <a href='connectors/refere.html 'style='text-decoration: none; color:#ff66dd' >4.	cambio de jurisprudencia</a><br> ";
              var t = titulo + primer+ mimportante+ u+ cambio;
  
              $myElement.tooltipster({
                  content: t,
                  side: 'top',
                  interactive: 'true',
                  theme: 'tooltipster-punk',
                  contentAsHTML: true
              });
              $(document).ready(function() {
                  $('.Autor').tooltipster(
                      { theme: 'tooltipster-punk'}
                  );
              });
  
              $myElement.tooltipster({
                  content: 'Alvaro Paul',
                  multiple: true,
                  side: 'bottom',interactive: 'true',theme: 'light'
              });
  
              var instances = $.tooltipster.instances($myElement);
  
              instances[0];
              instances[1];
  
              });
  
              // C26
              $(document).ready(function() {
          
              var $myElement = $('#C26');
              var titulo = " Juridical Conditions and Rights of  the Undocumented Migrants<br> ";
              var primer = "<a href='connectors/refere.html' style='color:#22bfe2; text-decoration: none'>1.	Primer precedente</a><br> " ;
              var  mimportante = " <a href='connectors/refere.html' style='text-decoration: none; color:#37dd27' >2.	El mas importante</a><br> ";
              var u = " <a href='connectors/refere.html' style='text-decoration: none; color:#ffa100' >3.	Ultimo precedente</a><br> ";
              var cambio = " <a href='connectors/refere.html 'style='text-decoration: none; color:#ff66dd' >4.	cambio de jurisprudencia</a><br> ";
              var t = titulo + primer+ mimportante+ u+ cambio;
  
              $myElement.tooltipster({
                  content: t,
                  side: 'top',
                  interactive: 'true',
                  theme: 'tooltipster-punk',
                  contentAsHTML: true
              });
              $(document).ready(function() {
                  $('.Autor').tooltipster(
                      { theme: 'tooltipster-punk'}
                  );
              });
  
              $myElement.tooltipster({
                  content: 'Alvaro Paul',
                  multiple: true,
                  side: 'bottom',interactive: 'true',theme: 'light'
              });
  
              var instances = $.tooltipster.instances($myElement);
  
              instances[0];
              instances[1];
  
              });
  
              // C27
              $(document).ready(function() {
          
              var $myElement = $('#C27');
              var titulo = " Juridical Conditions and Rights of  the Undocumented Migrants<br> ";
              var primer = "<a href='connectors/refere.html' style='color:#22bfe2; text-decoration: none'>1.	Primer precedente</a><br> " ;
              var  mimportante = " <a href='connectors/refere.html' style='text-decoration: none; color:#37dd27' >2.	El mas importante</a><br> ";
              var u = " <a href='connectors/refere.html' style='text-decoration: none; color:#ffa100' >3.	Ultimo precedente</a><br> ";
              var cambio = " <a href='connectors/refere.html 'style='text-decoration: none; color:#ff66dd' >4.	cambio de jurisprudencia</a><br> ";
              var t = titulo + primer+ mimportante+ u+ cambio;
  
              $myElement.tooltipster({
                  content: t,
                  side: 'top',
                  interactive: 'true',
                  theme: 'tooltipster-punk',
                  contentAsHTML: true
              });
              $(document).ready(function() {
                  $('.Autor').tooltipster(
                      { theme: 'tooltipster-punk'}
                  );
              });
  
              $myElement.tooltipster({
                  content: 'Alvaro Paul',
                  multiple: true,
                  side: 'bottom',interactive: 'true',theme: 'light'
              });
  
              var instances = $.tooltipster.instances($myElement);
  
              instances[0];
              instances[1];
  
              });
  
              // C28
              $(document).ready(function() {
          
              var $myElement = $('#C28');
              var titulo = " Juridical Conditions and Rights of  the Undocumented Migrants<br> ";
              var primer = "<a href='connectors/refere.html' style='color:#22bfe2; text-decoration: none'>1.	Primer precedente</a><br> " ;
              var  mimportante = " <a href='connectors/refere.html' style='text-decoration: none; color:#37dd27' >2.	El mas importante</a><br> ";
              var u = " <a href='connectors/refere.html' style='text-decoration: none; color:#ffa100' >3.	Ultimo precedente</a><br> ";
              var cambio = " <a href='connectors/refere.html 'style='text-decoration: none; color:#ff66dd' >4.	cambio de jurisprudencia</a><br> ";
              var t = titulo + primer+ mimportante+ u+ cambio;
  
              $myElement.tooltipster({
                  content: t,
                  side: 'top',
                  interactive: 'true',
                  theme: 'tooltipster-punk',
                  contentAsHTML: true
              });
              $(document).ready(function() {
                  $('.Autor').tooltipster(
                      { theme: 'tooltipster-punk'}
                  );
              });
  
              $myElement.tooltipster({
                  content: 'Alvaro Paul',
                  multiple: true,
                  side: 'bottom',interactive: 'true',theme: 'light'
              });
  
              var instances = $.tooltipster.instances($myElement);
  
              instances[0];
              instances[1];
  
              });
  
              // C29
              $(document).ready(function() {
          
              var $myElement = $('#C29');
              var titulo = " Juridical Conditions and Rights of  the Undocumented Migrants<br> ";
              var primer = "<a href='connectors/refere.html' style='color:#22bfe2; text-decoration: none'>1.	Primer precedente</a><br> " ;
              var  mimportante = " <a href='connectors/refere.html' style='text-decoration: none; color:#37dd27' >2.	El mas importante</a><br> ";
              var u = " <a href='connectors/refere.html' style='text-decoration: none; color:#ffa100' >3.	Ultimo precedente</a><br> ";
              var cambio = " <a href='connectors/refere.html 'style='text-decoration: none; color:#ff66dd' >4.	cambio de jurisprudencia</a><br> ";
              var t = titulo + primer+ mimportante+ u+ cambio;
  
              $myElement.tooltipster({
                  content: t,
                  side: 'top',
                  interactive: 'true',
                  theme: 'tooltipster-punk',
                  contentAsHTML: true
              });
              $(document).ready(function() {
                  $('.Autor').tooltipster(
                      { theme: 'tooltipster-punk'}
                  );
              });
  
              $myElement.tooltipster({
                  content: 'Alvaro Paul',
                  multiple: true,
                  side: 'bottom',interactive: 'true',theme: 'light'
              });
  
              var instances = $.tooltipster.instances($myElement);
  
              instances[0];
              instances[1];
  
              });
  
              // C30
              $(document).ready(function() {
          
              var $myElement = $('#C30');
              var titulo = " Juridical Conditions and Rights of  the Undocumented Migrants<br> ";
              var primer = "<a href='connectors/refere.html' style='color:#22bfe2; text-decoration: none'>1.	Primer precedente</a><br> " ;
              var  mimportante = " <a href='connectors/refere.html' style='text-decoration: none; color:#37dd27' >2.	El mas importante</a><br> ";
              var u = " <a href='connectors/refere.html' style='text-decoration: none; color:#ffa100' >3.	Ultimo precedente</a><br> ";
              var cambio = " <a href='connectors/refere.html 'style='text-decoration: none; color:#ff66dd' >4.	cambio de jurisprudencia</a><br> ";
              var t = titulo + primer+ mimportante+ u+ cambio;
  
              $myElement.tooltipster({
                  content: t,
                  side: 'top',
                  interactive: 'true',
                  theme: 'tooltipster-punk',
                  contentAsHTML: true
              });
              $(document).ready(function() {
                  $('.Autor').tooltipster(
                      { theme: 'tooltipster-punk'}
                  );
              });
  
              $myElement.tooltipster({
                  content: 'Alvaro Paul',
                  multiple: true,
                  side: 'bottom',interactive: 'true',theme: 'light'
              });
  
              var instances = $.tooltipster.instances($myElement);
  
              instances[0];
              instances[1];
  
              });
  
              // C31
              $(document).ready(function() {
          
              var $myElement = $('#C31');
              var titulo = " Juridical Conditions and Rights of  the Undocumented Migrants<br> ";
              var primer = "<a href='connectors/refere.html' style='color:#22bfe2; text-decoration: none'>1.	Primer precedente</a><br> " ;
              var  mimportante = " <a href='connectors/refere.html' style='text-decoration: none; color:#37dd27' >2.	El mas importante</a><br> ";
              var u = " <a href='connectors/refere.html' style='text-decoration: none; color:#ffa100' >3.	Ultimo precedente</a><br> ";
              var cambio = " <a href='connectors/refere.html 'style='text-decoration: none; color:#ff66dd' >4.	cambio de jurisprudencia</a><br> ";
              var t = titulo + primer+ mimportante+ u+ cambio;
  
              $myElement.tooltipster({
                  content: t,
                  side: 'top',
                  interactive: 'true',
                  theme: 'tooltipster-punk',
                  contentAsHTML: true
              });
              $(document).ready(function() {
                  $('.Autor').tooltipster(
                      { theme: 'tooltipster-punk'}
                  );
              });
  
              $myElement.tooltipster({
                  content: 'Alvaro Paul',
                  multiple: true,
                  side: 'bottom',interactive: 'true',theme: 'light'
              });
  
              var instances = $.tooltipster.instances($myElement);
  
              instances[0];
              instances[1];
  
              });
  
              // C32
              $(document).ready(function() {
          
              var $myElement = $('#C32');
              var titulo = " Juridical Conditions and Rights of  the Undocumented Migrants<br> ";
              var primer = "<a href='connectors/refere.html' style='color:#22bfe2; text-decoration: none'>1.	Primer precedente</a><br> " ;
              var  mimportante = " <a href='connectors/refere.html' style='text-decoration: none; color:#37dd27' >2.	El mas importante</a><br> ";
              var u = " <a href='connectors/refere.html' style='text-decoration: none; color:#ffa100' >3.	Ultimo precedente</a><br> ";
              var cambio = " <a href='connectors/refere.html 'style='text-decoration: none; color:#ff66dd' >4.	cambio de jurisprudencia</a><br> ";
              var t = titulo + primer+ mimportante+ u+ cambio;
  
              $myElement.tooltipster({
                  content: t,
                  side: 'top',
                  interactive: 'true',
                  theme: 'tooltipster-punk',
                  contentAsHTML: true
              });
              $(document).ready(function() {
                  $('.Autor').tooltipster(
                      { theme: 'tooltipster-punk'}
                  );
              });
  
              $myElement.tooltipster({
                  content: 'Alvaro Paul',
                  multiple: true,
                  side: 'bottom',interactive: 'true',theme: 'light'
              });
  
              var instances = $.tooltipster.instances($myElement);
  
              instances[0];
              instances[1];
  
              });
  
              // C33
              $(document).ready(function() {
          
              var $myElement = $('#C33');
              var titulo = " Juridical Conditions and Rights of  the Undocumented Migrants<br> ";
              var primer = "<a href='connectors/refere.html' style='color:#22bfe2; text-decoration: none'>1.	Primer precedente</a><br> " ;
              var  mimportante = " <a href='connectors/refere.html' style='text-decoration: none; color:#37dd27' >2.	El mas importante</a><br> ";
              var u = " <a href='connectors/refere.html' style='text-decoration: none; color:#ffa100' >3.	Ultimo precedente</a><br> ";
              var cambio = " <a href='connectors/refere.html 'style='text-decoration: none; color:#ff66dd' >4.	cambio de jurisprudencia</a><br> ";
              var t = titulo + primer+ mimportante+ u+ cambio;
  
              $myElement.tooltipster({
                  content: t,
                  side: 'top',
                  interactive: 'true',
                  theme: 'tooltipster-punk',
                  contentAsHTML: true
              });
              $(document).ready(function() {
                  $('.Autor').tooltipster(
                      { theme: 'tooltipster-punk'}
                  );
              });
  
              $myElement.tooltipster({
                  content: 'Alvaro Paul',
                  multiple: true,
                  side: 'bottom',interactive: 'true',theme: 'light'
              });
  
              var instances = $.tooltipster.instances($myElement);
  
              instances[0];
              instances[1];
  
              });
          </script>
  
    
    
    </head>
      
            <!-- <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                    <a class="navbar-brand" href="#">Navbar</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                      <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNav">
                      <ul class="navbar-nav">
                        <li class="nav-item ">
                          <a class="nav-link" href="home.html">Home </a>
                        </li>
                        <li class="nav-item active">
                          <a class="nav-link" href="#">CADH <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="about.html">Sobre Nosotros</a>
                        </li>
                        <li class="nav-item">
                                <a class="nav-link" href="contacto.html">Contacto</a>
                        </li>
                      </ul>
                    </div>
                  </nav> -->

                  <div class="contenedorPrincipal">
                  <button class="btn" title="I'm a tooltip!">Text</button>
                  <script>
                tippy('.btn')
                </script>
                                   
                                   <h2 align="center"><b>The American Convention on Human Rights
                                        <br> Updated by the Inter-American Court 
                                         </b></h2>
                                         <br>
                                         <br>
                     
                                    <h3 align="center"><em>Adopted at the Inter-American Specialized Conference on Human Rights,<br>
                                         San José, Costa Rica, 22 November 1969
                                         </em><br>(as interpreted by the Inter-American Court up to 2016)</h3>
                                         
                                    <br>
                                    <br>
                                    <h2 align="center"><b>PART I - STATE OBLIGATIONS AND RIGHTS PROTECTED
                                         <br>
                                         <br>
                                          CHAPTER I - GENERAL OBLIGATIONS
                                          </b>
                                     </h2>
                     
                                     <h4><b>&emsp; &emsp;       Article 1. Obligation to Respect Rights   </b></h4>
                                     
                                     <p align="justify"><b>1. &emsp;The States Parties to this Convention undertake to respect the rights and freedoms
                                          recognized herein and to ensure to all persons subject to their jurisdiction the free and full exercise of those 
                                          rights and freedoms, without any discrimination for reasons of race, color, 
                                          <span class="palabra">
                                                 
                                                <span class="5" id="C1">sex, 
                                             
                                                   </span>
                                         </span>
                                         ,
                                     
                                          
                                         </b>
                                         language, religion 
                                         <span class="palabra">
                                                 <span  id="C2">
                                                     or belief
                                     
                                               
                                                     </span>
                                         </span>
                     
                                         political or other opinion, national,
                                         
                                         <span class="palabra">
                                                 <span class="too" id="C3">ethnic
                                     
                                               
                                                     </span>
                                         </span>
                     
                                        
                                         or social origin, 
                 
                                         <span class="palabra">
                                                 <span class="too" id="C4">nationality, age, 
                                     
                                               
                                                     </span>
                                         </span>
                     
                                         
                                        economic status, 
                                     
                                        <span class="palabra">
                                             <span class="too" id="C5">property, civil status, 
                                 
                                           
                                                 </span>
                                     </span>
                     
                                
                                   birth,
                     
                                   <span class="palabra"> 
                                         <!-- C7 -->
                                        <span class="too" id="test2">sexual orientation, 
                                     
                                           </span>
                               </span>
                     
                     
                     
                               or any other
                 
                               <span class="palabra">
                                     
                                    <span class="too" id="C8"><del>social</del> 
                                 
                                       </span>
                           </span>
                 
                             
                               
                               condition.
                         </p>
                         <p>
                              1B.  &quot;[D]ecisions adopted by domestic bodies that could affect human rights must be duly funded and justified.  &quot;
                                 
                                         <span class="palabra">
                                                 
                                                <span class="too" id="C9"> <sup>[9]</sup> </span>
                                       </span>
                                       Otherwise, they will be considered arbitrary.
                                         <span class="palabra">
                                                 
                                             <span class="too" id="C10">  </span>
                                     </span> 
                                     
                                     In addition, States Parties must consult indigenous and tribal communities and peoples before taking administrative or legal measures affecting their rights .
                                         <span class="palabra">
                                                 
                                             <span class="too" id="C11"> <sup>[11]</sup>
                                                 </span>
                                     </span>          
                             
                                     
                                
                         <p>
                          <b>2.  For the purposes of this Convention, &quot;person&quot; means every human being.</b> 
                          .  This provision must be read in relation to Article 4(1).
                          <span class="palabra">
                                 
                             <span class="too" id="C12"> <sup>[12]</sup>
                                 </span>
                          </span>     
                              Legal persons are not right holders according to this Convention . 
                              <span class="palabra">
                                     
                                 <span class="too" id="C13"> <sup>[13]</sup>
                                     </span>
                         </span>   
                           However, indigenous communities, trade unions, federations and confederations may be considered victims.
                           <span class="palabra">
                                 
                             <span class="too" id="C14"> <sup>[14]</sup>
                                 </span>
                     </span>   
                           
                             Natural persons may appear before the Court claiming for actions committed against legal persons, whenever there is an essential and direct link between them .
                             <span class="palabra">
                                     
                                 <span class="too" id="C15"> <sup>[15]</sup>
                                     </span>
                         </span>   
                         </p>    
                 
                         <h4>&emsp; &emsp; Article 1B.  Right to the Truth   </h4>
                         <p>
                             1.  Every person has the right to know the truth about the human rights violations that affected him or his next of kin, in accordance with Articles 8 and 25 of this Convention. <span class="palabra">
                                 <span class="palabra"></span>    
                                 <span class="too" id="C16"> <sup>[16]</sup>
                                     </span>
                         </span>   
                             <br>
                             <br>
                             2. States Parties must take effective measures to prevent and investigate human rights violations, even by non-state actors.
                             <span class="palabra">
                             <span class="too" id="C17"> <sup>[17]</sup>
                             </span>
                             </span>   
                 
                             States Parties must promptly investigate those responsible for violations of human rights and, where possible, punish them.  
                             <span class="palabra">
                             <span class="too" id="C18"> <sup>[18]</sup>
                             </span>
                             </span>   
                                In cases where statutes of limitation are applicable, the state must adopt some kind of measure that allows the victim of human rights violations or their relatives to know what happened in a given case.
                                <span class="palabra">
                                <span class="too" id="C19"> <sup>[19]</sup>
                                </span>
                                 </span>   
                                   Victims or the next of kin of deceased
                                 victims shall have the right to participate in these proceedings.   
                                 <span class="palabra">
                                 <span class="too" id="C20"> <sup>[20]</sup>
                                 </span>
                                  </span>   
                             <br>
                             <br>
                 
                             3.  In cases of serious offenses to a human right, the right to the truth requires:
                             <div class="indent">
                                 a.  &quot; the procedural determination of the most complete historical truth as possible &quot;
                                 <span class="palabra">
                                 <span class="too" id="C21"> <sup>[21]</sup>
                                 </span>
                                  </span>   
                                 <br>
                                 b.  that the state does &quot;not invoke the statute of limitations, the non-retroactivity of criminal law or the ne bis in idem principle to decline its duty to investigate and punish those responsible.&quot;  <span class="palabra"> <span class="too" id="C22"> <sup>[22]</sup>
                                 </span>
                                  </span>  
                                 <br>
                                 c.  that states shall enact no amnesties in cases of serious human rights violations. <span class="palabra"><span class="too" id="C23"> <sup>[23]</sup>
                                 </span>
                                  </span>      This prohibition shall extend to amnesties benefitting both sides of a domestic conflict, <span class="palabra"><span class="too" id="C24"> <sup>[24]</sup>
                                  </span>
                                   </span>     and those enacted by democratic governments and ratified via referendum. <span class="palabra"> <span class="too" id="C25"> <sup>[25]</sup>
                                   </span>
                                    </span>   
                                 <br>
                                 d.  This investigation shall be initiated <i>ex officio</i>.<span class="palabra"><span class="too" id="C26"> <sup>[26]</sup>
                                 </span>
                                  </span>   
                                    The state shall also provide immediately sufficient and overall protection measures regarding any act of coercion, intimidation and threat towards witnesses and investigators.<span class="too" id="C27"> <sup>[28]</sup>
                                    </span>
                                     </span>      
                                 <br>
                             </div>
                             4.  States have the obligation to repair the victim&apos;s violation of human rights.<span class="palabra"><span class="too" id="C26"> <sup>[26]</sup>
                             </span>
                              </span>  
                                  Reparations must be comprehensive; they cannot be restricted to the payment of compensation.<span class="palabra"><span class="too" id="C27"> <sup>[27]</sup>
                                  </span>
                                   </span>   
                                      Civil or administrative procedures aimed at obtaining compensation cannot rest exclusively on the victim or their next of kin&apos;s procedural or evidentiary initiative. <span class="palabra"><span class="too" id="C28"> <sup>[28]</sup>
                                      </span>
                                       </span>   
                 
                 
                         </p>    
                         <h4>&emsp; &emsp; Article 1C. Conventionality Control  </h4>
                         <p>
                                 1. The obligation to respect rights established in Article 1 must be understood as requiring state bodies to exercise a conventionality control. 
                                 <span class="palabra">
                                 <span class="too" id="C29"> <sup>[29]</sup>
                                 </span>
                                  </span>   
                                    This means that &quot;the organs of any of the branches whose authorities perform judicial duties should exercise not only a control of constitutionality, but also of &apos;conventionality&apos; <i>ex officio</i> between the domestic norms and the American Convention, evidently in the context of their respective spheres of competence and the corresponding procedural regulations &quot; 
                                    <span class="palabra">
                                    <span class="too" id="C30"> <sup>[30]</sup>
                                    </span>
                                     </span>  
                                       When performing this task, these domestic bodies must &quot; take into account not only the treaty, but also the interpretation thereof made by the Inter-American Court, which is the ultimate interpreter of the American Convention. &quot; <span class="palabra"><span class="too" id="C28"> <sup>[28]</sup>
                                       </span>
                                        </span>  
                                 <br>
                                 2.  The existence of domestic laws that are contrary to the Convention or to the Inter-American Court’s interpretation of it shall not prevent domestic bodies from exercising the control of conventionality.
                                 <span class="palabra">
                                 <span class="too" id="C32"> <sup>[32]</sup>
                                 </span>
                                  </span>     Domestic laws that are contrary to this Convention shall not be considered a violation of it, unless they are self-executing laws.
                                  <span class="palabra">
                                  <span class="too" id="C33"> <sup>[33]</sup>
                                  </span>
                                   </span>     
                         </p>
                                
                     </div>
                 
 
      </body>
