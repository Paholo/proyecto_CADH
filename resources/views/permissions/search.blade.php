<?php ?>
@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Permission Management</div>

                    <div class="panel-body">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                        <!-- Search form -->
                        <!-- <div class="row">
                        
                            <form action="users.php" method="GET">
                            <input id="search" type="text" placeholder="Type here">
                            <input id="submit" type="submit" value="Search">
                           <div id="custom-search-input">
                                            <div class="input-group col-md-12">
                                                <input type="text" class="  search-query form-control" placeholder="Search" />
                                                <span class="input-group-btn">
                                                    <button class="btn btn-danger" type="button">
                                                        <span class=" glyphicon glyphicon-search"></span>
                                                    </button>
                                                </span>
                                            </div>
                                        </div>
                    </div> -->
                    {!! Form::open(['method'=>'GET','url'=>route('permissions.search'),'class'=>'navbar-form navbar-left','role'=>'search'])  !!}
                        <a href="{{ url('offices/create') }}" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-plus"></span> Add</a>
                        
                        <div class="input-group custom-search-form">
                            <input type="text" class="form-control" name="search" placeholder="Search...">
                            <span class="input-group-btn">
                                <button class="btn btn-default-sm" type="submit">
                                    <i class="fa fa-search"><!--<span class="hiddenGrammarError" pre="" data-mce-bogus="1"-->i>
                                </button>
                            </span>
                        </div>
                        {!! Form::close() !!}
                        <table class="table table-striped table-bordered table-condensed">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Description</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach ($permissions as $key => $permission)

                                <tr class="list-users">
                                    <td>{{ ++$i }}</td>
                                    <td>{{ $permission->display_name }}</td>
                                    <td>{{ $permission->description }}</td>
                                    <td>
                                        <a class="btn btn-info" href="{{ route('permissions.show',$permission->id) }}">Show</a>
                                        <a class="btn btn-primary" href="{{ route('permissions.edit',$permission->id) }}">Edit</a>
                                        <a>{{ route('permissions.show',$permission->id)}}</a> 
                                        <form action="{{ url('admin/permissions/'.$permission->id) }}" method="POST" style="display: inline-block">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}

                                            <button type="submit" id="delete-task-{{ $permission->id }}" class="btn btn-danger">
                                                <i class="fa fa-btn fa-trash"></i>Delete
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <a href="{{ route('permissions.create') }}" class="btn btn-success">New Permission</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection