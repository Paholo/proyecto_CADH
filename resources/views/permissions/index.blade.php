<?php ?>
@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Permission Management</div>

                    <div class="panel-body">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                        <!-- Search form -->
                        <!-- <div class="row">
                        
                            <form action="users.php" method="GET">
                            
                            {!! Form::open(['method'=>'GET','url'=>'admin\permissions\search','class'=>'navbar-form navbar-left','role'=>'search'])  !!}
                           <div id="custom-search-input">
                                            <div class="input-group col-md-12">
                                                <input type="text" class="  search-query form-control" placeholder="Search" />
                                                <span class="input-group-btn">
                                                    <button class="btn btn-danger" type="button">
                                                        <span class=" glyphicon glyphicon-search"></span>
                                                    </button>
                                                </span>
                                            </div>
                                        </div>
                                         {!! Form::close() !!}
                    </div> -->
                   
                    {!! Form::open(['method'=>'GET','url'=>'admin\permissions\search','class'=>'row','role'=>'search'])  !!}
                        
                    <div id="custom-search-input">
                                            <div class="input-group col-md-12">
                                                <input type="text" name ="search" class="  search-query form-control" placeholder="Search" />
                                                <span class="input-group-btn">
                                                    <button class="btn btn-danger" type="submit">
                                                        <span class=" glyphicon glyphicon-search"></span>
                                                    </button>
                                                </span>
                                            </div>
                                        </div>
                       
                     
                        {!! Form::close() !!}
                     @if(sizeof($permissions) >0 )   
                        <table class="table table-striped table-bordered table-condensed">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Description</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach ($permissions as $key => $permission)

                                <tr class="list-users">
                                    <td>{{ ++$i }}</td>
                                    <td>{{ $permission->display_name }}</td>
                                    <td>{{ $permission->description }}</td>
                                    <td>
                                        <a class="btn btn-info" href="{{ route('permissions.show',$permission->id) }}">Show</a>
                                        <a class="btn btn-primary" href="{{ route('permissions.edit',$permission->id) }}">Edit</a>
                                       
                                        <form action="{{ url('admin/permissions/'.$permission->id) }}" method="POST" style="display: inline-block">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}

                                            <button type="submit" id="delete-task-{{ $permission->id }}" class="btn btn-danger">
                                                <i class="fa fa-btn fa-trash"></i>Delete
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        @else
                        <h2> No hay resultados </h2>
                        <br>
                        @endif
                        <a href="{{ route('permissions.create') }}" class="btn btn-success">New Permission</a>
                        <a href="{{ route('permissions.index') }}" class="btn btn-info">Ver todo</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection