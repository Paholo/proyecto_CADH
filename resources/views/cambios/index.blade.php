<?php ?>
@extends('layouts.app')

@section('content')
    <div class="container">
    
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Cambios Management</div>

                    <div class="panel-body">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                        {!! Form::open(['method'=>'GET','url'=>'admin\cambios\search','class'=>'row','role'=>'search'])  !!}
                        
                    <div id="custom-search-input">
                                            <div class="input-group col-md-12">
                                                <input type="text" name ="search" class="  search-query form-control" placeholder="Search" />
                                                <span class="input-group-btn">
                                                    <button class="btn btn-danger" type="submit">
                                                        <span class=" glyphicon glyphicon-search"></span>
                                                    </button>
                                                </span>
                                            </div>
                                        </div>
                       
                     
                        {!! Form::close() !!}
                        @if(sizeof($cambios) >0 )  
                        <table class="table table-striped table-bordered table-condensed">
                      
                       
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Titulo</th>
                                <th>Documento</th>
                                <th>Segmento</th>
                                <!-- <th>Autor</th> -->
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $contador =0 ?>
                            @foreach ($cambios as $key => $cambio)
                                                
                                <tr class="list-users">
                                    <td>{{ ++$i }}</td>
                                    <td>{{ $cambio->Titulo }} 
                                    <button class="btn" title='{{$cambio->Titulo }} '>Text</button>
                                        <script>
                                        tippy('.btn')
                                        </script>
                                    </td>
                                    <td></td>
                                    <td>{{ $segmento[$contador] }}</td>
                                    <!-- <td>{{ $cambio->id_autor }}</td> -->
                                   
                                    <td>
                                        <a class="btn btn-info" href="{{ route('cambios.show',$cambio->id) }}">Show</a>
                                        <a class="btn btn-primary" href="{{ route('cambios.edit',$cambio->id) }}">Edit</a>

                                        <form action="{{ url('admin/cambios/'.$cambio->id) }}" method="POST" style="display: inline-block">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}

                                            <button type="submit" id="delete-task-{{ $cambio->id }}" class="btn btn-danger">
                                                <i class="fa fa-btn fa-trash"></i>Delete
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                                <?php $contador++ ?>
                            @endforeach
                            </tbody>
                            {{ $cambios->links() }}
                        </table>
                        @else
                        <h2> No hay resultados </h2>
                        <br>
                        @endif
                        <!-- <a href="{{ route('documentos.create') }}" class="btn btn-success">New Documento</a> -->
                        <a href="{{ route('cambios.index') }}" class="btn btn-info">Ver todo</a>
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection