<?php ?>
@extends('layouts.app')

@section('content')
    <div class="container">
  
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Informacion Documento</div>

                    <div class="panel-body">


                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Título</label>
                            {{ $seccion->Titulo }}
                        </div>

                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Tipo</label>
                            {{ $seccion->Tipo }}
                        </div>

                        
                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Autor</label>
                            {{ $seccion->Autor }}
                        </div>
                     
                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Contenido</label>
                            <?php echo htmlspecialchars_decode( $seccion->Contenido_html) ?>
                        </div>
                    </div>
                </div>
                <?php $i =0; ?>
                @if(sizeof($cambios) >0 )  
                @if(sizeof($cambios) >0 )  
                        <h2 >Listado de Cambios </h2>
                        <table class="table table-striped table-bordered table-condensed">
                      
                       
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Titulo</th>
                               
                                <!-- <th>Autor</th> -->
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $contador =0 ?>
                            @foreach ($cambios as $key => $cambio)
                                                
                                <tr class="list-users">
                                    <td>{{ ++$i }}</td>
                                    <td>{{ $cambio->Titulo }} 
                                
                                    </td>
                                    
                                   
                                    <!-- <td>{{ $cambio->id_autor }}</td> -->
                                   
                                    <td>
                                        <a class="btn btn-info" href="{{ route('cambios.show',$cambio->id) }}">Show</a>
                                        <a class="btn btn-primary" href="{{ route('cambios.edit',$cambio->id) }}">Edit</a>
                                    </td>
                                    <td>
                                        <form action="{{ url('admin/cambios/'.$cambio->id) }}" method="POST" style="display: inline-block">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}

                                            <button type="submit" id="delete-task-{{ $cambio->id }}" class="btn btn-danger">
                                                <i class="fa fa-btn fa-trash"></i>Delete
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                                <?php $contador++ ?>
                            @endforeach
                            </tbody>
                        </table>
                        @else
                        <h2> No hay resultados </h2>
                        <br>
                        @endif
                
                
                @endif
            </div>
        </div>
    </div>

@endsection