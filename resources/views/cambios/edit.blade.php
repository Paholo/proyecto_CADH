<?php ?>

@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Editar Cambio</div>

                    <div class="panel-body">
                        <!-- Display Validation Errors -->
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> Hay problemas con los datos que se ingresaron<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif


                        <form class="form-horizontal" role="form" method="POST"
                              action="{{ url('admin/cambios/'.$cambio->id) }}">
                            {{ csrf_field() }}
                            {{ method_field('PATCH') }}

                            <div class="form-group{{ $errors->has('Titulo') ? ' has-error' : '' }}">
                                <label for="Titulo" class="col-md-4 control-label">Título</label>

                                <div class="col-md-6">
                                    <input id="display_name" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"  name="Titulo"
                                           value="{{$cambio->Titulo}}"
                                           required autofocus>

                                    @if ($errors->has('Titulo'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('Titulo') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Contenido</label>
                            <?php echo htmlspecialchars_decode( $cambio->Contenido) ?>
                            </div>

                            
                            
                      <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Primer antecedente</label>
                            <br>
                            <div class="col-md-6">
                                <input id="PAT" type="primer" class="form-control" placeholder="Titulo" name="PAT" value="" required>

                                @if ($errors->has('PAT'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('PAT') }}</strong>
                                    </span>
                                @endif
                                

                                <input id="PAC" type="contenido" class="form-control" placeholder="contenido" name="PAC" value="" required>

                                @if ($errors->has('PAC'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('PAC') }}</strong>
                                    </span>
                                @endif

                                <input id="PAF" type="fecha" class="form-control" placeholder="Fecha" name="PAF" value="" required>

                                @if ($errors->has('PAF'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('PAF') }}</strong>
                                    </span>
                                @endif

                            </div>
                        </div>

                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Antecedente mas imporante </label>
                            <br>
                            <div class="col-md-6">
                            <input id="MAT" type="primer" class="form-control" placeholder="Titulo" name="MAT" value="" required>

                            @if ($errors->has('1AT'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('1AT') }}</strong>
                                </span>
                            @endif

                            <input id="MAC" type="contenido" class="form-control" placeholder="contenido" name="MAC" value="" required>

                            @if ($errors->has('MAC'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('MAC') }}</strong>
                                </span>
                            @endif

                            <input id="MAF" type="fecha" class="form-control" placeholder="Fecha" name="MAF" value="" required>

                            @if ($errors->has('MAF'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('MAF') }}</strong>
                                </span>
                            @endif
                            </div>
                            
                        </div>

                        

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Update
                                    </button>

                                    <a class="btn btn-link" href="{{ url('admin/cambios') }}">
                                        Cancel
                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    

        
@endsection