<?php function cita($ante){
    $nueva;
    $rest = substr($ante->Fecha, -2);
    //consultiva
    if($ante->Seria == "A"){

        $nueva = $ante->Nombre . '. Opinión Consultiva OC-'.$ante->Numero.'/'.$rest.', ' . $ante->Corte . '. (serie A) No' .$ante->Numero . ' ('.$ante->Fecha .') &#182; '.$ante->Parrafo. '.';
    }

    //contenciosa
    if($ante->Seria == "C"){
        $nueva = $ante->Nombre . " v. " .$ante->Contra .'. '.$ante->Tipo .'. Sentencia. ' . $ante->Corte . '. (serie A) No' .$ante->Numero . ' ('.$ante->Fecha .') &#182; '.$ante->Parrafo. '.';
    }

    //otra
    if($ante->Seria == "W"){
        $nueva = $ante->explicacion.'. '.$ante->Nombre . '. Opinión Consultiva OC-'.$ante->Numero.'/'.$rest.', ' . $ante->Corte . '. (serie A) No' .$ante->Numero . ' ('.$ante->Fecha .') &#182; '.$ante->Parrafo. '.';
    }
    return $nueva; 
}