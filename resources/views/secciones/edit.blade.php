<?php ?>

@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Editar Segmento</div>

                    <div class="panel-body">
                        <!-- Display Validation Errors -->
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> Existio un problema con los datos ingresados <br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif


                        <form class="form-horizontal" role="form" method="POST"
                              action="{{ url('admin/segmentos/'.$segmento->id) }}">
                            {{ csrf_field() }}
                            {{ method_field('PATCH') }}

                            <div class="form-group{{ $errors->has('Titulo') ? ' has-error' : '' }}">
                            <label for="Titulo" class="col-md-4 control-label">Titulo</label>

                            <div class="col-md-6">
                                <input id="Titulo" type="text" class="form-control" name="Titulo" 
                                value="{{$segmento->Titulo}}" required autofocus>

                                @if ($errors->has('Titulo'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('Titulo') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>



                            
                            <div class="form-group{{ $errors->has('tipo') ? ' has-error' : '' }}">
                                <label for="tipo" class="col-md-4 control-label">Tipo</label>

                                <div class="col-md-6">

                                    <select id="tipo" name="tipo[]" multiple>
                                     
                                          
                                                <option value="Carta" >Carta</option>
                                                <option value="Ley">Ley</option>
                                           
                                      
                                    </select>

                                    @if ($errors->has('tipo'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('tipo') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                         

                            <div class="form-group{{ $errors->has('documentos') ? ' has-error' : '' }}">
                                <label for="documentos" class="col-md-4 control-label">Documento Padre</label>

                                <div class="col-md-6">

                                    <select id="documento" name="documentos[]" multiple>
                                        @foreach ($documentos as $documento)
                                            <option value="{{$documento->id}}" >
                                                {{$documento->Titulo}}
                                            </option>
                                        @endforeach
                                    </select>

                                    @if ($errors->has('documentos'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('documentos') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Update
                                    </button>

                                    <a class="btn btn-link" href="{{ url('admin/segmentos') }}">
                                        Cancel
                                    </a>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection