<?php ?>
@extends('layouts.app')

@section('content')
    <div class="container">
  
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Informacion Documento</div>

                    <div class="panel-body">


                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Título</label>
                            {{ $seccion->Titulo }}
                        </div>

                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Tipo</label>
                            {{ $seccion->Tipo }}
                        </div>

                        
                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Autor</label>
                            {{ $seccion->Autor }}
                        </div>
                     
                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Contenido</label>
                            <div class="contendio" ><?php echo htmlspecialchars_decode( $seccion->Contenido) ?></div>
                        </div>
                    </div>
                    <a href="{{ route('seccionCambios.create',['titulo'=>$seccion->Titulo,'padre'=>$seccion->id]) }}" class="btn btn-success">Crear Cambio a la Sección</a>
                </div>
                @if(sizeof($secCambios) >0 )  
                
                @for($i = 0; $i < sizeof($secCambios); $i++)
                <div class="panel panel-default">
                <div class="panel-heading">Informacion Seccion  {{ $secCambios[$i]->Titulo }} </div>

                <div class="panel-body">


                    <div class="form-group">
                        <label for="name" class="col-md-4 control-label">Título:</label>
                        {{ $secCambios[$i]->Titulo }}
                    </div>

                    <div class="form-group">
                        <label for="name" class="col-md-4 control-label">Tipo:</label>
                        {{ $secCambios[$i]->Tipo }}
                    </div>

                    <div class="form-group">
                        <label for="name" class="col-md-4 control-label">Autor:</label>
                        {{ $secCambios[$i]->Autor }}
                    </div>

                    <div class="form-group">
                        <label for="name" class="col-md-4 control-label">Contenido:</label>
                        <br>
                        <br>
                        <div class ="conDiv"> <?php echo htmlspecialchars_decode( $secCambios[$i]->Contenido_html) ?> </div>
                    </div>


                  

                 
                </div>
                <a href="{{ route('seccionCambios.show',['iden'=>$secCambios[$i]->id]) }}" class="btn btn-success">Administrar Cambio</a>
                <a href="{{ route('displayCambio.show',['iden'=>$secCambios[$i]->id]) }}" class="btn btn-default">Ver</a>
                <!-- <a href="{{ route('secciones.show',['titulo'=>$secCambios[$i]->Titulo]) }}" class="btn btn-success">Ver Sección</a> -->
               
                </div>
                    
                    
                
               
                @endfor
                @endif
            </div>
            <!-- <div id = "test4" style="" >
    <div class="w3-content w3-display-container" style="width:250px; /* or whatever width you want. */
        max-width:250px; height:250px; background-color:black; ">  <div id="tipi"><h3>Cool <span style="color: pink;">HTML</span> inside here!</h3></div>
        <div class="mySlides"><h3>Cool <span style="color: pink;">HTML</span> inside here!</h3></div>
        <div class="mySlides"><h3>Cool <span style="color: blue;">HTML</span> inside here!</h3></div>
        <div class="mySlides"><h3>Cool <span style="color: red;">HTML</span> inside here!</h3></div>
        <div class="w3-center w3-container w3-section w3-large w3-text-white w3-display-bottommiddle" style="width:100%">
            <div class="w3-left w3-hover-text-khaki" onclick="plusDivs(-1)">&#10094;</div>
            <div class="w3-right w3-hover-text-khaki" onclick="plusDivs(1)">&#10095;</div>
            <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv(1)"></span>
            <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv(2)"></span>
            <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv(3)"></span>
        </div>
    </div>
  </div> -->

<script> 
                 tippy('#t31', {
                    // html: '#test4',
                    html: document.querySelector('#test4'),
                    delay: 25,
                        flip: true,
                        trigger: "click",
                        arrow: true,
                        arrowType: 'round',
                        size: 'large',
                        interactive: true,
                        interactiveBorder: 5,
                    // ...or...
                   // html: document.querySelector('#myTemplate'),
                    // ...or you can clone a direct element too...
                    //html: document.querySelector('#myTemplate').cloneNode(true)
                    })
                    </script>
            <script>
                        var x = document.getElementsByClassName("CADH");
                          x[0].setAttribute("id", "democlass");
                          var y = document.getElementsByClassName("pes");
                          y[0].setAttribute("id", "democlass");
                          y[0].innerHtml= "hello";
                    </script>

<script>
var slideIndex = 1;
showDivs(slideIndex);

function plusDivs(n) {
  showDivs(slideIndex += n);
}

function currentDiv(n) {
  showDivs(slideIndex = n);
}

function showDivs(n) {
  var i;
  var x = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  document.getElementById("myText").innerHTML = x.length;
  document.getElementById("total").innerHTML = n;
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length}
  for (i = 0; i < x.length; i++) {
     x[i].style.display = "none";  
  }
  for (i = 0; i < dots.length; i++) {
     dots[i].className = dots[i].className.replace(" w3-white", "");
  }
  x[slideIndex-1].style.display = "block";  
  dots[slideIndex-1].className += " w3-white";
  document.getElementById("tipi").innerHTML = "";

}
</script>
<!-- <p>
  Create a simple network with some nodes and edges.
</p>

<div id="mynetwork"></div> -->

<script type="text/javascript">
  // create an array with nodes
  var nodes = new vis.DataSet([
    {id: 1, label: 'Node 1',color:{border: 'black',
    background: 'red'}
    },
    {id: 2, label: 'Node 2'},
    {id: 3, label: 'Node 3'},
    {id: 4, label: 'Node 4'},
    {id: 5, label: 'Node 5'}
  ]);

  // create an array with edges
  var edges = new vis.DataSet([
    {from: 1, to: 3},
    {from: 1, to: 2},
    {from: 2, to: 4},
    {from: 2, to: 5},
    {from: 3, to: 3}
  ]);

  // create a network
  var container = document.getElementById('mynetwork');
  var data = {
    nodes: nodes,
    edges: edges
  };
  var options = {
    nodes:{
         color: '#00cc00'
    }
};
  var network = new vis.Network(container, data, options);
</script>

        </div>
    </div>

@endsection