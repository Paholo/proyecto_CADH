@extends('layouts.app')

@section('content')
<script src="{{ asset('js/vis.js') }}"></script>

<style type="text/css">
    #mynetwork {
      width: 600px;
      height: 400px;
      border: 1px solid lightgray;
    }
  </style>
  <style>
.mySlides {display:none}
.w3-left, .w3-right, .w3-badge {cursor:pointer}
.w3-badge {height:13px;width:13px;padding:0}
</style>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    

                <div class="x_panel">
                  <div class="x_title">
                    <h2>Documentos </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                    <div class="docDisplay" > 
                        <div class="arriba" onclick="toggle()" >
                            <div class="textoArriba"> <h4> Convencion Interamericana de Derechos Humanos up to date <h4></div>
                        </div>
                        <div class="abajo" onclick="toggle()">
                        <div class="textoAbajo">  <h4>  Alvaro Paul <h4> </div>
                        </div>
                    </div>

                    
                    
                    

                   
                    
                    
                  
                    </div>
                <div class="docDisplay">
                    <div class="x_panel" id="myGroup">
                  <div class="x_title">
                    <h2>Default Buttons </h2>
                    <ul class="nav navbar-right panel_toolbox"  id="myGroup">
                      <li><a onclick="test()" ><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                  <div class="lista" id="lista1">

                  <div class="lista" id="lista" >
                        <div class="ch2">Capítulos</h2>
                        <ul class="cul">
                            <li class="cli"><a href="#">Capítulo 1</a></li>
                            <li class="cli"><a href="#">Capítulo 2</a></li>
                            <li class="cli"><a href="#">Capítulo 3</a></li>
                            
                        </ul>
                        </div>
                        <div class="ch2">Artículos</h2>
                        <ul class="cul">
                            <li class="cli"><a href="#">Artículo 1</a></li>
                            <li class="cli"><a href="#">Artículo 2</a></li>
                            <li class="cli"><a href="#">Artículo 3</a></li>
                            <li class="cli"><a href="#">Artículo 4</a></li>
                            <li class="cli"><a href="#">Artículo 5</a></li>
                            <li class="cli"><a href="#">Artículo 6</a></li>
                            <li class="cli"><a href="#">Artículo 7</a></li>
                            <li class="cli"><a href="#">Artículo 8</a></li>
                            <li class="cli"><a href="#">Artículo 9</a></li>
                            <li class="cli"><a href="#">Artículo 10</a></li>
                            <li class="cli"><a href="#">Artículo 11</a></li>
                            <li class="cli"><a href="#">Artículo 12 </a></li>
                            <li class="cli"><a href="#">Artículo 13</a></li>
                            <li class="cli"><a href="#">Artículo 14</a></li>
                            <li class="cli"><a href="#">Artículo 15</a></li>
                            <li class="cli"><a href="#">Artículo 16</a></li>
                            <li class="cli"><a href="#">Artículo 17</a></li>
                            <li class="cli"><a href="#">Artículo 18</a></li>
                            <li class="cli"><a href="#">Artículo 19</a></li>
                            <li class="cli"><a href="#">Artículo 20</a></li>
                            <li class="cli"><a href="#">Artículo 21</a></li>
                            <li class="cli"><a href="#">Artículo 22</a></li>
                            <li class="cli"><a href="#">Artículo 23</a></li>
                            <li class="cli"><a href="#">Artículo 24</a></li>
                            <li class="cli"><a href="#">Artículo 25</a></li>
                            <li class="cli"><a href="#">Artículo 26</a></li>
                            

                            
                        </ul>
                        </div>
                  </div>
                </div>
                </div>

                
                    
                    
                  
                </div>

                
                  
                   
                </div>

                
            </div>
        </div>
    </div>
</div>

<!-- <div class="x_panel">
                  <div class="x_title">
                    <h2>Default Buttons </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <button type="button" class="btn btn-default">Default</button>

                    <button type="button" class="btn btn-primary">Primary</button>

                    <button type="button" class="btn btn-success">Success</button>

                    <button type="button" class="btn btn-info">Info</button>

                    <button type="button" class="btn btn-warning">Warning</button>

                    <button type="button" class="btn btn-danger">Danger</button>

                    <button type="button" class="btn btn-dark">Dark</button>

                    <button type="button" class="btn btn-link">Link</button>
                  </div>
                </div> -->

            <script type="text/javascript">
                function toggle(){
                    var off=document.getElementById('lista');
                    if (off.style.display == "none") {
                        off.style.display = "block";
                    } else {
                        off.style.display = "none";
                    }

                    var off=document.getElementById('lista1');
                    if (off.style.display == "none") {
                        off.style.display = "block";
                    } else {
                        off.style.display = "none";
                    }               
                }

                function test(){
                                    var $myGroup = $('#myGroup');
                    $myGroup.on('show','.collapse', function() {
                        $myGroup.find('.collapse.in').collapse('hide');
                    });
                }
            </script> 
@endsection

