<?php ?>
@extends('layouts.app')

@section('content')

<?php  function cita($ante){
    $nueva;
    $rest = substr($ante->Fecha, -2);
    //consultiva
    if($ante->Serie == "A"){

        $nueva = $ante->Nombre . '. Opinión Consultiva OC-'.$ante->Numero.'/'.$rest.'.';
    }

    //contenciosa
    if($ante->Serie == "C"){
        $nueva = $ante->Nombre . " v. " .$ante->Contra .'. '.$ante->Tipo .'.';
    }

    //otra
    if($ante->Serie == "W"){
        $nueva = $ante->explicacion.'. '.$ante->Nombre . '. ';
    }
    return $nueva; 
} ?>
    <div class="container">
    <a id="back" href="{{  url('admin/seccionCambios/back/'.$seccion->id_secPadre) }}" class="previous round">&#8249;</a>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Informacion Documento</div>

                    <div class="panel-body">


                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Título</label>
                            {{ $seccion->Titulo }}
                        </div>


                        
                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Autor</label>
                            {{ $seccion->Autor }}
                        </div>
                     
                        <div class="form-group">
                           
                            <br>
                            <br>
                            <div class ="conDiv">
                            <?php echo htmlspecialchars_decode( $seccion->Contenido_html) ?>
                            </div>
                        </div>
                        <?php $contador =0; ?>
                       
                        @foreach ($antecedentes as $ant )
                         <?php $tag1 = 'cam'.(string) $ant[0]; ?>
                            
                            <div id = "{{$tag1}}"  class="referencia" >
                                   
                                   @for ($i =1; $i< sizeof($ant);$i++)
                                        
                                        <a style="color:white;"  href="{{ route('displayAntecedente.show',$ant[0]) }}">
                                   <h5>  {{$i}}.&nbsp; <?php $comp = cita($ant[$i]); echo $comp; ?><br></h5> </a>
                                   @endfor
                                  
                            </div>

                                <script> 
                                        var num = <?php echo $ant[0] ?>;
                                        var nom = '#t'+num;
                                        var nom2 = '#'+ "{{$tag1}}";


                                                tippy(nom, {
                                                    html: document.querySelector(nom2),
                                                    delay: 25,
                                                        flip: true,
                                                        arrow: true,
                                                        arrowType: 'round',
                                                        size: 'large',
                                                        interactive: true,
                                                        interactiveBorder: 5,
                                                  
                                                    })
                                                    </script>
                                
                          <?php $contador++ ?>          
                        @endforeach
                    </div>
                </div>
              
            </div>
        </div>
    </div>

@endsection