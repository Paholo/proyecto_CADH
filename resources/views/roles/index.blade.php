<?php ?>
@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Role Management</div>

                    <div class="panel-body">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                        {!! Form::open(['method'=>'GET','url'=>'admin\roles\search','class'=>'row','role'=>'search'])  !!}
                        
                    <div id="custom-search-input">
                                            <div class="input-group col-md-12">
                                                <input type="text" name ="search" class="  search-query form-control" placeholder="Search" />
                                                <span class="input-group-btn">
                                                    <button class="btn btn-danger" type="submit">
                                                        <span class=" glyphicon glyphicon-search"></span>
                                                    </button>
                                                </span>
                                            </div>
                                        </div>
                       
                     
                        {!! Form::close() !!}
                        @if(sizeof($roles) >0 )
                        <table class="table table-striped table-bordered table-condensed">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Description</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach ($roles as $key => $role)

                                <tr class="list-users">
                                    <td>{{ ++$i }}</td>
                                    <td>{{ $role->display_name }}</td>
                                    <td>{{ $role->description }}</td>
                                    <td>
                                        <a class="btn btn-info" href="{{ route('roles.show',$role->id) }}">Show</a>
                                        <a class="btn btn-primary" href="{{ route('roles.edit',$role->id) }}">Edit</a>

                                        <form action="{{ url('admin/roles/'.$role->id) }}" method="POST" style="display: inline-block">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}

                                            <button type="submit" id="delete-task-{{ $role->id }}" class="btn btn-danger">
                                                <i class="fa fa-btn fa-trash"></i>Delete
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        @else
                        <h2> No hay resultados </h2>
                        <br>
                        @endif
                        <a href="{{ route('roles.create') }}" class="btn btn-success">New Role</a>
                        <a href="{{ route('roles.index') }}" class="btn btn-info">Ver todo</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection