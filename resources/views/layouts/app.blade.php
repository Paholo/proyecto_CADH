<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Doctrina Interamericana') }}</title>

  <!-- Styles -->
  <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous"> -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  <link href="{{ asset('css/estilos.css') }}" rel="stylesheet">
  <link href="{{ asset('css/bootstrap-select.css') }}" rel="stylesheet">
  <!-- <link href="{{ asset('css/estilos.scss') }}" rel="stylesheet"> -->
  <!-- <link href="{{ asset('custom.min.css') }}" rel="stylesheet"> -->
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

   <!-- Bootstrap -->
      <link href="{{ asset("css/bootstrap.min.css") }}" rel="stylesheet">
      
      <!-- Font Awesome -->
      <link href="{{ asset("css/font-awesome.css") }}" rel="stylesheet">
      <!-- Custom Theme Style -->
      <link href="{{ asset("css/gentelella.css") }}" rel="stylesheet">

      <link href="{{ asset("css/vis.min.css") }}" rel="stylesheet">

      <!-- <link href="{{ asset("css/animate.css") }}" rel="stylesheet"> -->
      <script src="https://unpkg.com/tippy.js@2.5.4/dist/tippy.all.min.js"></script>
      

      @stack('stylesheets')
     
</head>
<body class="nav-sm">

<div  class="container body">

    <div class="main_container">
    @if (Auth::guest())
     @include('layouts.sidebar')
    @else
    @include('layouts.sidebar')
 
    @endif
<div class="right_col" role="main">

@yield('content')
</div> <!-- fin right_col-->    
</div> <!-- fin main container-->
   

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
          <!-- Scripts -->
          
            <!-- jQuery -->
            <!-- <script src="{{ asset('js/jquery.min.js') }}"></script> -->
        <!-- Bootstrap -->
        <!-- <script src="{{ asset('js/bootstrap.min.js') }}"></script> -->
        <!-- Custom Theme Scripts -->
        <script src="{{ asset('js/gentelella.min.js') }}"></script>
        <script src="{{ asset('js/vis.js') }}"></script>
        <script src="{{ asset('js/bootstrap-select.js') }}"></script>

        
        <!-- <script src="{{ asset('js/app.js') }}"></script> -->

        @stack('scripts')
        <script>$('select').selectpicker();</script>
     </div> <!--fin div inicial -->
</body>
</html>
