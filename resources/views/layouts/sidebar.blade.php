<div class="col-md-3 left_col  menu_fixed">
<div class="left_col scroll-view">

<img src="{{ asset('img/mundo-blanco.png') }}"  > 
    <div class="navbar nav_title" style="border: 0;">
    
    </div>

    <div class="profile"><!--img_2 -->
        <div class="profile_pic">
           <!-- <i class="fa fa-paw"></i> -->
        </div>
        <div class="profile_info">
            <span>Bienvenido,</span>
            @if (Auth::guest())
            
             @else
             <h2>{{ Auth::user()->name }}</h2>
           @endif
        </div>
    </div>
    <div class="clearfix"></div>
    <br>
    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

        <div class="menu_section">
            <h3>General</h3>
            <ul class="nav side-menu">
                <li><a href="{{  url('/inicio') }}"><i class="fa fa-home"></i> Inicio </span></a>                        
                </li>
            
            @role('admin')
                <li><a><i class="fa fa-table"></i> Tablas <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                    <li><a href="{{ route('roles.index') }}">Roles</a></li>
                    <li><a href="{{ route('users.index') }}">Usuarios</a></li>
                    <li><a href="{{ route('permissions.index') }}">Permisos</a></li>
                    <li><a href="{{ route('productos.index') }}">Productos</a></li>
                    <li><a href="{{ route('documentos.index') }}">Documentos</a></li>
                    <li><a href="{{ route('secciones.index') }}">Secciones</a></li>
                    <li><a href="{{ route('cambios.index') }}">Cambios</a></li>
                    </ul>
                </li>
            @endrole

            @role('Editor')
                <li><a><i class="fa fa-table"></i> Tablas <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                    
                    <li><a href="{{ route('documentos.index') }}">Documentos</a></li>
               
                    </ul>
                </li>
            @endrole
            <li><a href="{{ route('displayDocumento.show',1) }}"><i class="fa fa-book"></i> Documentos </span></a>                        
            </li>

             <li><a><i class="fa fa-bookmark-o"></i> Notícias </span></a>                        
            </li>

            <li><a><i class="fa fa-circle-o"></i> Sobre nosotros </span></a>                        
            </li>   
            @if (Auth::guest())
            
             @else
             <li><a onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i> Cerrar  Sesión</span></a>                        
                </li>
           @endif
            
            </ul>

        </div>
        

    </div>

    <div class="sidebar-footer hidden-small">
        <a data-toggle="tooltip" data-placement="top" title="Settings">
            <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
        </a>
        <a data-toggle="tooltip" data-placement="top" title="FullScreen">
            <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
        </a>
        <a data-toggle="tooltip" data-placement="top" title="Lock">
            <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
        </a>
        <a data-toggle="tooltip" data-placement="top" title="Cerrar Sesión" onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
            <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
        </a>
        

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
    </div>

    
</div>

</div> <!--left-col-->
@include('layouts.topbar')
</div> <!--col-md-3 left_col-->
