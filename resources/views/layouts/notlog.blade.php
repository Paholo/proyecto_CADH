<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

  <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  <link href="{{ asset('css/estilos.css') }}" rel="stylesheet">
  <!-- <link href="{{ asset('custom.min.css') }}" rel="stylesheet"> -->

   <!-- Bootstrap -->
      <link href="{{ asset("css/bootstrap.min.css") }}" rel="stylesheet">
      <!-- Font Awesome -->
      <link href="{{ asset("css/font-awesome.css") }}" rel="stylesheet">
      <!-- Custom Theme Style -->
      <link href="{{ asset("css/gentelella.css") }}" rel="stylesheet">

      <link href="{{ asset("css/animate.css") }}" rel="stylesheet">

      @stack('stylesheets')
      
    
</head>
<body class="nav-md">
<div id="app" class="container body">
   
@yield('content')



</body>
</html>
