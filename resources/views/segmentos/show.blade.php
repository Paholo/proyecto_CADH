<?php ?>
@extends('layouts.app')

@section('content')
    <div class="container">
  
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Informacion Documento</div>

                    <div class="panel-body">


                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Título</label>
                            {{ $segmento->Titulo }}
                        </div>

                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Tipo</label>
                            {{ $segmento->tipo }}
                        </div>

                        
                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Documento Padre</label>
                            {{ $segmento->documento_titulo }}
                        </div>
                     
                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Contenido</label>
                            <?php echo htmlspecialchars_decode( $segmento->Contenido) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection