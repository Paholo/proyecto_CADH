<?php ?>
@extends('layouts.app')

@section('content')
    <div class="container">
    
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Segmentos Management</div>

                    <div class="panel-body">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                        {!! Form::open(['method'=>'GET','url'=>'admin\segmentos\search','class'=>'row','role'=>'search'])  !!}
                        
                    <div id="custom-search-input">
                                            <div class="input-group col-md-12">
                                                <input type="text" name ="search" class="  search-query form-control" placeholder="Search" />
                                                <span class="input-group-btn">
                                                    <button class="btn btn-danger" type="submit">
                                                        <span class=" glyphicon glyphicon-search"></span>
                                                    </button>
                                                </span>
                                            </div>
                                        </div>
                       
                     
                        {!! Form::close() !!}
                        @if(sizeof($segmentos) >0 )  
                        <table class="table table-striped table-bordered table-condensed">
                      
                       
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Titulo</th>
                                <th>Tipo</th>
                                <th>Documento Padre</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach ($segmentos as $key => $segmento)
                                                
                                <tr class="list-users">
                                    <td>{{ ++$i }}</td>
                                    <td>{{ $segmento->Titulo }} 
                                    <button class="btn" title='{{$segmento->Titulo }} '>Text</button>
                                        <script>
                                        tippy('.btn')
                                        </script>
                                    </td>
                                    <td>{{ $segmento->tipo }}</td>
                                    <td>{{ $segmento->documento_titulo }}</td>
                                   
                                    <td>
                                        <a class="btn btn-info" href="{{ route('segmentos.show',$segmento->id) }}">Show</a>
                                        <a class="btn btn-primary" href="{{ route('segmentos.edit',$segmento->id) }}">Edit</a>

                                        <form action="{{ url('admin/segmentos/'.$segmento->id) }}" method="POST" style="display: inline-block">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}

                                            <button type="submit" id="delete-task-{{ $segmento->id }}" class="btn btn-danger">
                                                <i class="fa fa-btn fa-trash"></i>Delete
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        @else
                        <h2> No hay resultados </h2>
                        <br>
                        @endif
                        <a href="{{ route('segmentos.create') }}" class="btn btn-success">New Segmento</a>
                        <a href="{{ route('segmentos.index') }}" class="btn btn-info">Ver todo</a>
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection