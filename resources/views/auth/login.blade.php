@extends('layouts.notlog')

@section('content')
<div class="login">
<a class="hiddenanchor" id="signup"></a>
<a class="hiddenanchor" id="signin"></a>

<div class="login_wrapper">
  <div class="animate form login_form">
    <section class="login_content">

        <form class="form-horizontal" method="POST" action="{{ route('login') }}">
          <h1>Iniciar Sesión</h1>
          {{ csrf_field() }}
          
          <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
              
              <div >
                  <input id="email" type="email" class="form-control" placeholder="Correo Electronico" name="email" value="{{ old('email') }}" required autofocus>
        
                  @if ($errors->has('email'))
                      <span class="help-block">
                          <strong>{{ $errors->first('email') }}</strong>
                      </span>
                  @endif
              </div>
          </div>

          <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
              <!-- <label for="password" class="col-md-4 control-label">Password</label> -->

              <div class>
                  <input id="password" type="password" class="form-control" placeholder="Contraseña" name="password" required>

                  @if ($errors->has('password'))
                      <span class="help-block">
                          <strong>{{ $errors->first('password') }}</strong>
                      </span>
                  @endif
              </div>
          </div>
<!-- 
          <div class="form-group">
              <div class="col-md-6 col-md-offset-4">
                  <div class="checkbox">
                      <label>
                          <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                      </label>
                  </div>
              </div>
          </div> -->

          <div class="form-group">
              <div>
                  <button type="submit" class="btn btn-primary">
                      Ingresar
                  </button>

                  <a class="btn btn-link" href="{{ route('password.request') }}">
                      Olvidaste tu contraseña
                  </a>
              </div>
          </div>
          <div class="clearfix"></div>
          
                        <div class="separator">
                          <p>New to site?
                            <a href="{{ route('register') }}" class="to_register"> Crear cuenta </a>
                          </p>
          
                          <div class="clearfix"></div>
                          <br />
          
                          <div>
                            <h1><i class="fa fa-paw"></i> Gentelella Alela!</h1>
                            <p>©2016 All Rights Reserved. Gentelella Alela! is a Bootstrap 3 template. Privacy and Terms</p>
                          </div>
                        </div>
        </div>
      </form>
      
    </section>
  </div>
    
 
</div>
</div>
@endsection
