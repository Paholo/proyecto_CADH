<?php ?>
@extends('layouts.app')

@section('content')
<script src="{{ asset('js/vis.js') }}"></script>

<style type="text/css">
    #mynetwork {
    
      border: 1px solid lightgray;
    }
  </style>
  <?php  function cita($ante){
    $nueva;
    $rest = substr($ante->Fecha, -2);
    //consultiva
    if($ante->Serie == "A"){

        $nueva = $ante->Nombre . '. Opinión Consultiva OC-'.$ante->Numero.'/'.$rest.', ' . $ante->Corte . '. (serie A) No.' .$ante->Numero . ' ('.$ante->Fecha .') &#182; '.$ante->Parrafo. '.';
    }

    //contenciosa
    if($ante->Serie == "C"){
        $nueva = $ante->Nombre . " v. " .$ante->Contra .'. '.$ante->Tipo .'. Sentencia. ' . $ante->Corte . '. (serie C) No.' .$ante->Numero . ' ('.$ante->Fecha .') &#182; '.$ante->Parrafo. '.';
    }

    //otra
    if($ante->Serie == "W"){
        $nueva = $ante->explicacion.'. '.$ante->Nombre . '. Opinión Consultiva OC-'.$ante->Numero.'/'.$rest.', ' . $ante->Corte . '. (serie A) No.' .$ante->Numero . ' ('.$ante->Fecha .') &#182; '.$ante->Parrafo. '.';
    }
    return $nueva; 
} ?>
    <div class="container">
  
        <div class="row">
            <div class="col-md-8 col-md-offset-2" >
                <div class="panel panel-default">
                    <div class="panel-heading">Informacion Cambio</div>

                    <div class="panel-body">


                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Título</label>
                            <?php echo htmlspecialchars_decode( $cambio->Titulo) ?>
                        </div>

                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Autor</label>
                            {{ $autor }}
                        </div>

                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Cambio sobre Seccion</label>
                            {{ $sec->Titulo }}
                        </div>


                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Contenido</label>
                            <br>
                            <br>
                             <div class ="conDiv">
                            <?php echo htmlspecialchars_decode( $cambio->Contenido) ?>
                            </div>
                        </div>

                   </div>
              </div>

           
            </div>

            <!-- <div class="col-md-8 col-md-offset-2" >
                <div class="panel panel-default">
                    <div class="panel-heading">Visualizacion</div>

                    <div class="panel-body">

                    <div id="mynetwork" style="height:500px"></div>

                    <script type="text/javascript">
                    // create an array with nodes
                    var nodes = new vis.DataSet([
                        {id: 1, label: 'Node 1',color:{border: 'black',
                        background: 'red'}
                        },
                        {id: 2, label: 'Node 2'},
                        {id: 3, label: 'Node 3'},
                        {id: 4, label: 'Node 4'},
                        {id: 5, label: 'Node 5'}
                    ]);

                    // create an array with edges
                    var edges = new vis.DataSet([
                        {from: 1, to: 3},
                        {from: 1, to: 2},
                        {from: 2, to: 4},
                        {from: 2, to: 5},
                        {from: 3, to: 3}
                    ]);

                    // create a network
                    var container = document.getElementById('mynetwork');
                    var data = {
                        nodes: nodes,
                        edges: edges
                    };
                    var options = {
                        nodes:{
                            color: '#00cc00'
                        }
                    };
                    var network = new vis.Network(container, data, options);
                    </script>

                   </div>
              </div>

           
            </div> -->


            @if(sizeof($antecedentes) >0 ) 
            <?php $cont = 0;?>
            @foreach ($antecedentes as $key => $antecedente)
           
            <div class="col-md-8 col-md-offset-2" >
                <div class="panel panel-default">
                    <div >

                            <div id="cuadrados">
                            @foreach ($cat[$cont] as $key => $cate)
                                @if($cate == "Primer precedente")
                                <div  class="primero" data-toggle="tooltip" data-placement="top" title="Primer precedente" ></div>
                                
                                @endif

                                @if($cate == "Precedente más relevante")
                                <div  class="masImpo"  data-toggle="tooltip" data-placement="top" title="Precedente más relevante"></div>
                                @endif

                                @if($cate == "Último precedente")
                                <div class="ultimo" data-toggle="tooltip" data-placement="top" title="Último precedente"></div>
                                @endif

                                @if($cate == "Origen fuera del sistema")
                                <div class="fuera" data-toggle="tooltip" data-placement="top" title="Origen fuera del sistema"></div>
                                @endif

                                @if($cate == "Cambio de criterio")
                                <div class="cambio" data-toggle="tooltip" data-placement="top" title="Cambio de criterio"></div>
                                @endif

                                @if($cate == "Otro")
                                <div class="pOtro" data-toggle="tooltip" data-placement="top" title="Otro"></div>
                                @endif


                                
                            @endforeach
                            </div>
                </div >
                <!-- nombre, -->
                    <div class="panel-heading"><?php $comp = cita($antecedente); echo $comp; ?> </div>
                    <div class="panel-body">

                        <div class="form-group">
                                <label for="name" class="col-md-4 control-label">Nombre:</label>
                                {{ $antecedente->Nombre .' v. '. $antecedente->Contra }}
                            </div>

                            <div class="form-group">
                                <label for="name" class="col-md-4 control-label">Fecha:</label>
                                {{ $antecedente->Fecha }}
                            </div>


                            <div class="form-group">
                                <label for="name" class="col-md-4 control-label">Contenido:</label>
                                <br>
                                <br>
                                    <div class ="conDiv">
                                <?php echo htmlspecialchars_decode( $antecedente->Detalle) ?>
                                 </div>
                                 <br>
                                 <a href="{{ route('displayAntecedenteAnt.show',$antecedente->id) }}" class="btn btn-success">Antecedentes</a>
                            </div>
                    
                   </div>
                </div>

           
            </div>
            <?php $cont++ ;?>
            @endforeach
            @endif
 
                    
         </div>
    </div>

@endsection
