
## Sobre Doctrina CADH
Doctrina CADH es un proyecto social y académico de la Universidad San Francisco de Quito (Ecuador), en el cual participaron el Politécnico y colegio de Jurisprudencia. El objetivo del proyecto es permitir una gestión eficiente y sistemática de los cambios realizados dentro de la Convención Americana de Derechos Humanos, además de las justificaciones necesarias para los cambios. 

El proceso de ingeniería de software fue utilizado como proyecto de titulación. 

## About Doctrina CADH

Doctrina CADH is a social and academic project done by Universidad San Francisco de Quito (Ecuador), in which the Polytechnic and Jurisprudence School where involved. The main objective of this project was to create an efficient and systematic of the changes in the American Convention of Human Rights. 

## Tecnologies 
- Laravel<br>
- Blade<br>
- CSS<br>
- Javascript<br>
- MySQL<br>


## Modules
-Roles and Permisons based system<br>
-CRUD for documents, sections and changes, Users, Roles, Permitions<br> 
-Administrator and Editor views for the documents<br>
-Dynamic presentation of documents changes<br>
-Interactive editor over sections<br>

## Link 
https://www.doctrinaidh.com/
